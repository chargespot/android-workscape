package com.chargespot.workscape;

import android.content.Intent;

import android.support.annotation.NonNull;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Toast;

import com.chargespot.workscape.Helpers.MixpanelHelper;
import com.chargespot.workscape.Requests.CSRequestManager;
import com.chargespot.workscape.Requests.CSSession;
import com.crashlytics.android.Crashlytics;
import com.google.android.gms.auth.api.Auth;
import com.google.android.gms.auth.api.signin.GoogleSignInAccount;
import com.google.android.gms.auth.api.signin.GoogleSignInOptions;
import com.google.android.gms.auth.api.signin.GoogleSignInResult;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.SignInButton;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.common.api.OptionalPendingResult;
import com.google.android.gms.common.api.ResultCallback;
import com.google.android.gms.common.api.Scope;
import com.mixpanel.android.mpmetrics.MixpanelAPI;

import org.json.JSONObject;

import java.util.Date;

import io.fabric.sdk.android.Fabric;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class AuthActivity extends AppCompatActivity implements
        View.OnClickListener,
        GoogleApiClient.OnConnectionFailedListener {

    private static final String TAG = AuthActivity.class.getCanonicalName();
    private static GoogleApiClient mGoogleApiClient;
    private static final int RC_SIGN_IN = 9001;

    private GoogleSignInOptions mSignInOptions;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Fabric.with(this, new Crashlytics());
        setContentView(R.layout.activity_auth);
        this.setupGoogleApi();
        this.setupView();

        MixpanelHelper.track(this, "AuthActivity - App started");
    }

    protected void onStart() {
        this.silentSignIn();
        super.onStart();
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        MixpanelHelper.flush(this);
    }

    private void setupView() {
        SignInButton signInButton = (SignInButton) findViewById(R.id.sign_in_button);
        signInButton.setSize(SignInButton.SIZE_WIDE);
        signInButton.setScopes(mSignInOptions.getScopeArray());
        signInButton.setOnClickListener(this);

        if (android.os.Build.VERSION.SDK_INT >= 21) {
            Window window = getWindow();
            window.addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS);
            window.clearFlags(WindowManager.LayoutParams.FLAG_TRANSLUCENT_STATUS);
            window.setStatusBarColor(getResources().getColor(R.color.colorAuthStatusBar));
        }
    }

    private void setupGoogleApi() {
        Scope resourceScope = new Scope("https://www.googleapis.com/auth/admin.directory.resource.calendar");
        Scope directoryScope = new Scope("https://www.googleapis.com/auth/admin.directory.customer");
        Scope calendarScope = new Scope("https://www.googleapis.com/auth/calendar");
        Scope userInfoScope = new Scope("https://www.googleapis.com/auth/userinfo.profile");

        // Configure sign-in to request the user's ID, email address, and basic
        // profile. ID and basic profile are included in DEFAULT_SIGN_IN.
        mSignInOptions = new GoogleSignInOptions.Builder(GoogleSignInOptions.DEFAULT_SIGN_IN)
                .requestScopes(resourceScope, directoryScope, calendarScope, userInfoScope)
                .requestIdToken(getString(R.string.googleServerClientId))
                .requestEmail()
                .build();

        mGoogleApiClient = new GoogleApiClient.Builder(this)
                .enableAutoManage(this /* FragmentActivity */, this /* OnConnectionFailedListener */)
                .addApi(Auth.GOOGLE_SIGN_IN_API, mSignInOptions)
                .build();
    }

    private void silentSignIn() {
        OptionalPendingResult<GoogleSignInResult> pendingResult = Auth.GoogleSignInApi.silentSignIn(mGoogleApiClient);
        pendingResult.setResultCallback(new ResultCallback<GoogleSignInResult>() {
            @Override
            public void onResult(@NonNull GoogleSignInResult googleSignInResult) {
                handleSignInResult(googleSignInResult);
            }
        });
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.sign_in_button:
                signIn();
                break;
            // ...
        }
    }

    private void signIn() {
        if (!mGoogleApiClient.isConnected()) {
            mGoogleApiClient.connect();
        }
        Intent signInIntent = Auth.GoogleSignInApi.getSignInIntent(mGoogleApiClient);
        startActivityForResult(signInIntent, RC_SIGN_IN);
    }


    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        // Result returned from launching the Intent from GoogleSignInApi.getSignInIntent(...);
        if (requestCode == RC_SIGN_IN) {
            GoogleSignInResult result = Auth.GoogleSignInApi.getSignInResultFromIntent(data);
            handleSignInResult(result);
            MixpanelHelper.track(this, "AuthActivity - Silent Sign-In");
        }
    }

    @Override
    public void onConnectionFailed(@NonNull ConnectionResult connectionResult) {
        // An unresolvable error has occurred and Google APIs (including Sign-In) will not
        // be available.
        Log.d(TAG, "onConnectionFailed:" + connectionResult);
    }

    private void handleSignInResult(GoogleSignInResult result) {
        if (result.isSuccess()) {
            // Signed in successfully, show authenticated UI.
            GoogleSignInAccount acct = result.getSignInAccount();
            CSRouter.getInstance().setGoogleAccount(acct);
            updateUI(true);

            JSONObject jsonObject = new JSONObject();
            try {
                jsonObject.put("$name", acct.getDisplayName());
                jsonObject.put("$email", acct.getEmail());
                jsonObject.put("Google Id", acct.getId());
                jsonObject.put("Last Login", new Date());
            } catch (Exception e) {
                e.printStackTrace();
            }
            MixpanelHelper.track(AuthActivity.this, "AuthActivity - Successfully logged in", jsonObject);
            MixpanelHelper.identify(this, acct.getEmail());
            MixpanelHelper.setPeople(this, jsonObject);

            fetchCSToken(acct);

        } else {
            // Signed out, show unauthenticated UI.
            updateUI(false);
        }
    }

    private void fetchCSToken(GoogleSignInAccount acct) {
        String idToken = acct.getIdToken();
        try {
            CSRequestManager.getInstance(this).auth(idToken, new Callback<CSSession>() {
                @Override
                public void onResponse(Call<CSSession> call, Response<CSSession> response) {
                    Log.d(TAG, "Auth request successful - " + response.code() + " - " + response.message());
                    if (response.isSuccessful()) {
                        CSSession session = response.body();
                        Log.d(TAG, "Auth Token - " + session.authToken);

                        CSRouter.getInstance().setSession(session);
                        startOnboardingActivity();
                    } else {
                        processSignInError();
                    }
                }

                @Override
                public void onFailure(Call<CSSession> call, Throwable t) {
                    Log.d(TAG, "Auth request failed - " + t.getLocalizedMessage());

                    processSignInError();
                }
            });
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void processSignInError() {
        Toast.makeText(this, "There was a problem logging into the system. Please try again.", Toast.LENGTH_SHORT).show();
        updateUI(false);
        MixpanelHelper.track(this, "AuthActivity - Sign-In Error");
    }

    private void updateUI(boolean signedIn) {
        if (signedIn) {
            findViewById(R.id.sign_in_button).setVisibility(View.GONE);
            findViewById(R.id.loadingSpinner).setVisibility(View.VISIBLE);
        } else {
            findViewById(R.id.sign_in_button).setVisibility(View.VISIBLE);
            findViewById(R.id.loadingSpinner).setVisibility(View.GONE);
        }
    }

    private void startOnboardingActivity() {
        Intent intent = new Intent(this, OnboardingActivity.class);
        startActivity(intent);
        finish();
    }

    private void startHomeActivity() {
        Intent intent = new Intent(this, BaseActivity.class);
        startActivity(intent);
        finish();
    }
}
