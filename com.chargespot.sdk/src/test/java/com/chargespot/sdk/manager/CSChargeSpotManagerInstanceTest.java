package com.chargespot.sdk.manager;

import android.content.Context;
import android.content.pm.PackageManager;
import android.test.AndroidTestCase;


import com.chargespot.sdk.CSChargeSpotManager;
import com.chargespot.sdk.config.CSChargeSpotConfig;
import com.chargespot.sdk.events.ICSChargeSpotEventHandler;
import com.chargespot.sdk.events.ICSGroupEventHandler;
import com.chargespot.sdk.exceptions.CSInvalidParameterException;
import com.chargespot.sdk.handler.CSChargeSpotHandler;
import com.chargespot.sdk.manager.chargespot.CSChargeSpotManagerInstance;
import com.chargespot.sdk.manager.chargespot.CSChargeSpotManagerOptions;
import com.chargespot.sdk.manager.chargespot.ICSChargeSpotManagerInstance;

import junit.framework.Assert;

import org.junit.runner.RunWith;
import org.mockito.Mockito;
import org.powermock.api.mockito.PowerMockito;
import org.powermock.core.classloader.annotations.PrepareForTest;
import org.powermock.modules.junit4.PowerMockRunner;


/**
 * Created by kelvinlau on 16-06-02.
 */
@RunWith(PowerMockRunner.class)
@PrepareForTest(CSChargeSpotManagerInstance.class)
public class CSChargeSpotManagerInstanceTest extends AndroidTestCase {

    public void testSetupChargeSpotSDK() {
        ICSChargeSpotManagerInstance mockInstance = PowerMockito.spy(CSChargeSpotManager.getInstance());

        CSChargeSpotConfig config = new CSChargeSpotConfig("DEV_TOKEN", "PROD_TOKEN", false);

        Context mockContext = Mockito.mock(Context.class);
        PackageManager mockPackageManager = Mockito.mock(PackageManager.class);

        Mockito.when(mockContext.getPackageManager()).thenReturn(mockPackageManager);
        Mockito.when(mockPackageManager.hasSystemFeature(Mockito.anyString())).thenReturn(true);
        Mockito.when(mockInstance.isChargeSpotSDKInitialized()).thenReturn(true);

        try {
            mockInstance.setupChargeSpotSDK(mockContext, config);
        } catch (Exception e) {
            fail("Setup threw exception(" + e.toString() + ")");
        }

        Assert.assertEquals(config, mockInstance.getChargeSpotConfig());
    }

    public void testSetupChargeSpotSDKWithNoConfig() {
        ICSChargeSpotManagerInstance mockInstance = PowerMockito.spy(CSChargeSpotManager.getInstance());
        Context mockContext = Mockito.mock(Context.class);

        try {
            mockInstance.setupChargeSpotSDK(mockContext, null);
        } catch (Exception e) {
            Assert.assertEquals(CSInvalidParameterException.class, e.getClass());
            Assert.assertEquals("config", e.getMessage());
        }
    }

    public void testSetupChargeSpotSDKWithNoContext() {
        ICSChargeSpotManagerInstance mockInstance = PowerMockito.spy(CSChargeSpotManager.getInstance());
        CSChargeSpotConfig config = new CSChargeSpotConfig("DEV_TOKEN", "PROD_TOKEN", false);

        try {
            mockInstance.setupChargeSpotSDK(null, config);
        } catch (Exception e) {
            Assert.assertEquals(CSInvalidParameterException.class, e.getClass());
            Assert.assertEquals("context", e.getMessage());
        }
    }

    public void testRegisterForChargeSpotEvents() {
        ICSChargeSpotManagerInstance mockInstance = PowerMockito.spy(CSChargeSpotManager.getInstance());
        CSChargeSpotHandler mockHandler = PowerMockito.mock(CSChargeSpotHandler.class);
        ICSChargeSpotEventHandler mockCallback = PowerMockito.mock(ICSChargeSpotEventHandler.class);

        CSChargeSpotManagerOptions options = new CSChargeSpotManagerOptions();

        Mockito.when(mockInstance.isChargeSpotSDKInitialized()).thenReturn(true);
        Mockito.doNothing().when(mockHandler).registerForChargeSpotEvents(mockCallback, options);

        try {
            mockInstance.registerForChargeSpotEvents(mockCallback, options);
        } catch (Exception e) {}

        Mockito.verify(mockInstance).isChargeSpotSDKInitialized();
        Assert.assertEquals(options, mockInstance.getChargeSpotOptions());
        Assert.assertNull(mockInstance.getGroupOptions());
    }

    public void testRegisterForGroupEvents() {
        ICSChargeSpotManagerInstance mockInstance = PowerMockito.spy(CSChargeSpotManager.getInstance());
        CSChargeSpotHandler mockHandler = PowerMockito.mock(CSChargeSpotHandler.class);
        ICSGroupEventHandler mockCallback = PowerMockito.mock(ICSGroupEventHandler.class);

        CSChargeSpotManagerOptions options = new CSChargeSpotManagerOptions();

        Mockito.when(mockInstance.isChargeSpotSDKInitialized()).thenReturn(true);
        Mockito.doNothing().when(mockHandler).registerForGroupEvents(mockCallback, options);

        try {
            mockInstance.registerForGroupEvents(mockCallback, options);
        } catch (Exception e) {}

        Mockito.verify(mockInstance).isChargeSpotSDKInitialized();
        Assert.assertEquals(options, mockInstance.getGroupOptions());
        Assert.assertNull(mockInstance.getChargeSpotOptions());
    }
}
