package com.chargespot.workscape;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.chargespot.workscape.Helpers.CircleTransform;
import com.chargespot.workscape.Helpers.MixpanelHelper;
import com.chargespot.workscape.Helpers.SharedPreferencesHelper;
import com.chargespot.workscape.Models.CSClient;
import com.chargespot.workscape.Models.CSUser;
import com.squareup.picasso.Picasso;

/**
 * Created by kelvinlau on 2016-08-23.
 */

public class OnboardingProfileFragment extends Fragment implements
        View.OnClickListener,
        TextView.OnEditorActionListener {

    public static final String TAG = OnboardingProfileFragment.class.getCanonicalName();

    public interface OnboardingProfileSubmitListener {
        void onSubmit();
    }

    private OnboardingProfileSubmitListener mListener;

    private ImageView mProfileImageView;
    private TextView mNameTextView;
    private TextView mSubtitleTextView;
    private EditText mFloorEditText;
    private Button mSubmitButton;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_onboarding_profile, container, false);

        CSUser currentUser = CSRouter.getInstance().getCurrentUser();
        CSClient currentClient = CSRouter.getInstance().getSession().getClient();

        mProfileImageView = (ImageView)view.findViewById(R.id.profileImageView);
        mNameTextView = (TextView)view.findViewById(R.id.nameTextView);
        mSubtitleTextView = (TextView)view.findViewById(R.id.subtitleTextView);
        mSubmitButton = (Button)view.findViewById(R.id.submitButton);
        mFloorEditText = (EditText)view.findViewById(R.id.floorEditText);

        Picasso.with(getActivity()).load(currentUser.getImageUrl()).transform(new CircleTransform()).fit().into(mProfileImageView);
        mNameTextView.setText(currentUser.getDisplayName());
        mSubtitleTextView.setText(currentClient.getName());

        mFloorEditText.setOnEditorActionListener(this);

        mSubmitButton.setOnClickListener(this);

        return view;
    }

    public void setOnboardingProfileSubmitListener(OnboardingProfileSubmitListener listener) {
        mListener = listener;
    }

    @Override
    public void onClick(View v) {
        switch(v.getId()) {
            case R.id.submitButton:
                submit();
                break;
        }
    }

    @Override
    public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {
        Log.d(TAG, "Actionid: " + actionId);
        if (actionId == KeyEvent.KEYCODE_ENDCALL) {
            submit();
            return true;
        }

        return false;
    }

    private void submit() {
        if (mFloorEditText.getText().length() == 0) {
            Toast.makeText(getActivity(), getString(R.string.onboarding_profile_missing_floor), Toast.LENGTH_SHORT).show();
            return;
        }
        saveFloor(Integer.valueOf(mFloorEditText.getText().toString()));

        if (mListener != null) {
            mListener.onSubmit();
        }

        MixpanelHelper.track(getActivity(), "OnboardingProfileFragment - Submitted");
    }

    private void saveFloor(int floor) {
        SharedPreferencesHelper.saveCurrentUserFloor(getActivity(), floor);
    }
}
