/*
    Copyright (C) 2015 ChargeSpot
 */
package com.chargespot.sdk.handler;

import android.support.annotation.Nullable;

import com.chargespot.sdk.api.v1.model.ChargeSpotHookListModel;
import com.chargespot.sdk.config.CSChargeSpotSDKConfig;
import com.chargespot.sdk.events.CSChargeSpotEvent;

import java.util.Timer;
import java.util.TimerTask;

public class CSChargeSpotEventHandler
        implements ICSChargeSpotEventHandler {
    public final static int CHARGESPOT_TIMER_TRIGGER = 1000;

    private com.chargespot.sdk.events.ICSChargeSpotEventHandler mEventHandler;
    private Timer mDelayTimer;

    private int mTimeout;
    private int mCountdown;

    private boolean mCountdownActive;
    private boolean mTimeoutActive;
    private boolean mEventFired;

    private CSChargeSpotEvent mDelayedChargeSpotEvent;
    private ChargeSpotHookListModel mDelayChargeSpotHookList;

    {
        mEventHandler = null;
        mDelayTimer = null;
        mTimeout = 0;
        mCountdown = 0;
        mCountdownActive = false;
        mTimeoutActive = false;
        mEventFired = false;
    }

    public CSChargeSpotEventHandler() {
        mDelayTimer = new Timer();

        mDelayTimer.scheduleAtFixedRate(new TimerTask() {
            synchronized public void run() {
                if (mCountdownActive) {
                    mCountdown--;
                } else if (mTimeoutActive) {
                    mTimeout--;
                } else {
                    return;
                }

                if (mCountdown <= 0) {
                    if (CSChargeSpotEventHandler.this.mEventHandler != null) {
                        CSChargeSpotEventHandler.this.mEventHandler.onChargeSpotEventSuccess(mDelayedChargeSpotEvent, mDelayChargeSpotHookList);
                    }
                    CSChargeSpotEventHandler.this.onResetDelayEvent();
                    CSChargeSpotEventHandler.this.mEventFired = true;
                } else if (mTimeout <= 0) {
                    CSChargeSpotEventHandler.this.onResetDelayEvent();
                }
            }
        }, 0, CHARGESPOT_TIMER_TRIGGER);
    }

    /**
     * Register for ChargeSpot events to be received
     *
     * @param callback Callback to execute upon receiving a ChargeSpot event
     */
    public synchronized void registerChargeSpotCallback(@Nullable com.chargespot.sdk.events.ICSChargeSpotEventHandler callback) {
        mEventHandler = callback;
    }

    /**
     * Unregister for ChargeSpot events to be received
     */
    public synchronized void unregisterChargeSpotCallback() {
        mEventHandler = null;
    }

    /**
     * ChargeSpot event has been successfully tracked.
     *
     * @param event         Kind of the event that occurred
     * @param hookListModel List of hook models
     */
    public synchronized void onChargeSpotEvent(CSChargeSpotEvent event, ChargeSpotHookListModel hookListModel) {
        if (event == null || hookListModel == null || hookListModel.hook == null || hookListModel.hook.length == 0) {
            return;
        }

        if (hookListModel.hook[0].action == -1) {
            // Disregard the action
            return;
        }

        if (hookListModel.hook[0].action_delay == 0) {
            if (mEventHandler != null) {
                this.mEventHandler.onChargeSpotEventSuccess(event, hookListModel);
            }
        } else {
            if (event == CSChargeSpotEvent.CSEventStartedChargingOnChargeSpot) {
                mDelayedChargeSpotEvent = event;
                mDelayChargeSpotHookList = hookListModel;

                if (!mCountdownActive && !mTimeoutActive) {
                    this.mCountdown = hookListModel.hook[0].action_delay;
                    this.mTimeout = CSChargeSpotSDKConfig.CHARGESPOT_EVENT_DELAY_OFFLINE;
                }

                mCountdownActive = true;
                mTimeoutActive = false;
            } else if (event == CSChargeSpotEvent.CSEventStoppedChargingOnChargeSpot) {
                if (CSChargeSpotEventHandler.this.mEventFired) {
                    if (mEventHandler != null) {
                        this.mEventHandler.onChargeSpotEventSuccess(event, hookListModel);
                    }
                    CSChargeSpotEventHandler.this.mEventFired = false;
                } else {
                    mCountdownActive = false;
                    mTimeoutActive = true;
                }
            }
        }
    }

    private synchronized void onResetDelayEvent() {
        this.mDelayChargeSpotHookList = null;
        this.mDelayedChargeSpotEvent = null;

        this.mCountdownActive = false;
        this.mTimeoutActive = false;

        this.mCountdown = 0;
        this.mTimeout = 0;
    }
}
