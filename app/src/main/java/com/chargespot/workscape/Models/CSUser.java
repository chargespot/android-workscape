package com.chargespot.workscape.Models;

import android.net.Uri;

import com.google.android.gms.auth.api.signin.GoogleSignInAccount;

/**
 * Created by kelvinlau on 2016-08-03.
 */

public class CSUser {

    private String idToken;

    private String identifier;

    private String displayName;

    private String email;

    private String givenName;

    private String familyName;

    private Uri imageUrl;

    public CSUser() {

    }

    public CSUser(GoogleSignInAccount googleSignInAccount) {
        setIdToken(googleSignInAccount.getIdToken());
        setIdentifier(googleSignInAccount.getId());
        setDisplayName(googleSignInAccount.getDisplayName());
        setEmail(googleSignInAccount.getEmail());
        setFamilyName(googleSignInAccount.getFamilyName());
        setGivenName(googleSignInAccount.getGivenName());
        setImageUrl(googleSignInAccount.getPhotoUrl());
    }

    public String getIdToken() {
        return idToken;
    }

    public void setIdToken(String idToken) {
        this.idToken = idToken;
    }

    public String getIdentifier() {
        return identifier;
    }

    public void setIdentifier(String identifier) {
        this.identifier = identifier;
    }

    public String getDisplayName() {
        return displayName;
    }

    public void setDisplayName(String displayName) {
        this.displayName = displayName;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getGivenName() {
        return givenName;
    }

    public void setGivenName(String givenName) {
        this.givenName = givenName;
    }

    public String getFamilyName() {
        return familyName;
    }

    public void setFamilyName(String familyName) {
        this.familyName = familyName;
    }

    public Uri getImageUrl() {
        return imageUrl;
    }

    public void setImageUrl(Uri imageUrl) {
        this.imageUrl = imageUrl;
    }
}
