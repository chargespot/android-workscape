package com.chargespot.sdk.manager.chargespot;

import com.chargespot.sdk.model.CSDistance;

/**
 * Created by kelvinlau on 16-05-31.
 */
public class CSChargeSpotManagerOptions {

    private int maximumRangingDistance;
    private boolean retrieveHookOnRange;

    public CSChargeSpotManagerOptions() {
        this.maximumRangingDistance = CSDistance.FAR;
        this.retrieveHookOnRange = false;
    }

    public int getMaximumRangingDistance() {
        return maximumRangingDistance;
    }

    public void setMaximumRangingDistance(int maximumRangingDistance) {
        this.maximumRangingDistance = maximumRangingDistance;
    }

    public boolean shouldRetrieveHookOnRange() {
        return retrieveHookOnRange;
    }

    public void setRetrieveHookOnRange(boolean retrieveHookOnRange) {
        this.retrieveHookOnRange = retrieveHookOnRange;
    }
}
