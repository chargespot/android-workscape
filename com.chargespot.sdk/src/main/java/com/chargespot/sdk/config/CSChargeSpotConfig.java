/*
    Copyright (C) 2015 ChargeSpot
 */
package com.chargespot.sdk.config;

import android.support.annotation.NonNull;

import com.chargespot.sdk.trace.CSChargeSpotTraceLevel;
import com.chargespot.sdk.trace.CSChargeSpotTraceLogger;

public final class CSChargeSpotConfig {

    private CSChargeSpotTraceLogger mTraceLogger = new CSChargeSpotTraceLogger();

    private String mDevelopmentAppToken;
    private String mProductionAppToken;

    private String mDevelopmentUrl;
    private static final String CHARGESPOT_PRODUCTION_URL = com.chargespot.sdk.BuildConfig.PRODUCTION_SERVER_URL;
    private static final String CHARGESPOT_DEVELOPMENT_URL = com.chargespot.sdk.BuildConfig.DEVELOPMENT_SERVER_URL;

    Boolean mIsProduction;

    /**
     * Default constuctor
     */
    public CSChargeSpotConfig() {
        mDevelopmentAppToken = "";
        mProductionAppToken = "";
        mIsProduction = false;

        mDevelopmentUrl = CHARGESPOT_DEVELOPMENT_URL;
    }

    public CSChargeSpotConfig(String developmentKey, String productionKey, Boolean isProduction) {
        mDevelopmentAppToken = developmentKey;
        mProductionAppToken = productionKey;
        mIsProduction = isProduction;

        mDevelopmentUrl = CHARGESPOT_DEVELOPMENT_URL;

        this.mTraceLogger.Trace(CSChargeSpotTraceLevel.CHARGESPOT_TRACE_DEBUG, "Development Token: " + this.mDevelopmentAppToken);
        this.mTraceLogger.Trace(CSChargeSpotTraceLevel.CHARGESPOT_TRACE_DEBUG, "Production Token: " + this.mProductionAppToken);
        this.mTraceLogger.Trace(CSChargeSpotTraceLevel.CHARGESPOT_TRACE_DEBUG, "Is Production: " + this.mIsProduction);
    }

    /**
     * Retrieves the application token for development API endpoint
     *
     * @return Development API endpoint application token
     */
    public String getDevelopmentAppToken() {
        return this.mDevelopmentAppToken;
    }

    /**
     * Sets the application token for development API endpoint
     *
     * @param developmentKey
     */
    public void setDevelopmentAppToken(@NonNull String developmentKey) {
        this.mDevelopmentAppToken = developmentKey;
    }

    /**
     * Retrieves the application token for production API  endpoint
     *
     * @return Production API endpoint application token
     */
    public String getProductionAppToken() {
        return this.mProductionAppToken;
    }

    /**
     * Sets the application token for production API endpoint
     *
     * @param productionKey
     */
    public void setProductionAppToken(@NonNull String productionKey) {
        this.mProductionAppToken = productionKey;
    }

    /**
     * Check if the current state is production
     *
     * @return Boolean for production state
     */
    public Boolean isProduction() {
        return this.mIsProduction;
    }

    /**
     * Change the state to use production api endpoint and app token
     *
     * @param production
     */
    public void isProduction(@NonNull Boolean production) {
        this.mIsProduction = production;
    }

    /**
     * Get the current development API endpoint Url
     *
     * @return Development API endpoint Url
     */
    public String getDevelopmentUrl() {
        return this.mDevelopmentUrl;
    }

    /**
     * Override the default development URL
     * If the url provided is null, the default development url will be used
     * Please ensure you have the corresponding development key
     *
     * @param developmentUrl
     */
    public void setDevelopmentUrl(String developmentUrl) {
        if (developmentUrl == null) {
            this.mDevelopmentUrl = CHARGESPOT_DEVELOPMENT_URL;
            return;
        }
        this.mDevelopmentUrl = developmentUrl;
    }

    public String getAppTokenForEnvironment() {
        if (this.isProduction()) {
            return this.mProductionAppToken;
        }
        return this.mDevelopmentAppToken;
    }

    public String getAPIEndpointUrlForEnvironment() {
        if (this.isProduction()) {
            return CHARGESPOT_PRODUCTION_URL;
        }
        return this.mDevelopmentUrl;
    }
}
