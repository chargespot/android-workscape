/*
    Copyright (C) 2015 ChargeSpot
 */
package com.chargespot.sdk.manager.api;

import android.support.annotation.Nullable;

import com.chargespot.sdk.api.v1.model.ChargeSpotGroupListModel;
import com.chargespot.sdk.api.v1.model.ChargeSpotRegionListModel;
import com.chargespot.sdk.exceptions.CSInvalidParameterException;

import retrofit2.Callback;

/**
 * ChargeSpot SDK API interface. Used to execute general
 * API calls to retrieve information about the ChargeSpot groups
 * and regions.
 *
 * @version 0.3
 */
public interface IChargeSpotApiManager {
    /**
     * Retrieve the global list of ChargeSpot Regions
     *
     * @param callback Callback to return the list of chargespot regions
     * @throws CSInvalidParameterException
     */
    void listRegions(@Nullable Callback<ChargeSpotRegionListModel> callback) throws CSInvalidParameterException;

    /**
     * Retrieve the global list of ChargeSpot groups.
     *
     * @param callback Callback to return the list of chargespot groups
     * @throws CSInvalidParameterException
     */
    void listGroups(@Nullable Callback<ChargeSpotGroupListModel> callback) throws CSInvalidParameterException;
}
