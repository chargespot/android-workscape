/*
    Copyright (C) 2015 ChargeSpot
 */
package com.chargespot.sdk.model;

import com.google.gson.Gson;

/**
 * Id of a ChargeSpot group
 *
 * @version 0.3
 */
public class CSGroupId {
    /**
     * ID of the group
     */
    public String id;

    /**
     * Convert the model to a JSON string
     */
    public String toString() {
        return new Gson().toJson(this);
    }
}
