package com.chargespot.workscape.Models;

import android.content.Context;
import android.util.Log;

import com.chargespot.sdk.model.CSChargeSpot;
import com.chargespot.workscape.Helpers.DateHelper;
import com.chargespot.workscape.Requests.CSRequestManager;
import com.google.gson.Gson;
import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Created by kelvinlau on 2016-07-20.
 */


public final class CSRoom {
    public static final String TAG = CSRoom.class.getCanonicalName();

    @SerializedName("id")
    private String identifier;
    private String name;

    @SerializedName("google_resource_email")
    private String googleResourceEmail;

    @SerializedName("google_resource_id")
    private String googleResourceId;

    private String[] amenities;
    private int capacity;
    private CSFloor floor;

    private List<CSCalendarEvent> events;

    @SerializedName("ble_uuid")
    private String uuid;

    @SerializedName("ble_major")
    private int major;

    @SerializedName("ble_minor")
    private int minor;

    @SerializedName("image_url")
    private String imageUrl;

    public static final int ROOM_STATE_UNAVAILABLE = -1;
    public static final int ROOM_STATE_AVAILABLE = 0;
    public static final int ROOM_STATE_AVAILABLE_LIMITED = 1;
    public static final int ROOM_STATE_OCCUPIED = 2;

    public CSCalendarEvent getNextEvent() {
        for(CSCalendarEvent event : events) {
            Date currentDate = new Date();

            if (currentDate.after(event.getEnd())) {
                continue;
            } else {
                return event;
            }
        }
        return null;
    }

    public int getState() {
        CSCalendarEvent event = getNextEvent();

        if (event != null) {
            Date currentDate = new Date();

            if (currentDate.after(event.getStart()) && currentDate.before(event.getEnd())) {
                return ROOM_STATE_OCCUPIED;
            }

            long diff = event.getStart().getTime() - currentDate.getTime();
            long diffMinutes = diff / (60 * 1000);
            if (diffMinutes < 60) {
                return  ROOM_STATE_AVAILABLE_LIMITED;
            }
        }

        return ROOM_STATE_AVAILABLE;
    }

    public List<Date> getAvailableTimeSlots() {
        List<Date> timeList = new ArrayList<>();

        int roomState = getState();
        if (roomState == ROOM_STATE_OCCUPIED || roomState == ROOM_STATE_UNAVAILABLE) {
            return timeList;
        }

        Date currentDate = new Date();

        long timeSlots = 4;
        if (!events.isEmpty()) {
            CSCalendarEvent event = getNextEvent();
            long timeDiff = event.getStart().getTime() - currentDate.getTime();
            long diffMinutes = timeDiff / (60 * 1000) % 60;

            if (diffMinutes < 0) {
                return timeList;
            } else if (diffMinutes < 60) {
                timeSlots = diffMinutes / 15;
            }
        }

        for (int i=0; i<timeSlots; i++) {
            Date date = DateHelper.addMinutesToDate(currentDate, i * 15);
            date = DateHelper.roundDateToNearestFifteenMinutes(date);
            timeList.add(date);
        }

        return timeList;
    }

    public void refresh(Context context, final Callback<CSRoom> callback) {
        try {
            CSRequestManager.getInstance(context).getRoom(identifier, new Callback<CSRoom>() {
                @Override
                public void onResponse(Call<CSRoom> call, Response<CSRoom> response) {
                    Log.d(TAG, "Room request successful - " + response.code() + " - " + response.message());
                    if (response.isSuccessful()) {
                        CSRoom.this.updateRoomFromRoom(response.body());
                    }
                    callback.onResponse(call, response);
                }

                @Override
                public void onFailure(Call<CSRoom> call, Throwable t) {
                    Log.d(TAG, "Room request failed - " + t.getLocalizedMessage());
                    callback.onFailure(call, t);

                }
            });
        } catch (Exception e) {
            e.printStackTrace();
        }

    }

    public void updateRoomFromRoom(CSRoom room) {
        identifier = room.identifier;
        name = room.name;

        googleResourceEmail = room.googleResourceEmail;
        googleResourceId = room.googleResourceId;

        amenities = room.amenities;
        capacity = room.capacity;
        floor = room.floor;

        events = room.events;
        for(CSCalendarEvent event : events) {
            Log.d(TAG, "event  - " + event.getCreator().name);
        }

        uuid = room.uuid;
        major = room.major;
        minor = room.minor;
    }

    public boolean isValid() {
        return (identifier != null &&
                name != null &&
                googleResourceEmail != null &&
                googleResourceId != null &&
                uuid != null);
    }

    public boolean equals(CSRoom room) {
        if (room != null && !room.isValid()) {
            return false;
        }
        return (identifier.equals(room.identifier) &&
                name.equals(room.name) &&
                googleResourceEmail.equals(room.googleResourceEmail) &&
                googleResourceId.equals(room.googleResourceId) &&
                uuid.equals(room.uuid)&&
                major == room.major&&
                minor == room.minor);
    }

    public boolean matchesChargeSpot(CSChargeSpot chargeSpot) {
        String chargeSpotUUID = chargeSpot.UUID.replace("-", "").toUpperCase();
        String roomUUID = uuid.replace("-", "").toUpperCase();

        return (roomUUID.equals(chargeSpotUUID)&&
                major == chargeSpot.Major&&
                minor == chargeSpot.Minor);
    }

    public String toJson() {
        return new Gson().toJson(this);
    }

    public static CSRoom fromJson(String json) {
        CSRoom room = new Gson().fromJson(json, CSRoom.class);
        return room;
    }

    public String getIdentifier() {
        return identifier;
    }

    public void setIdentifier(String identifier) {
        this.identifier = identifier;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getGoogleResourceEmail() {
        return googleResourceEmail;
    }

    public void setGoogleResourceEmail(String googleResourceEmail) {
        this.googleResourceEmail = googleResourceEmail;
    }

    public String getGoogleResourceId() {
        return googleResourceId;
    }

    public void setGoogleResourceId(String googleResourceId) {
        this.googleResourceId = googleResourceId;
    }

    public String[] getAmenities() {
        return amenities;
    }

    public void setAmenities(String[] amenities) {
        this.amenities = amenities;
    }

    public int getCapacity() {
        return capacity;
    }

    public void setCapacity(int capacity) {
        this.capacity = capacity;
    }

    public CSFloor getFloor() {
        return floor;
    }

    public void setFloor(CSFloor floor) {
        this.floor = floor;
    }

    public List<CSCalendarEvent> getEvents() {
        return events;
    }

    public void setEvents(List<CSCalendarEvent> events) {
        this.events = events;
    }

    public String getUuid() {
        return uuid;
    }

    public void setUuid(String uuid) {
        this.uuid = uuid;
    }

    public int getMajor() {
        return major;
    }

    public void setMajor(int major) {
        this.major = major;
    }

    public int getMinor() {
        return minor;
    }

    public void setMinor(int minor) {
        this.minor = minor;
    }

    public String getImageUrl() {
        return imageUrl;
    }

    public void setImageUrl(String imageUrl) {
        this.imageUrl = imageUrl;
    }
}


