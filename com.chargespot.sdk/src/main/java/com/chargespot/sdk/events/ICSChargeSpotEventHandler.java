/*
    Copyright (C) 2015 ChargeSpot
 */
package com.chargespot.sdk.events;

import com.chargespot.sdk.api.v1.model.ChargeSpotHookListModel;
import com.chargespot.sdk.exceptions.CSException;
import com.chargespot.sdk.model.CSChargeSpot;

/**
 * ChargeSpot event handler
 *
 * @version 0.5
 */
public interface ICSChargeSpotEventHandler {

    /**
     * Provides information about the chargespot that was found when ranging
     *
     * @param chargeSpot         ChargeSpot beacon that was found during ranging
     */
    void onChargeSpotRanged(CSChargeSpot chargeSpot);

    /**
     * ChargeSpot event has been successfully tracked.
     *
     * @param event         Kind of the event that occurred
     * @param hookListModel List of hook models
     */
    void onChargeSpotEventSuccess(CSChargeSpotEvent event, ChargeSpotHookListModel hookListModel);

    /**
     * An exception has been found during the tracking of
     * ChargeSpot event.
     *
     * @param e Exception that occurred while trying to
     *          track a ChargeSpot.
     */
    void onChargeSpotEventFailure(CSException e);
}
