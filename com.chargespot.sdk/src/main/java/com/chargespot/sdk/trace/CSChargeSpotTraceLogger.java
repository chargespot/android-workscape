/*
    Copyright (C) 2015 ChargeSpot
 */
package com.chargespot.sdk.trace;

import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.util.Log;

/**
 * Base trace logger
 * <p/>
 * Date: 05/12/2015
 *
 * @version 0.3
 */
public final class CSChargeSpotTraceLogger
        implements ICSChargeSpotTraceLogger {
    private final static String NULL = "null";
    private final static String CHARGE_SPOT_SDK = "CHARGE_SPOT_SDK";

    /**
     * Generic logging interface
     *
     * @param level   Type of the trace message
     * @param message Message to be logged
     */
    @Override
    public void Trace(@Nullable final CSChargeSpotTraceLevel level, @Nullable final String message) {
        if (null == level) {
            return;
        }

        switch (level) {
            case CHARGESPOT_TRACE_ERROR:
                Log.e(CHARGE_SPOT_SDK, message != null ? message : NULL);
                break;
            case CHARGESPOT_TRACE_WARNING:
                Log.w(CHARGE_SPOT_SDK, message != null ? message : NULL);
                break;
            case CHARGESPOT_TRACE_DEBUG:
                Log.d(CHARGE_SPOT_SDK, message != null ? message : NULL);
                break;
        }
    }
}
