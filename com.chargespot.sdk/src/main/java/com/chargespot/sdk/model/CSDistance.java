package com.chargespot.sdk.model;

/**
 * Created by kelvinlau on 16-06-01.
 */
public class CSDistance {
    /**
     * With approx 10-50 meters
     */
    public static final int FAR = 0X2;

    /**
     * Within approx 1-10 meters
     */
    public static final int NEAR = 0X1;

    /**
     * Within approx. 1 meter
     */
    public static final int IMMEDIATE = 0x0;
}
