/*
    Copyright (C) 2015 ChargeSpot
 */
package com.chargespot.sdk.discovery;

import android.support.annotation.NonNull;

import com.chargespot.sdk.model.CSChargeSpotRadio;

/**
 * Interface for discovery of a ChargeSpot device
 * <p/>
 * Date: 10/12/2015
 *
 * @version 0.5
 */
public interface ICSChargeSpotDiscoveryCallback {
    /**
     * Handle a discovered ChargeSpot device
     *
     * @param deviceConnection Connection to a ChargeSpot device
     *                         that was discovered.
     */
    void discoveredChargeSpotDevice(@NonNull CSChargeSpotRadio deviceConnection);
}
