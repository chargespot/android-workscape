/*
    Copyright (C) 2015 ChargeSpot
 */
package com.chargespot.sdk.api.v1.model;

import com.chargespot.sdk.model.CSGroup;

/**
 * Region of ChargeSpot groups
 *
 * @version 0.5
 */
public final class ChargeSpotGroupListModel {
    /**
     * List of ChargeSpot groups
     */
    public CSGroup[] groups;
}
