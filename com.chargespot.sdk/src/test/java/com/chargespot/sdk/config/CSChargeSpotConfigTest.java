package com.chargespot.sdk.config;

import android.test.suitebuilder.annotation.SmallTest;

import junit.framework.Assert;
import junit.framework.TestCase;

/**
 * Created by kelvinlau on 16-04-19.
 */
public class CSChargeSpotConfigTest extends TestCase {

    private String DEFAULT_PRODUCTION_URL = "https://connect.chargespot.com/api/v1/";
    private String DEFAULT_DEVELOPMENT_URL = "https://connect-uat.chargespot.com/api/v1/";
    private String DEFAULT_UUID = "64F941FA087111E5A6C01697F925EC7B";

    @SmallTest
    public void testEmptyInitialization() {
        CSChargeSpotConfig config = new CSChargeSpotConfig();

        Assert.assertNotNull(config);
        Assert.assertEquals("", config.getDevelopmentAppToken());
        Assert.assertEquals("", config.getProductionAppToken());
        Assert.assertFalse(config.isProduction());
        Assert.assertEquals(DEFAULT_DEVELOPMENT_URL, config.getDevelopmentUrl());
    }

    @SmallTest
    public void testInitialization() {
        CSChargeSpotConfig config = new CSChargeSpotConfig("abc", "123", true);

        Assert.assertNotNull(config);
        Assert.assertEquals("abc", config.getDevelopmentAppToken());
        Assert.assertEquals("123", config.getProductionAppToken());
        Assert.assertTrue(config.isProduction());
        Assert.assertEquals(DEFAULT_DEVELOPMENT_URL, config.getDevelopmentUrl());
    }

    @SmallTest
    public void testSetDevelopmentAppToken() {
        CSChargeSpotConfig config = new CSChargeSpotConfig("abc", "123", true);
        config.setDevelopmentAppToken("111");

        Assert.assertEquals("111", config.getDevelopmentAppToken());
    }

    @SmallTest
    public void testSetDevelopmentAppTokenWithNull() {
        CSChargeSpotConfig config = new CSChargeSpotConfig("abc", "123", true);
        config.setDevelopmentAppToken(null);

        Assert.assertNull(config.getDevelopmentAppToken());
    }

    @SmallTest
    public void testSetProductionAppToken() {
        CSChargeSpotConfig config = new CSChargeSpotConfig("abc", "123", false);
        config.setProductionAppToken("zzz");

        Assert.assertEquals("zzz", config.getProductionAppToken());
    }

    @SmallTest
    public void testSetProductionAppTokenWithNull() {
        CSChargeSpotConfig config = new CSChargeSpotConfig("abc", "123", false);
        config.setProductionAppToken(null);

        Assert.assertNull(config.getProductionAppToken());
    }

    @SmallTest
    public void testSetDevelopmentUrl() {
        String newUrl = "https://hello.com";
        CSChargeSpotConfig config = new CSChargeSpotConfig("abc", "123", false);
        Assert.assertNotSame(newUrl, config.getDevelopmentUrl());

        config.setDevelopmentUrl(newUrl);
        Assert.assertEquals(newUrl, config.getDevelopmentUrl());
    }

    @SmallTest
    public void testSetDevelopmentUrlWithNull() {
        CSChargeSpotConfig config = new CSChargeSpotConfig("abc", "123", false);
        Assert.assertNotNull(config.getDevelopmentUrl());

        config.setDevelopmentUrl("SomeUrl");
        config.setDevelopmentUrl(null);
        Assert.assertNotNull(config.getDevelopmentUrl());
        Assert.assertEquals(DEFAULT_DEVELOPMENT_URL, config.getDevelopmentUrl());
    }

    @SmallTest
    public void testSetIsProduction() {
        CSChargeSpotConfig config = new CSChargeSpotConfig("abc", "123", false);

        config.isProduction(true);
        Assert.assertTrue(config.isProduction());

        config.isProduction(false);
        Assert.assertFalse(config.isProduction());
    }

    @SmallTest
    public void testGetAppTokenForEnvironment() {
        CSChargeSpotConfig config = new CSChargeSpotConfig("abc", "123", false);

        String apptoken = config.getAppTokenForEnvironment();
        Assert.assertEquals(apptoken, "abc");
    }

    @SmallTest
    public void testGetAppTokenForEnvironmentForProduction() {
        CSChargeSpotConfig config = new CSChargeSpotConfig("abc", "123", false);
        config.isProduction(true);

        String apptoken = config.getAppTokenForEnvironment();
        Assert.assertEquals(apptoken, "123");
    }

    @SmallTest
    public void testGetAPIEndpointForEnvironment() {
        CSChargeSpotConfig config = new CSChargeSpotConfig("abc", "123", false);

        String url = config.getAPIEndpointUrlForEnvironment();
        Assert.assertEquals(url, DEFAULT_DEVELOPMENT_URL);
    }

    @SmallTest
    public void testGetAPIEndpointForEnvironmentForProduction() {
        CSChargeSpotConfig config = new CSChargeSpotConfig("abc", "123", false);
        config.isProduction(true);

        String url = config.getAPIEndpointUrlForEnvironment();
        Assert.assertEquals(url, DEFAULT_PRODUCTION_URL);
    }
}
