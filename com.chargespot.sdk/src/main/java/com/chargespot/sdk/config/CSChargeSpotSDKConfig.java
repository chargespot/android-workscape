/*
    Copyright (C) 2015 ChargeSpot
 */
package com.chargespot.sdk.config;

/**
 * ChargeSpot definitions
 *
 * @version 0.5
 */
public final class CSChargeSpotSDKConfig {
    private CSChargeSpotSDKConfig() {
    }

    public final static int CHARGESPOT_BLE_SCAN_TIMEOUT = 15 * 1000;

    public final static int CHARGESPOT_GROUP_SCAN_PERIOD = 10 * 60 * 1000;

    public final static int CHARGESPOT_DEVICE_TIMEOUT = 30 * 1000;

    public final static int CHARGESPOT_EVENT_DELAY_OFFLINE = 10 * 1000;
}
