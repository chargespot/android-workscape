/*
    Copyright (C) 2015 ChargeSpot
 */
package com.chargespot.sdk.model;

import com.google.gson.Gson;

/**
 * ChargeSpot Hook model
 *
 * @version 0.3
 */
public final class CSHook {
    /**
     * ID of the ChargeSpot hook
     */
    public String id;

    /**
     * Name of the ChargeSpot hook
     */
    public String name;

    /**
     * Action to be executed by the application
     */
    public int action;

    /**
     * Action URL associated with the ChargeSpot
     */
    public String action_url;

    /**
     * Charging delay before the action should be executed
     */
    public int action_delay;

    /**
     * Notification text to be shown
     */
    public String notification_text;

    /**
     * Notification title to be shown
     */
    public String notification_title;

    /**
     * Convert the model to a JSON string
     */
    public String toString() {
        return new Gson().toJson(this);
    }
}
