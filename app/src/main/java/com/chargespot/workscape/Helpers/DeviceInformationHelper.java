package com.chargespot.workscape.Helpers;

import android.content.Context;
import android.os.Build;
import android.provider.Settings;
import android.support.annotation.Nullable;

import com.chargespot.workscape.BuildConfig;

import java.util.Locale;

/**
 * Created by kelvinlau on 2016-07-20.
 */

public class DeviceInformationHelper {

    private int mOSSDKVersion;
    private String mDeviceUID;
    private String mManufacturer;
    private String mBuildModel;
    private String mDeviceLanguage;
    private String mChargeSpotSDKVersion;

    {
        mOSSDKVersion = -1;
        mDeviceUID = "DEFAULT_UID";
        mManufacturer = "DEFAULT_MANUFACTURER";
        mBuildModel = "DEFAULT_MODEL";
        mDeviceLanguage = "DEFAULT_LANGUAGE";
        mChargeSpotSDKVersion = "DEFAULT_SDK_VERSION";
    }

    /**
     * Setup the information
     */
    public DeviceInformationHelper(final Context context) {
        // SDK Version
        this.mOSSDKVersion = Build.VERSION.SDK_INT;

        // Manufacturer
        if (android.os.Build.MANUFACTURER != null) {
            this.mManufacturer = android.os.Build.MANUFACTURER;
        }

        // Build Model
        if (android.os.Build.MODEL != null) {
            this.mBuildModel = android.os.Build.MODEL;
        }

        if (null == context) {
            return;
        }

        // UUID of Android device
        if (Settings.Secure.getString(context.getContentResolver(), Settings.Secure.ANDROID_ID) != null) {
            this.mDeviceUID = Settings.Secure.getString(context.getContentResolver(), Settings.Secure.ANDROID_ID);
        }


        // Device Language
        this.mDeviceLanguage = Locale.getDefault().toString();

        // ChargeSpot SDK Version
        this.mChargeSpotSDKVersion = BuildConfig.VERSION_NAME;
    }

    /**
     * Get the device UID
     */
    @Nullable
    public String getDeviceUID() {
        return this.mDeviceUID;
    }

    @Nullable
    public String getFormattedDeviceInformation() {
        return this.mManufacturer + ";" + this.mBuildModel + ";" + this.mOSSDKVersion + ";" + this.mChargeSpotSDKVersion + ";";
    }

    @Nullable
    public String getDeviceLanguagePreference() {
        return this.mDeviceLanguage;
    }
}
