/*
    Copyright (C) 2015 ChargeSpot
 */
package com.chargespot.sdk.state;

import com.chargespot.sdk.model.CSChargeSpot;

/**
 * Device state handler interface
 * <p/>
 * Date: 10/12/2015
 *
 * @version 0.3
 */
public interface ICSDeviceStateHandler {
    void onDeviceStateRetrievedSuccess(CSDeviceState state, CSChargeSpot chargeSpot);
}
