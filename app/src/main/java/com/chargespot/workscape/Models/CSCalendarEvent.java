package com.chargespot.workscape.Models;

import android.content.Context;
import android.util.Log;
import android.widget.Toast;

import com.chargespot.workscape.CSRouter;
import com.chargespot.workscape.Requests.CSRequestManager;
import com.google.gson.Gson;
import com.google.gson.annotations.SerializedName;

import java.util.Date;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Created by kelvinlau on 2016-07-26.
 */

public class CSCalendarEvent {
    public static final String TAG = CSCalendarEvent.class.getCanonicalName();

    private String summary;
    private Date start;
    private Date end;

    @SerializedName("creator_user")
    private CSCalendarEventCreator creator;

    @SerializedName("organizer_user")
    private CSCalendarEventCreator organizer;

    @SerializedName("id")
    public String identifier;

    public static void createEvent(final Context context, final CSRoom room, Date startDate, Date endDate, final Callback<CSCalendarEvent> callback) {
        String userName = CSRouter.getInstance().getGoogleAccount().getDisplayName();
        String summary = "Reserved by " + userName;
        createEvent(context, room, summary, startDate, endDate, callback);
    }

    public static void createEvent(final Context context, final CSRoom room, String summary, Date startDate, Date endDate, final Callback<CSCalendarEvent> callback) {
        try {
            CSRequestManager.getInstance(context).createEvent(room.getIdentifier(), summary, startDate, endDate, new Callback<CSCalendarEvent>() {
                @Override
                public void onResponse(Call<CSCalendarEvent> call, Response<CSCalendarEvent> response) {
                    Log.d(TAG, "Book now request success - " + response.code() + " - " + response.message());
                    if (response.isSuccessful()) {
                        Toast.makeText(context, "Successfully booked " + room.getName(), Toast.LENGTH_SHORT).show();
                    } else {
                        handleCreateEventError(context, response.code());
                    }
                    callback.onResponse(call, response);
                }

                @Override
                public void onFailure(Call<CSCalendarEvent> call, Throwable t) {
                    Log.d(TAG, "Book now request failed - " + t.getLocalizedMessage());
                    handleCreateEventError(context, 0);
                    callback.onFailure(call, t);

                }
            });
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private static  void handleCreateEventError(Context context, int code) {
        if (code == 409) {
            Toast.makeText(context, "There is a conflicting meeting. Please try again.", Toast.LENGTH_SHORT).show();
        } else {
            Toast.makeText(context, "There was a problem creating your event. Please try again later.", Toast.LENGTH_SHORT).show();
        }
    }

    public boolean canEndNow() {
        if (organizer == null) {
            return false;
        }

        Date currentTime = new Date();

        if (currentTime.before(start) || currentTime.after(end)) {
            return false;
        }

        String currentUserEmail = CSRouter.getInstance().getCurrentUser().getEmail();
        return (currentUserEmail.equals(organizer.email));
    }

    public String toJson() {
        return new Gson().toJson(this);
    }

    public static CSCalendarEvent fromJson(String json) {
        CSCalendarEvent event = new Gson().fromJson(json, CSCalendarEvent.class);
        return event;
    }

    public String getSummary() {
        return summary;
    }

    public void setSummary(String summary) {
        this.summary = summary;
    }

    public Date getStart() {
        return start;
    }

    public void setStart(Date start) {
        this.start = start;
    }

    public Date getEnd() {
        return end;
    }

    public void setEnd(Date end) {
        this.end = end;
    }

    public CSCalendarEventCreator getCreator() {
        return creator;
    }

    public void setCreator(CSCalendarEventCreator creator) {
        this.creator = creator;
    }

    public CSCalendarEventCreator getOrganizer() {
        return organizer;
    }

    public void setOrganizer(CSCalendarEventCreator organizer) {
        this.organizer = organizer;
    }
}

class CSCalendarEventCreator {
    @SerializedName("id")
    public String identifier;
    public String name;
    public String email;
}

