package com.chargespot.workscape;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentStatePagerAdapter;
import android.support.v4.view.PagerAdapter;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;

import com.chargespot.workscape.Helpers.MixpanelHelper;
import com.chargespot.workscape.Helpers.SharedPreferencesHelper;

/**
 * Created by kelvinlau on 2016-08-23.
 */

public class OnboardingActivity extends AppCompatActivity implements
        OnboardingMeetingFragment.OnboardingMeetingListener,
        OnboardingProfileFragment.OnboardingProfileSubmitListener {

    public static final String TAG = OnboardingActivity.class.getCanonicalName();

    private static final int NUM_PAGES = 2;

    private ViewPager mPager;

    private PagerAdapter mPagerAdapter;

    private Fragment mMeetingFragment;
    private Fragment mProfileFragment;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        int floor = 0;
        floor = SharedPreferencesHelper.getCurretUserFloor(this);
        if (floor != 0) {
            startHomeActivity();
            return;
        }

        setContentView(R.layout.activity_onboarding);

        mPager = (ViewPager) findViewById(R.id.pager);
        mPagerAdapter = new ScreenSlidePagerAdapter(getSupportFragmentManager());
        mPager.setAdapter(mPagerAdapter);

        mMeetingFragment = createOnboardingMeetingFragment();
        mProfileFragment = createOnboardingProfileFragment();

        MixpanelHelper.track(this, "OnboardingActivity - Onboarding");
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        MixpanelHelper.flush(this);
    }

    @Override
    public void onBackPressed() {
        if (mPager.getCurrentItem() > 0) {
            mPager.setCurrentItem(mPager.getCurrentItem() - 1, true);
            return;
        }

        super.onBackPressed();
    }

    /**
     * A simple pager adapter that represents 5 ScreenSlidePageFragment objects, in
     * sequence.
     */
    private class ScreenSlidePagerAdapter extends FragmentStatePagerAdapter {
        public ScreenSlidePagerAdapter(FragmentManager fm) {
            super(fm);
        }

        @Override
        public Fragment getItem(int position) {
            Fragment fragment = null;
            switch(position) {
                case 0:
                    fragment = mMeetingFragment;
                    break;
                case 1:
                    fragment = mProfileFragment;
            }
            return fragment;
        }

        @Override
        public int getCount() {
            return NUM_PAGES;
        }
    }

    private Fragment createOnboardingMeetingFragment() {
        OnboardingMeetingFragment meetingFragment = new OnboardingMeetingFragment();
        meetingFragment.setOnboardingMeetingListener(this);
        return meetingFragment;
    }

    private Fragment createOnboardingProfileFragment() {
        OnboardingProfileFragment profileFragment = new OnboardingProfileFragment();
        profileFragment.setOnboardingProfileSubmitListener(this);
        return profileFragment;
    }

    private void startHomeActivity() {
        Intent intent = new Intent(this, BaseActivity.class);
        startActivity(intent);
        finish();
    }


    @Override
    public void onNextClick() {
        mPager.setCurrentItem(mPager.getCurrentItem() + 1, true);
    }

    @Override
    public void onSubmit() {
        startHomeActivity();
    }
}
