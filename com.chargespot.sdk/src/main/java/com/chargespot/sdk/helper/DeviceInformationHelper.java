/*
    Copyright (C) 2015 ChargeSpot
 */
package com.chargespot.sdk.helper;

import android.content.Context;
import android.os.Build;
import android.provider.Settings;
import android.support.annotation.Nullable;

import com.chargespot.sdk.BuildConfig;
import com.chargespot.sdk.trace.CSChargeSpotTraceLevel;
import com.chargespot.sdk.trace.ICSChargeSpotTraceLogger;

import java.util.Locale;

/**
 * Device information interface
 * <p/>
 * Date: 10/17/2015
 *
 * @version 0.5
 */
public final class DeviceInformationHelper
        implements IDeviceInformationHelper {
    //region Trace
    private ICSChargeSpotTraceLogger mTraceLogger;
    //endregion

    //region Internal
    private int mOSSDKVersion;
    private String mDeviceUID;
    private String mManufacturer;
    private String mBuildModel;
    private String mDeviceLanguage;
    private String mChargeSpotSDKVersion;

    {
        mOSSDKVersion = -1;
        mDeviceUID = "DEFAULT_UID";
        mManufacturer = "DEFAULT_MANUFACTURER";
        mBuildModel = "DEFAULT_MODEL";
        mDeviceLanguage = "DEFAULT_LANGUAGE";
        mChargeSpotSDKVersion = "DEFAULT_SDK_VERSION";
    }
    //endregion

    //region Constructors

    /**
     * Setup the information
     */
    public DeviceInformationHelper(final Context context, final ICSChargeSpotTraceLogger traceLogger) {
        this.mTraceLogger = traceLogger;

        // SDK Version
        this.mOSSDKVersion = Build.VERSION.SDK_INT;

        // Manufacturer
        if (android.os.Build.MANUFACTURER != null) {
            this.mManufacturer = android.os.Build.MANUFACTURER;
        }

        // Build Model
        if (android.os.Build.MODEL != null) {
            this.mBuildModel = android.os.Build.MODEL;
        }

        this.mTraceLogger.Trace(CSChargeSpotTraceLevel.CHARGESPOT_TRACE_DEBUG, "Device OS SDK Version:" + Integer.toString(this.mOSSDKVersion));
        this.mTraceLogger.Trace(CSChargeSpotTraceLevel.CHARGESPOT_TRACE_DEBUG, "Manufacturer:" + mManufacturer);
        this.mTraceLogger.Trace(CSChargeSpotTraceLevel.CHARGESPOT_TRACE_DEBUG, "Model:" + mBuildModel);

        if (null == context) {
            this.mTraceLogger.Trace(CSChargeSpotTraceLevel.CHARGESPOT_TRACE_WARNING, "Context is null");
            return;
        }

        // UUID of Android device
        if (Settings.Secure.getString(context.getContentResolver(), Settings.Secure.ANDROID_ID) != null) {
            this.mDeviceUID = Settings.Secure.getString(context.getContentResolver(), Settings.Secure.ANDROID_ID);
        }

        this.mTraceLogger.Trace(CSChargeSpotTraceLevel.CHARGESPOT_TRACE_DEBUG, "Device UID:" + this.mDeviceUID);

        // Device Language
        this.mDeviceLanguage = Locale.getDefault().toString();
        this.mTraceLogger.Trace(CSChargeSpotTraceLevel.CHARGESPOT_TRACE_DEBUG, "Device Language:" + this.mDeviceLanguage);

        // ChargeSpot SDK Version
        this.mChargeSpotSDKVersion = BuildConfig.VERSION_NAME;
        this.mTraceLogger.Trace(CSChargeSpotTraceLevel.CHARGESPOT_TRACE_DEBUG, "ChargeSpot SDK Version:" + this.mChargeSpotSDKVersion);
    }
    //endregion

    //region DeviceInformation

    /**
     * Get the device UID
     */
    @Nullable
    public String getDeviceUID() {
        return this.mDeviceUID;
    }

    @Nullable
    public String getFormattedDeviceInformation() {
        return this.mManufacturer + ";" + this.mBuildModel + ";" + this.mOSSDKVersion + ";" + this.mChargeSpotSDKVersion + ";";
    }

    @Nullable
    public String getDeviceLanguagePreference() {
        return this.mDeviceLanguage;
    }
    //endregion
}
