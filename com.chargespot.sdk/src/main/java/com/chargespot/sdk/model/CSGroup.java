/*
    Copyright (C) 2015 ChargeSpot
 */
package com.chargespot.sdk.model;

import com.google.gson.Gson;

/**
 * Region of ChargeSpot groups
 *
 * @version 0.3
 */
public final class CSGroup
        extends CSGroupId {
    /**
     * Name of the ChargeSpot group
     */
    public String name;

    /**
     * Address
     */
    public String address;

    /**
     * Image of the group
     */
    public String image_url;

    /**
     * Latitude of the ChargeSpot group
     */
    public float latitude;

    /**
     * Longitude of the ChargeSpot group
     */
    public float longitude;

    /**
     * Count of devices in the ChargeSpot group
     */
    public int chargespot_count;

    /**
     * Convert the model to a JSON string
     */
    public String toString() {
        return new Gson().toJson(this);
    }
}
