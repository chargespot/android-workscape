/*
    Copyright (C) 2015 ChargeSpot
 */
package com.chargespot.sdk.exceptions;

/**
 * ChargeSpot Unsupported exception
 *
 * @version 0.3
 */
public class CSUnsupportedException
        extends CSException {
    /**
     * ChargeSpot unsupported exception
     *
     * @param message Information exception message
     */
    public CSUnsupportedException(final String message) {
        super(message);
    }
}
