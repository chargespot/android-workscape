/*
    Copyright (C) 2015 ChargeSpot
 */
package com.chargespot.sdk;

import com.chargespot.sdk.manager.chargespot.CSChargeSpotManagerInstance;
import com.chargespot.sdk.manager.chargespot.ICSChargeSpotManagerInstance;

/**
 * ChargeSpot shared interface manager, to
 * provide a consistent view of the interface.
 * <p/>
 * The class is responsible for providing the shared
 * instance to the ChargeSpot SDK. Only one instance of the SDK
 * can be initialized within a single application.
 *
 * @version 0.3
 */
public final class CSChargeSpotManager {
    //region Internal
    private static ICSChargeSpotManagerInstance Instance;
    //endregion

    //region Initialize
    static {
        Instance = null;
    }
    //endregion

    /**
     * CSChargeSpotManager can not be instanced. It should only
     * handle the instance of the manager.
     */
    private CSChargeSpotManager() {
    }

    /**
     * Only one instance of the ChargeSpot manager is allowed in a single application.
     * Create the instance the first time it is accessed, otherwise return the shared instance.
     *
     * @return ChargeSpot shared manager instance used for communicating with the
     * ChargeSpot SDK.
     */
    public synchronized static ICSChargeSpotManagerInstance getInstance() {
        if (Instance == null) {
            Instance = new CSChargeSpotManagerInstance();
        }

        return Instance;
    }
}
