package com.chargespot.workscape;

import com.chargespot.sdk.model.CSChargeSpot;
import com.chargespot.workscape.Models.CSRoom;
import com.chargespot.workscape.Requests.CSSession;
import com.chargespot.workscape.Models.CSUser;
import com.google.android.gms.auth.api.signin.GoogleSignInAccount;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by kelvinlau on 2016-07-21.
 */

public class CSRouter {
    public static String TAG = CSRouter.class.getCanonicalName();
    private static CSRouter mInstance = null;
    private static List<CSRoom> mRoomList;
    private static GoogleSignInAccount mGoogleAccount;
    private static CSUser mCurrentUser;
    private static CSChargeSpot mCurrentChargeSpot;
    private static CSSession mSession;

    {
        mInstance = null;
        mRoomList = null;
        mGoogleAccount = null;
        mCurrentUser = null;
        mCurrentChargeSpot = null;
        mSession = null;
    }

    public static final String mRequestOrigin = "android";

    private CSRouter() {
        mRoomList = new ArrayList<>();
    }

    public static CSRouter getInstance() {
        if (mInstance == null) {
            mInstance = new CSRouter();
        }
        return mInstance;
    }

    public void setRoomList(List<CSRoom> rooms) {
        mRoomList = rooms;
    }

    public List<CSRoom> getRoomList() {
        return mRoomList;
    }

    public void updateRoom(CSRoom room) {
        for (CSRoom r : mRoomList) {
            if (r.equals(room)) {
                r.updateRoomFromRoom(room);
                break;
            }
        }
    }

    public void setGoogleAccount(GoogleSignInAccount googleAccount) {
        mGoogleAccount = googleAccount;
        setCurrentUser(new CSUser(googleAccount));
    }

    public GoogleSignInAccount getGoogleAccount() {
        return mGoogleAccount;
    }


    public void setCurrentUser(CSUser user) {
        mCurrentUser = user;
    }

    public CSUser getCurrentUser() {
        return mCurrentUser;
    }

    public void setSession(CSSession session) {
        mSession = session;
    }

    public CSSession getSession() {
        return mSession;
    }

    public String getAuthToken() {
        if (mSession == null) {
            return null;
        }
        return mSession.authToken;
    }

    public String getRequestOrigin() {
        return mRequestOrigin;
    }

    public void setCurrentChargeSpot(CSChargeSpot chargeSpot) {
        mCurrentChargeSpot = chargeSpot;
    }

    public CSChargeSpot getCurrentChargeSpot() {
        return mCurrentChargeSpot;
    }

    public CSRoom getRoomForChargeSpot(CSChargeSpot chargeSpot) {
        for (CSRoom room : mRoomList) {
            if (room.matchesChargeSpot(chargeSpot)) {
                return room;
            }
        }
        return null;
    }
}
