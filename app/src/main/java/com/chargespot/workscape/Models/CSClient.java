package com.chargespot.workscape.Models;

import com.google.gson.annotations.SerializedName;

/**
 * Created by kelvinlau on 2016-08-25.
 */

public class CSClient {

    @SerializedName("id")
    private String identifier;
    private String name;

    @SerializedName("image_url")
    private String imageUrl;

    public String getIdentifier() {
        return identifier;
    }

    public void setIdentifier(String identifier) {
        this.identifier = identifier;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getImageUrl() {
        return imageUrl;
    }

    public void setImageUrl(String imageUrl) {
        this.imageUrl = imageUrl;
    }
}
