package com.chargespot.workscape.Requests;

import okhttp3.MultipartBody;
import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.http.Header;
import retrofit2.http.Multipart;
import retrofit2.http.POST;
import retrofit2.http.Part;

/**
 * Created by kelvinlau on 2016-07-28.
 */

public interface FileUploadService {
    @Multipart
    @POST("tickets")
    Call<ResponseBody> uploadSupportTicket(
            @Header("X-CSAuth-Token") String token,
            @Header("X-Device-Identifier") String deviceUid,
            @Header("X-User-Agent") String userAgent,
            @Header("X-SDK-Version") String sdkVersion,
            @Header("X-User-Language") String contentLanguage,
            @Header("X-Request-Origin") String requestOrigin,
            @Part("app_info") String appInfo,
            @Part("user_email") String email,
            @Part("location_name") String location,
            @Part("category_type_id") int categoryType,
            @Part("message") String message,
            @Part MultipartBody.Part image);
}
