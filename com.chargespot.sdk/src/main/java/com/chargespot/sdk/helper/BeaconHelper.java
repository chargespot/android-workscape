package com.chargespot.sdk.helper;

public class BeaconHelper {

    private BeaconHelper() {

    }

    public static String formatUUID(String uuid) {
        if (uuid == null) {
            return null;
        }

        if (uuid.length() < 20) {
            return uuid;
        }

        String formattedUUID = String.format("%s-%s-%s-%s-%s", uuid.substring(0, 8), uuid.substring(8, 12), uuid.substring(12, 16), uuid.substring(16, 20), uuid.substring(20, 32)).toUpperCase();
        return formattedUUID;
    }

    public static String formatUUIDForFilter(String uuid) {
        if (uuid == null) {
            return null;
        }

        return uuid.replace("-", "").toUpperCase();
    }
}
