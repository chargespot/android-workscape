/*
    Copyright (C) 2015 ChargeSpot
 */
package com.chargespot.sdk.discovery.lollipop;

import android.Manifest;
import android.annotation.TargetApi;
import android.bluetooth.BluetoothAdapter;
import android.bluetooth.BluetoothManager;
import android.bluetooth.le.BluetoothLeScanner;
import android.bluetooth.le.ScanCallback;
import android.bluetooth.le.ScanFilter;
import android.bluetooth.le.ScanResult;
import android.bluetooth.le.ScanSettings;
import android.content.Context;
import android.content.pm.PackageManager;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.util.Log;

import com.chargespot.sdk.CSChargeSpotManager;
import com.chargespot.sdk.api.v1.model.ChargeSpotRegionListModel;
import com.chargespot.sdk.api.v1.model.ChargeSpotRegionModel;
import com.chargespot.sdk.discovery.ICSChargeSpotDiscoveryCallback;
import com.chargespot.sdk.discovery.ICSChargeSpotDiscoveryService;
import com.chargespot.sdk.exceptions.CSBluetoothErrors;
import com.chargespot.sdk.exceptions.CSBluetoothException;
import com.chargespot.sdk.exceptions.CSInvalidParameterException;
import com.chargespot.sdk.exceptions.CSUnsupportedException;
import com.chargespot.sdk.helper.HexConverterHelper;
import com.chargespot.sdk.helper.BeaconHelper;
import com.chargespot.sdk.model.CSChargeSpot;
import com.chargespot.sdk.model.CSChargeSpotRadio;
import com.chargespot.sdk.trace.CSChargeSpotTraceLevel;
import com.chargespot.sdk.trace.ICSChargeSpotTraceLogger;

import java.nio.ByteBuffer;
import java.util.ArrayList;
import java.util.List;

/**
 * ChargeSpot handler interface for handling
 * all of the events being received from the devices, and
 * executing proper actions related to them.
 *
 * @version 0.3
 */
@TargetApi(21)
public final class CSChargeSpotDiscoveryService
        implements ICSChargeSpotDiscoveryService {
    private Context mContext;

    //region Bluetooth
    private BluetoothManager mBluetoothManager;
    private BluetoothAdapter mBluetoothAdapter;
    private BluetoothLeScanner mBluetoothLeScanner;
    private ScanSettings mScanSettings;
    private boolean mScanning;
    //endregion

    //region Interfaces
    private ICSChargeSpotDiscoveryCallback mChargeSpotCallback;
    private ICSChargeSpotTraceLogger mTraceLogger;
    //endregion

    //endregion

    {
        mTraceLogger = null;
        mContext = null;
        mChargeSpotCallback = null;
        mBluetoothAdapter = null;
        mBluetoothManager = null;
        mBluetoothLeScanner = null;
        mScanSettings = null;
    }

    //region BleScanFilter

    /**
     * BLE scanner filter
     */
    class BleScanFilter {
        //region ChargeSpot
        private final byte[] CS_BEACON_ID =     {9, 9, 9, 9, 9, 9, 9, 9, 9, 9, 9, 9, 9, 9, 9, 9, 9, 9, 9, 9, 9, 9, 9};

        // Set 1 for bytes that will need to be filtered
        private final byte[] CS_BEACON_MASK =   {0, 0, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 0, 0, 0, 0, 0};
        //endregion



        private ArrayList<ScanFilter> filters = new ArrayList<>();

        {
            List<String> filterList = new ArrayList<>();
            ChargeSpotRegionListModel regionList = CSChargeSpotManager.getInstance().getRegionList();
            if (regionList != null) {
                for (ChargeSpotRegionModel region : regionList.chargespot_regions) {
                    byte[] beaconIDFilter = CS_BEACON_ID.clone();

                    byte[] convertedByteArr = HexConverterHelper.convertHexToBytes(BeaconHelper.formatUUIDForFilter(region.uuid));
                    System.arraycopy(convertedByteArr, 0, beaconIDFilter, 2, convertedByteArr.length);

                    // Don't add duplicate filters
                    String filterString = region.uuid + region.major;
                    if (filterList.contains(filterString)) {
                        continue;
                    }

                    ScanFilter filter = new ScanFilter.Builder().setManufacturerData(76, beaconIDFilter, CS_BEACON_MASK).build();
                    filters.add(filter);
                    filterList.add(filterString);
                }
            }
        }

        public ArrayList<ScanFilter> getScanFilters() {
            return filters;
        }
    }
    //endregion

    //region BLEScanCallback
    /**
     * BLE scanner callback
     */
    private final ScanCallback mScanCallback = new ScanCallback() {
        @Override
        public void onScanResult(final int callbackType, ScanResult result) {
            super.onScanResult(callbackType, result);
            if (result == null || result.getDevice() == null || result.getScanRecord() == null) {
                return;
            }

            if (CSChargeSpotDiscoveryService.this.mChargeSpotCallback == null) {
                return;
            }

            byte[] scanRecord = result.getScanRecord().getManufacturerSpecificData(76);

            final String UUID;
            try {
                UUID = HexConverterHelper.convertBytesToHex(scanRecord, 2, 16);
            } catch (CSInvalidParameterException e) {
                CSChargeSpotDiscoveryService.this.mTraceLogger.Trace(CSChargeSpotTraceLevel.CHARGESPOT_TRACE_ERROR, e.getMessage());
                return;
            }

            final int major = (((scanRecord[18]) & 0xFF) << 8) + (scanRecord[19] & 0xFF);
            final int minor = (((scanRecord[20]) & 0xFF) << 8) + (scanRecord[21] & 0xFF);
            final int txPower = ((byte) (scanRecord[22] & 0xFF));

            final CSChargeSpot device = new CSChargeSpot();

            device.Address = result.getDevice().getAddress();
            device.UUID = UUID;
            device.Major = major;
            device.Minor = minor;

            CSChargeSpotRadio deviceConnection = new CSChargeSpotRadio(device, result.getRssi(), txPower);
            CSChargeSpotDiscoveryService.this.mChargeSpotCallback.discoveredChargeSpotDevice(deviceConnection);
        }

        @Override
        public void onScanFailed(int error) {
            super.onScanFailed(error);
        }
    };
    //endregion

    /**
     * Create a CS discovery service
     *
     * @param traceLogger Trace logger user to handle the events
     * @param context     Application context
     * @throws CSUnsupportedException
     */
    public CSChargeSpotDiscoveryService(@NonNull final ICSChargeSpotTraceLogger traceLogger, @NonNull final Context context)
            throws CSUnsupportedException {
        this.mTraceLogger = traceLogger;
        this.mContext = context;

        this.mBluetoothManager = (BluetoothManager) this.mContext.getSystemService(Context.BLUETOOTH_SERVICE);
        if (this.mBluetoothManager == null) {
            this.mTraceLogger.Trace(CSChargeSpotTraceLevel.CHARGESPOT_TRACE_ERROR, "No BlueTooth Manager.");
            throw new CSUnsupportedException("BluetoothManager");
        }

        this.mBluetoothAdapter = mBluetoothManager.getAdapter();
        if (this.mBluetoothAdapter == null) {
            this.mTraceLogger.Trace(CSChargeSpotTraceLevel.CHARGESPOT_TRACE_ERROR, "No BlueTooth Adapter.");
            throw new CSUnsupportedException("BluetoothAdapter");
        }

        this.mBluetoothLeScanner = this.mBluetoothAdapter.getBluetoothLeScanner();
        if (this.mBluetoothLeScanner == null) {
            this.mTraceLogger.Trace(CSChargeSpotTraceLevel.CHARGESPOT_TRACE_ERROR, "No BlueTooth LE Scanner.");
            throw new CSUnsupportedException("BluetoothLeScanner");
        }

        this.mScanSettings = new ScanSettings.Builder()
                .setScanMode(ScanSettings.SCAN_MODE_BALANCED)
                .build();
    }

    //region DiscoveryInterface

    /**
     * Register a ChargeSpot discovery
     * callback.
     *
     * @param callback Callback for handling discovery events
     */
    @Override
    public synchronized void registerChargeSpotDiscoveryCallback(@Nullable ICSChargeSpotDiscoveryCallback callback) {
        this.mTraceLogger.Trace(CSChargeSpotTraceLevel.CHARGESPOT_TRACE_DEBUG, "Callback registered");
        this.mChargeSpotCallback = callback;
    }

    /**
     * Unregister a ChargeSpot discovery callback
     */
    @Override
    public synchronized void unregisterChargeSpotDiscoveryCallback() {
        this.mTraceLogger.Trace(CSChargeSpotTraceLevel.CHARGESPOT_TRACE_DEBUG, "Callback unregistered");
        this.mChargeSpotCallback = null;
    }

    /**
     * Start the discovery
     */
    @Override
    public synchronized void startDiscovery()
            throws CSBluetoothException {
        if (!mScanning) {
            requestPermissions();

            if (!this.mBluetoothAdapter.isEnabled()) {
                if (!this.mBluetoothAdapter.enable()) {
                    throw new CSBluetoothException("Bluetooth not enabled", CSBluetoothErrors.BluetoothNotEnabled);
                }
            }

            this.mTraceLogger.Trace(CSChargeSpotTraceLevel.CHARGESPOT_TRACE_DEBUG, "Discovery started");
            this.mBluetoothLeScanner.startScan((new BleScanFilter()).getScanFilters(), this.mScanSettings, this.mScanCallback);
            mScanning = true;
        }
    }

    /**
     * Stop the discovery
     */
    @Override
    public synchronized void stopDiscovery() {
        if (mScanning) {
            this.mTraceLogger.Trace(CSChargeSpotTraceLevel.CHARGESPOT_TRACE_DEBUG, "Discovery stopped");
            this.mBluetoothLeScanner.stopScan(this.mScanCallback);
            mScanning = false;
        }
    }
    //endregion

    /**
     * Request Bluetooth and location permissions
     */
    private synchronized void requestPermissions()
            throws CSBluetoothException {
        if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.M) {
            if (this.mContext.checkSelfPermission(Manifest.permission.BLUETOOTH_ADMIN) != PackageManager.PERMISSION_GRANTED) {
                throw new CSBluetoothException(Manifest.permission.BLUETOOTH_ADMIN, CSBluetoothErrors.BluetoothNoPermission);
            }

            if (this.mContext.checkSelfPermission(Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
                throw new CSBluetoothException(Manifest.permission.ACCESS_FINE_LOCATION, CSBluetoothErrors.BluetoothNoPermission);
            }

            if (this.mContext.checkSelfPermission(Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
                throw new CSBluetoothException(Manifest.permission.ACCESS_COARSE_LOCATION, CSBluetoothErrors.BluetoothNoPermission);
            }
        }
    }
}
