/*
    Copyright (C) 2015 ChargeSpot
 */
package com.chargespot.sdk.api.v1.model;

import com.chargespot.sdk.model.CSGroupId;
import com.chargespot.sdk.model.CSHook;

/**
 * ChargeSpot group hook list model
 *
 * @version 0.5
 */
public final class ChargeSpotGroupHookListModel {
    /**
     * Group associated with the ChargeSpot
     */
    public CSGroupId group;

    /**
     * Hooks associated with the ChargeSpot
     */
    public CSHook[] hook;
}
