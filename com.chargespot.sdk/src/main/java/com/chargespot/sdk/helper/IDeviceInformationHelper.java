/*
    Copyright (C) 2015 ChargeSpot
 */
package com.chargespot.sdk.helper;

import android.support.annotation.Nullable;

/**
 * Device information interface
 * <p/>
 * Date: 10/17/2015
 *
 * @version 0.3
 */
public interface IDeviceInformationHelper {
    /**
     * Get the device UID
     */
    @Nullable
    String getDeviceUID();

    @Nullable
    String getFormattedDeviceInformation();

    @Nullable
    String getDeviceLanguagePreference();
}
