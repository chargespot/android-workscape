package com.chargespot.workscape.Helpers;

import android.content.Context;
import android.content.SharedPreferences;

/**
 * Created by kelvinlau on 2016-08-22.
 */

public class SharedPreferencesHelper {

    private final static String CS_SHARED_PREFERENCE_KEY = "CSSharedPreferenceKey";
    public final static String CURRENT_USER_FLOOR = "CurrentUserFloor";

    public static void savePreference(Context context, String key, int value) {
        SharedPreferences preferences = context.getSharedPreferences(CS_SHARED_PREFERENCE_KEY, Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = preferences.edit();
        editor.putInt(key, value);
        editor.commit();
    }

    public static int getPreference(Context context, String key) {
        SharedPreferences preferences = context.getSharedPreferences(CS_SHARED_PREFERENCE_KEY, Context.MODE_PRIVATE);
        int value = preferences.getInt(key, 0);

        return value;
    }

    public static void saveCurrentUserFloor(Context context, int floor) {
        savePreference(context, CURRENT_USER_FLOOR, floor);
    }

    public static int getCurretUserFloor(Context context) {
        return getPreference(context, CURRENT_USER_FLOOR);
    }

    public static void clearSharedPreferences(Context context) {
        SharedPreferences preferences = context.getSharedPreferences(CS_SHARED_PREFERENCE_KEY, Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = preferences.edit();
        editor.clear();
        editor.commit();
    }
}
