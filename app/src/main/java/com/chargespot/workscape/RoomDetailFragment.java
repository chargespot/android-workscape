package com.chargespot.workscape;

import android.os.Bundle;
import android.os.Handler;
import android.support.annotation.Nullable;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TabHost;
import android.widget.TextView;
import android.widget.Toast;

import com.chargespot.workscape.Helpers.AlertDialogHelper;
import com.chargespot.workscape.Helpers.DateHelper;
import com.chargespot.workscape.Helpers.MixpanelHelper;
import com.chargespot.workscape.Helpers.NotificationHelper;
import com.chargespot.workscape.Models.CSRoom;
import com.chargespot.workscape.Models.CSCalendarEvent;
import com.github.clans.fab.FloatingActionButton;
import com.github.clans.fab.FloatingActionMenu;
import com.squareup.picasso.Picasso;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Created by kelvinlau on 2016-07-25.
 */

public class RoomDetailFragment extends BaseFragment
    implements View.OnClickListener {

    public interface BookLaterListener {
        void onBookLaterSelect(CSRoom room);
    }
    private CSRoom mRoom;
    private BookLaterListener mListener;

    private ImageView mHeaderImageView;

    private TextView mNameTextView;
    private TextView mSubtitleTextView;
    private TextView mFloorTextView;
    private TextView mCapacityTextView;
    private TextView mAmenitiesTextView;
    private TextView mEventTextView;
    private TextView mAvailabilityTextView;
    private LinearLayout mEventLinearLayout;

    private FloatingActionMenu mFloatingMenu;
    private FloatingActionButton mBookNowButton;
    private FloatingActionButton mBookLaterButton;

    public RoomDetailFragment() {
    }

    @Override
    public void onResume() {
        super.onResume();
        Toolbar toolbar = (Toolbar) getActivity().findViewById(R.id.toolbar);
        toolbar.setTitle(getTitle());
        refreshRoom();
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_room_detail, container, false);

        mHeaderImageView = (ImageView)view.findViewById(R.id.headerImageView);

        mNameTextView = (TextView) view.findViewById(R.id.roomNameTextView);
        mSubtitleTextView = (TextView)view.findViewById(R.id.subtitleTextView);
        mFloorTextView = (TextView) view.findViewById(R.id.floorTextView);
        mCapacityTextView = (TextView) view.findViewById(R.id.capacityTextView);
        mAmenitiesTextView = (TextView) view.findViewById(R.id.amenitiesTextView);
        mAvailabilityTextView = (TextView) view.findViewById(R.id.availabilityTextView);
        mEventTextView = (TextView) view.findViewById(R.id.eventNameTextView);

        mEventLinearLayout = (LinearLayout) view.findViewById(R.id.eventLinearView);

        mFloatingMenu = (FloatingActionMenu)view.findViewById(R.id.fab_menu);
        mFloatingMenu.setIconAnimated(false);

        mBookNowButton = (FloatingActionButton)view.findViewById(R.id.book_now_fab);
        mBookLaterButton = (FloatingActionButton)view.findViewById(R.id.book_later_fab);
        mBookNowButton.setOnClickListener(this);
        mBookLaterButton.setOnClickListener(this);

        loadHeaderImage();
        setupTabHost(view);
        updateUI();

        return view;
    }

    @Override
    public void onStart() {
        super.onStart();
        MixpanelHelper.track(getActivity(), "RoomDetailFragment - Room Detail");
    }

    public void onRefresh() {
        refreshRoom();
    }

    private void refreshRoom() {
        mRoom.refresh(getActivity(), new Callback<CSRoom>() {
            @Override
            public void onResponse(Call<CSRoom> call, Response<CSRoom> response) {
                if (isAdded()) {
                    updateUI();
                }
            }

            @Override
            public void onFailure(Call<CSRoom> call, Throwable t) {
                Log.d(TAG, "Room detail refresh failed");
            }
        });

    }

    private void loadHeaderImage() {
        Picasso.with(getActivity()).load(mRoom.getImageUrl()).into(mHeaderImageView);
    }

    private void updateUI() {
        mNameTextView.setText(this.mRoom.getName());
        mSubtitleTextView.setText(CSRouter.getInstance().getSession().getClient().getName());
        mFloorTextView.setText(this.mRoom.getFloor().floorName());
        mCapacityTextView.setText("" + this.mRoom.getCapacity());

        String amenitiesString = "";
        if (this.mRoom.getAmenities().length == 0) {
            amenitiesString = "None";
        } else {
            for(String amenity : this.mRoom.getAmenities()) {
                amenitiesString += amenity + "\n";
            }
        }
        mAmenitiesTextView.setText(amenitiesString);

        CSCalendarEvent event = this.mRoom.getNextEvent();
        if (event == null) {
            mEventLinearLayout.setVisibility(View.GONE);
        } else {
            updateEvent(event);
        }

        updateFloatingMenu();
        updateAvailability(this.mRoom.getState());
    }

    private void updateFloatingMenu() {
        int roomState = mRoom.getState();

        mFloatingMenu.setVisibility(View.VISIBLE);
        mBookNowButton.setLabelText(getString(R.string.book_now));
        mBookNowButton.setColorNormal(ContextCompat.getColor(getActivity(), R.color.colorCSBlue));
        mBookNowButton.setColorPressed(ContextCompat.getColor(getActivity(), R.color.colorCSBlue));
        mBookNowButton.setImageDrawable(ContextCompat.getDrawable(getActivity(), R.drawable.fab_book_now_icon));

        if (roomState == CSRoom.ROOM_STATE_UNAVAILABLE) {
            mFloatingMenu.setVisibility(View.GONE);
        } else if (roomState == CSRoom.ROOM_STATE_OCCUPIED) {
            CSCalendarEvent event = this.mRoom.getNextEvent();
            if (event != null && event.canEndNow()) {
                mBookNowButton.setLabelText(getString(R.string.end_now));
                mBookNowButton.setColorNormal(ContextCompat.getColor(getActivity(), R.color.colorCSRed));
                mBookNowButton.setColorPressed(ContextCompat.getColor(getActivity(), R.color.colorCSRed));
                mBookNowButton.setImageDrawable(ContextCompat.getDrawable(getActivity(), R.drawable.fab_end_now_icon));
            } else {
                mBookNowButton.setVisibility(View.GONE);
            }
        }
    }

    public void setRoom(CSRoom room) {
        this.mRoom = room;
    }

    private void setupTabHost(View view) {
        TabHost tabHost = (TabHost) view.findViewById(R.id.tabhost);
        tabHost.setup();

        TabHost.TabSpec spec = tabHost.newTabSpec("Tab 1");
        spec.setContent(R.id.tab1);
        spec.setIndicator("");
        tabHost.addTab(spec);

        spec = tabHost.newTabSpec("Detail Tab");
        spec.setContent(R.id.detailTab);
        spec.setIndicator("Detail");
        tabHost.addTab(spec);

        spec = tabHost.newTabSpec("Tab 3");
        spec.setContent(R.id.tab3);
        spec.setIndicator("");
        tabHost.addTab(spec);

        tabHost.getTabWidget().getChildTabViewAt(0).setEnabled(false);
        tabHost.getTabWidget().getChildTabViewAt(2).setEnabled(false);
        tabHost.setCurrentTab(1);

    }

     private void updateEvent(CSCalendarEvent event) {
         String eventString = "NEXT MEETING\n";
         SimpleDateFormat formatter = new SimpleDateFormat("hh:mm a");
         eventString += formatter.format(event.getStart()) + " to " + formatter.format(event.getEnd()) + "\n";
         eventString += event.getSummary();
         mEventTextView.setText(eventString);
     }

     private void updateAvailability(int availability) {

        if (availability == CSRoom.ROOM_STATE_AVAILABLE) {
            mAvailabilityTextView.setText(getString(R.string.room_state_available).toUpperCase());
            mAvailabilityTextView.setBackgroundColor(ContextCompat.getColor(getActivity(), R.color.colorCSTeal));
        } else if (availability == CSRoom.ROOM_STATE_AVAILABLE_LIMITED) {
            mAvailabilityTextView.setText(getString(R.string.room_state_available).toUpperCase());
            mAvailabilityTextView.setBackgroundColor(ContextCompat.getColor(getActivity(), R.color.colorCSYellow));
        } else if (availability == CSRoom.ROOM_STATE_OCCUPIED) {
            mAvailabilityTextView.setText(getString(R.string.room_state_occupied).toUpperCase());
            mAvailabilityTextView.setBackgroundColor(ContextCompat.getColor(getActivity(), R.color.colorCSRed));
        } else {
            mAvailabilityTextView.setVisibility(View.GONE);
        }
     }

    public String getTitle() {
        return mRoom.getName();
    }

    @Override
    public void onClick(View v) {
        switch(v.getId()) {
            case R.id.book_now_fab:
                CSCalendarEvent event = mRoom.getNextEvent();
                if (event != null && event.canEndNow()) {
                    handleEndNow();
                } else {
                    handleBookNow();
                }
                break;
            case R.id.book_later_fab:
                handleBookLater();
                break;
        }
    }
    private void handleBookNow() {
        mFloatingMenu.close(true);
        final List<Date> timeSlots = mRoom.getAvailableTimeSlots();
        if (timeSlots.size() == 0) {
            Toast.makeText(getActivity(), "No time slots available.", Toast.LENGTH_SHORT).show();
            return;
        }

        final Date currentDate = new Date();
        CharSequence[] timeSlotText = DateHelper.dateStringsForList(timeSlots, "h:mm a");

        AlertDialog dialog = AlertDialogHelper.createBookNowDialog(getActivity(), "Book Until...", timeSlotText, new AlertDialogHelper.AlertDialogListener() {
            @Override
            public void OnAlertDialogSelect(int position) {
                Date bookingDate = timeSlots.get(position);

                CSCalendarEvent.createEvent(getActivity(), mRoom, currentDate, bookingDate, new Callback<CSCalendarEvent>() {
                    @Override
                    public void onResponse(Call<CSCalendarEvent> call, Response<CSCalendarEvent> response) {
                        if (response.isSuccessful()) {
                            String message = getString(R.string.welcome_text, mRoom.getName());
                            CSCalendarEvent event = response.body();
                            NotificationHelper.createEndNowNotification(getActivity(), getString(getActivity().getApplicationInfo().labelRes), message, mRoom, event);
                            MixpanelHelper.track(getActivity(), "RoomDetailFragment - Book Now Successful");
                        } else {
                            Toast.makeText(getActivity(), getString(R.string.booking_error), Toast.LENGTH_SHORT).show();;
                            MixpanelHelper.track(getActivity(), "RoomDetailFragment - Book Now Failed");
                        }

                        final Handler handler = new Handler();
                        handler.postDelayed(new Runnable() {
                            @Override
                            public void run() {
                                refreshRoom();
                            }
                        }, 1000);
                    }

                    @Override
                    public void onFailure(Call<CSCalendarEvent> call, Throwable t) {
                        Toast.makeText(getActivity(), getString(R.string.booking_error), Toast.LENGTH_SHORT).show();;
                        MixpanelHelper.track(getActivity(), "RoomDetailFragment - Book Now Failed");
                    }
                });
            }

            @Override
            public void OnAlertDialogCancel() {
                // Do Nothing
            }
        });
        dialog.setCanceledOnTouchOutside(false);
    }

    private void handleEndNow() {
        mFloatingMenu.close(true);
        AlertDialogHelper.createEndNowDialog(getActivity(), mRoom, new AlertDialogHelper.AlertDialogListener() {
            @Override
            public void OnAlertDialogSelect(int position) {

            }

            @Override
            public void OnAlertDialogCancel() {

            }
        }, new Callback<CSCalendarEvent>() {
            @Override
            public void onResponse(Call<CSCalendarEvent> call, Response<CSCalendarEvent> response) {
                if (response.isSuccessful()) {
                    Toast.makeText(getActivity(), getString(R.string.end_now_success), Toast.LENGTH_SHORT).show();
                    MixpanelHelper.track(getActivity(), "RoomDetailFragment - End Now Successful");
                } else {
                    Toast.makeText(getActivity(), getString(R.string.end_now_failure), Toast.LENGTH_SHORT).show();;
                    MixpanelHelper.track(getActivity(), "RoomDetailFragment - End Now Failed");
                }

                final Handler handler = new Handler();
                handler.postDelayed(new Runnable() {
                    @Override
                    public void run() {
                        refreshRoom();
                    }
                }, 1000);
            }

            @Override
            public void onFailure(Call<CSCalendarEvent> call, Throwable t) {
                Toast.makeText(getActivity(), getString(R.string.end_now_failure), Toast.LENGTH_SHORT).show();;
                MixpanelHelper.track(getActivity(), "RoomDetailFragment - End Now Failed");
            }
        });
    }

    public void setBookLaterListener(BookLaterListener listener) {
        mListener = listener;
    }

    private void handleBookLater() {
        mFloatingMenu.close(true);

        if (mListener != null) {
            mListener.onBookLaterSelect(mRoom);
        }
    }
}
