/*
    Copyright (C) 2015 ChargeSpot
 */
package com.chargespot.sdk.model;

import android.test.suitebuilder.annotation.SmallTest;

import junit.framework.Assert;
import junit.framework.TestCase;

/**
 * CSChargeSpotRadioTest
 * Date: 10/12/2015
 *
 * @version 0.3
 */
public class CSChargeSpotRadioTest
        extends TestCase {
    private int mockTxpower = -59;

    public CSChargeSpotRadioTest() {
    }

    @SmallTest
    public void testInitialization()
            throws Exception {
        CSChargeSpot chargeSpot = new CSChargeSpot();
        CSChargeSpotRadio device1 = new CSChargeSpotRadio(null, -40, mockTxpower);
        CSChargeSpotRadio device2 = new CSChargeSpotRadio(chargeSpot, -50, mockTxpower);
        CSChargeSpotRadio device3 = new CSChargeSpotRadio(chargeSpot, -100, mockTxpower);

        Assert.assertEquals(device1.getChargeSpot(), null);
        Assert.assertEquals(device1.getRssi(), -40);
        Assert.assertEquals(device1.getTxPower(), -59);

        Assert.assertEquals(device2.getChargeSpot(), chargeSpot);
        Assert.assertEquals(device2.getRssi(), -50);
        Assert.assertEquals(device2.getTxPower(), -59);

        Assert.assertEquals(device3.getChargeSpot(), chargeSpot);
        Assert.assertEquals(device3.getRssi(), -100);
        Assert.assertEquals(device3.getTxPower(), -59);

    }

    @SmallTest
    public void testGetChargeSpotSame() {
        CSChargeSpot chargeSpot = new CSChargeSpot();
        CSChargeSpotRadio device = new CSChargeSpotRadio(chargeSpot, -40, mockTxpower);

        Assert.assertEquals(device.getChargeSpot(), chargeSpot);

    }

    @SmallTest
    public void testGetChargeSpotDifferent() {
        CSChargeSpot chargeSpot = new CSChargeSpot();
        CSChargeSpotRadio device = new CSChargeSpotRadio(chargeSpot, -40, mockTxpower);
        CSChargeSpot chargeSpot2 = new CSChargeSpot();

        Assert.assertNotSame(device.getChargeSpot(), chargeSpot2);
    }

    @SmallTest
    public void testGetRssi() {
        CSChargeSpot chargeSpot = new CSChargeSpot();
        CSChargeSpotRadio device = new CSChargeSpotRadio(chargeSpot, -40, mockTxpower);

        Assert.assertEquals(device.getRssi(), -40);
    }

    @SmallTest
    public void testGetTxPower() {
        CSChargeSpot chargeSpot = new CSChargeSpot();
        CSChargeSpotRadio device = new CSChargeSpotRadio(chargeSpot, -40, mockTxpower);

        Assert.assertEquals(device.getTxPower(), mockTxpower);
    }

    @SmallTest
    public void testDistanceFar() throws Exception {
        CSChargeSpot chargeSpot = new CSChargeSpot();
        CSChargeSpotRadio device = new CSChargeSpotRadio(chargeSpot, -100, mockTxpower);

        Assert.assertEquals(device.getDistance(), CSDistance.FAR);
    }

    @SmallTest
    public void testDistanceNear() throws Exception {
        CSChargeSpot chargeSpot = new CSChargeSpot();
        CSChargeSpotRadio device = new CSChargeSpotRadio(chargeSpot, -60, mockTxpower);

        Assert.assertEquals(device.getDistance(), CSDistance.NEAR);
    }

    @SmallTest
    public void testDistanceImmediate() throws Exception {
        CSChargeSpot chargeSpot = new CSChargeSpot();
        CSChargeSpotRadio device = new CSChargeSpotRadio(chargeSpot, -40, mockTxpower);

        Assert.assertEquals(device.getDistance(), CSDistance.IMMEDIATE);
    }

    @SmallTest
    public void testPingIncrement() throws Exception {
        CSChargeSpot chargeSpot = new CSChargeSpot();
        CSChargeSpotRadio device = new CSChargeSpotRadio(chargeSpot, -40, mockTxpower);
        Assert.assertEquals(device.getPingCount(), 0);

        device.incrementPingCount();
        Assert.assertEquals(device.getPingCount(), 1);

        device.incrementPingCount();
        device.incrementPingCount();
        Assert.assertEquals(device.getPingCount(), 3);
    }

    @SmallTest
    public void testSignalRating() throws Exception {
        CSChargeSpot chargeSpot = new CSChargeSpot();
        CSChargeSpotRadio device = new CSChargeSpotRadio(chargeSpot, -40, mockTxpower);

        int expectedSignalRating = -40 - mockTxpower;

        Assert.assertEquals(device.getSignalRating(), expectedSignalRating);
    }

    @SmallTest
    public void testEquals() {
        CSChargeSpot chargeSpot = new CSChargeSpot();
        CSChargeSpotRadio device = new CSChargeSpotRadio(chargeSpot, -40, mockTxpower);

        Assert.assertTrue(device.equals(device));

        CSChargeSpotRadio device2 = new CSChargeSpotRadio(chargeSpot, -40, mockTxpower);
        Assert.assertTrue(device.equals(device2));

        CSChargeSpotRadio device3 = new CSChargeSpotRadio(chargeSpot, -400, 1);
        Assert.assertFalse(device.equals(device3));
    }
}
