/*
    Copyright (C) 2015 ChargeSpot
 */
package com.chargespot.sdk.handler;

import android.support.annotation.Nullable;

import com.chargespot.sdk.api.v1.model.ChargeSpotHookListModel;
import com.chargespot.sdk.events.CSChargeSpotEvent;

/**
 * ChargeSpot event handler for notifications
 *
 * @version 0.5
 */
public interface ICSChargeSpotEventHandler {
    /**
     * Register for ChargeSpot events to be received
     *
     * @param callback Callback to execute upon receiving a ChargeSpot event
     */
    void registerChargeSpotCallback(@Nullable com.chargespot.sdk.events.ICSChargeSpotEventHandler callback);

    /**
     * Unregister for ChargeSpot events to be received
     */
    void unregisterChargeSpotCallback();

    /**
     * ChargeSpot event has been successfully tracked.
     *
     * @param event         Kind of the event that occurred
     * @param hookListModel List of hook models
     */
    void onChargeSpotEvent(CSChargeSpotEvent event, ChargeSpotHookListModel hookListModel);
}
