package com.chargespot.workscape;

import android.bluetooth.BluetoothAdapter;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.pm.PackageManager;
import android.graphics.drawable.Drawable;
import android.net.Uri;
import android.os.Handler;
import android.support.design.widget.Snackbar;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.widget.ImageView;
import android.widget.Toast;

import com.chargespot.sdk.CSChargeSpotManager;
import com.chargespot.sdk.api.v1.model.ChargeSpotHookListModel;
import com.chargespot.sdk.config.CSChargeSpotConfig;
import com.chargespot.sdk.events.CSChargeSpotEvent;
import com.chargespot.sdk.events.ICSChargeSpotEventHandler;
import com.chargespot.sdk.exceptions.CSException;
import com.chargespot.workscape.Helpers.AlertDialogHelper;
import com.chargespot.workscape.Helpers.DateHelper;
import com.chargespot.workscape.Helpers.MixpanelHelper;
import com.chargespot.workscape.Helpers.NotificationHelper;
import com.chargespot.workscape.Helpers.PermissionsHelper;
import com.chargespot.sdk.manager.chargespot.CSChargeSpotManagerOptions;
import com.chargespot.sdk.manager.chargespot.ICSChargeSpotManagerInstance;
import com.chargespot.sdk.model.CSChargeSpot;
import com.chargespot.sdk.model.CSDistance;
import com.chargespot.workscape.Helpers.SharedPreferencesHelper;
import com.chargespot.workscape.Models.CSCalendarEvent;
import com.chargespot.workscape.Models.CSRoom;
import com.chargespot.workscape.Models.CSUser;
import com.google.android.gms.auth.api.Auth;
import com.google.android.gms.auth.api.signin.GoogleSignInOptions;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.common.api.ResultCallback;
import com.google.android.gms.common.api.Status;
import com.mikepenz.materialdrawer.AccountHeader;
import com.mikepenz.materialdrawer.AccountHeaderBuilder;
import com.mikepenz.materialdrawer.Drawer;
import com.mikepenz.materialdrawer.DrawerBuilder;
import com.mikepenz.materialdrawer.model.DividerDrawerItem;
import com.mikepenz.materialdrawer.model.PrimaryDrawerItem;
import com.mikepenz.materialdrawer.model.ProfileDrawerItem;
import com.mikepenz.materialdrawer.model.interfaces.IDrawerItem;
import com.mikepenz.materialdrawer.util.AbstractDrawerImageLoader;
import com.mikepenz.materialdrawer.util.DrawerImageLoader;
import com.squareup.picasso.Picasso;

import java.util.Date;
import java.util.List;
import java.util.Timer;
import java.util.TimerTask;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class BaseActivity extends AppCompatActivity
    implements
        HomeFragment.RoomListSelectListener,
        SupportCategoryFragment.SupportCategorySelectListener,
        SupportFormFragment.SupportFormFragmentListener,
        RoomDetailFragment.BookLaterListener,
        BookLaterFragment.BookLaterFragmentListener,
        Drawer.OnDrawerItemClickListener,
        ICSChargeSpotEventHandler {
    private static final String TAG = BaseActivity.class.getCanonicalName();
    private static final int REFRESH_TIME = 60000;

    private Drawer mDrawer;
    private Toolbar mToolbar;
    private GoogleApiClient mGoogleApiClient;

    private ICSChargeSpotManagerInstance mChargeSpotManager;

    private PermissionsHelper mPermissionsHelper;
    private boolean mIsInForegroundMode;

    private Timer mRangingTimer;
    private Timer mRefreshTimer;
    private View mCoordView;

    private CSChargeSpot mLastChargeSpot;
    private CSChargeSpotManagerOptions mChargeSpotOptions;

    private HomeFragment mHomeFragment;
    private RoomDetailFragment mRoomDetailFragment;

    private static final int sHomeIdentifier = 1;
    private static final int sSupportIdentifier = 2;
    private static final int sSettingsIdentifier = 3;
    private static final int sLogoutIdentifier = 4;

    private static final int sRangingTimerDuration = 75000;

    // Catches events when user turns off bluetooth in the app
    private final BroadcastReceiver mReceiver = new BroadcastReceiver() {
        public void onReceive(Context context, Intent intent) {
            String action = intent.getAction();

            if (BluetoothAdapter.ACTION_STATE_CHANGED.equals(action)) {
                if (intent.getIntExtra(BluetoothAdapter.EXTRA_STATE, -1)
                        == BluetoothAdapter.STATE_OFF) {
                    BaseActivity.this.mChargeSpotManager = null;
                    mPermissionsHelper.getPermissions();
                } else if (intent.getIntExtra(BluetoothAdapter.EXTRA_STATE, -1)
                        == BluetoothAdapter.STATE_ON) {
                    BaseActivity.this.startChargeSpotManager();
                }
            }
        }
    };


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_base);

        // Set a Toolbar to replace the ActionBar.
        mToolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(mToolbar);

        setupDrawer();
        mPermissionsHelper = new PermissionsHelper(this);

        mCoordView = findViewById(R.id.coordView);

        GoogleSignInOptions gso = new GoogleSignInOptions.Builder(GoogleSignInOptions.DEFAULT_SIGN_IN)
                .build();
        mGoogleApiClient = new GoogleApiClient.Builder(this)
                .addApi(Auth.GOOGLE_SIGN_IN_API, gso)
                .build();
    }

    @Override
    protected void onStart() {
        super.onStart();
        mGoogleApiClient.connect();
        registerServices();
    }

    @Override
    protected void onResume() {
        super.onResume();

        registerServices();
        refreshFragments();
        if (mChargeSpotManager != null) {
            startRangingForBeacons();
        }
        this.mIsInForegroundMode = true;
    }

    @Override
    protected void onPause() {
        super.onPause();
        unregisterServices();
        this.mIsInForegroundMode = false;

        mLastChargeSpot = null;
    }



    @Override
    protected void onDestroy() {
        super.onDestroy();
        MixpanelHelper.flush(this);
        unregisterServices();
    }

    /**
     * Registers broadcast receiver and instantiates services
     */
    private void registerServices() {
        registerReceiver(mReceiver, new IntentFilter(BluetoothAdapter.ACTION_STATE_CHANGED));
        registerForChargeSpotEvent();
        startRefreshTimer();
    }

    /**
     * Unregisters receivers
     */
    private void unregisterServices() {

        // Will cause an exception when logging out or garbage clean up. Ignore it.
        try {
            unregisterReceiver(mReceiver);
        } catch (Exception e) {
            e.printStackTrace();
        }
        unregisterChargeSpotEvent();
        stopRefreshTimer();
    }

    private void startRefreshTimer() {
        mRefreshTimer = new Timer();
        mRefreshTimer.scheduleAtFixedRate(new TimerTask() {
            @Override
            public void run() {
                runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        refreshFragments();
                    }
                });
            }
        }, REFRESH_TIME, REFRESH_TIME);
    }

    private void stopRefreshTimer() {
        if (mRefreshTimer != null) {
            mRefreshTimer.cancel();
            mRefreshTimer.purge();
            mRefreshTimer = null;
        }
    }

    private void setupDrawer() {
        PrimaryDrawerItem homeDrawerItem = new PrimaryDrawerItem().withIdentifier(sHomeIdentifier).withName(R.string.home_title);
//        PrimaryDrawerItem supportDrawerItem = new PrimaryDrawerItem().withIdentifier(sSupportIdentifier).withName(R.string.support_title);
        PrimaryDrawerItem settingsDrawerItem = new PrimaryDrawerItem().withIdentifier(sSettingsIdentifier).withName(R.string.settings_title);
        PrimaryDrawerItem logoutDrawerItem = new PrimaryDrawerItem().withIdentifier(sLogoutIdentifier).withName(R.string.logout_title).withSelectable(false);

        String name = "";
        String email = "";
        Uri imageUri = null;

        CSUser currentUser = CSRouter.getInstance().getCurrentUser();
        if (currentUser != null) {
            name = currentUser.getDisplayName();
            email = currentUser.getEmail();
            imageUri = currentUser.getImageUrl();
        }

        //initialize and create the image loader logic
        DrawerImageLoader.init(new AbstractDrawerImageLoader() {
            @Override
            public void set(ImageView imageView, Uri uri, Drawable placeholder) {
                Picasso.with(imageView.getContext()).load(uri).placeholder(placeholder).into(imageView);
            }

            @Override
            public void cancel(ImageView imageView) {
                Picasso.with(imageView.getContext()).cancelRequest(imageView);
            }

            @Override
            public Drawable placeholder(Context ctx) {
                return ContextCompat.getDrawable(ctx, R.drawable.profile_icon);
            }
        });

        AccountHeader headerResult = new AccountHeaderBuilder()
                .withActivity(this)
                .withHeaderBackground(R.drawable.header_background)
                .addProfiles(
                        new ProfileDrawerItem().withName(name).withEmail(email).withIcon(imageUri))
                .withProfileImagesClickable(false)
                .withSelectionListEnabledForSingleProfile(false)
                .build();

        mDrawer = new DrawerBuilder().withActivity(this)
                .withToolbar(mToolbar)
                .addDrawerItems(
                        homeDrawerItem,
//                        supportDrawerItem,
                        new DividerDrawerItem(),
                        settingsDrawerItem,
                        logoutDrawerItem
                )
                .withAccountHeader(headerResult)
                .withOnDrawerItemClickListener(this)
                .build();

    }

    private void setupChargeSpotManager() {
        this.mChargeSpotManager = CSChargeSpotManager.getInstance();

        if (this.mChargeSpotManager.isChargeSpotSDKInitialized()) {
            return;
        }

        CSChargeSpotConfig config = new CSChargeSpotConfig("eyJhbGciOiJIUzI1NiJ9.eyJ1c2VyX2lkIjoxLCJlbWFpbCI6InBhdEBjaGFyZ2VzcG90LmNhIiwiY2xpZW50X2lkIjoxLCJyb2xlX2lkIjoxfQ.NTNYXljjx0ChZVUTvJYDQ8tqS7YHQJYaweWSLtT3Xe0", null, false);
        config.setDevelopmentUrl("https://betteroffice.chargespot.com/api/v1/");

        mChargeSpotOptions = new CSChargeSpotManagerOptions();
        mChargeSpotOptions.setMaximumRangingDistance(CSDistance.FAR);
        mChargeSpotOptions.setRetrieveHookOnRange(false);

        try {
            this.mChargeSpotManager.setupChargeSpotSDK(this, config);
            this.mChargeSpotManager.registerForChargeSpotEvents(this, mChargeSpotOptions);
            scheduleRangingForBeacons();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void scheduleRangingForBeacons() {
        if (mRangingTimer != null) {
            mRangingTimer.cancel();
            mRangingTimer.purge();
            mRangingTimer = null;
        }
        mRangingTimer = new Timer();
        mRangingTimer.scheduleAtFixedRate(new TimerTask() {
            @Override
            public void run() {
                startRangingForBeacons();
            }
        }, sRangingTimerDuration, sRangingTimerDuration);
    }

    private void startRangingForBeacons() {
        if (mChargeSpotManager != null) {
            mChargeSpotManager.startRangingForChargeSpotBeacons();
        }
    }

    // `onPostCreate` called when activity start-up is complete after `onStart()`
    // NOTE! Make sure to override the method with only a single `Bundle` argument
    @Override
    protected void onPostCreate(Bundle savedInstanceState) {
        super.onPostCreate(savedInstanceState);

        Fragment fragment = createHomeFragment();
        replaceBaseFragment(fragment);
    }

    @Override
    public void onBackPressed() {

        if (mDrawer.isDrawerOpen()) {
            mDrawer.closeDrawer();
            return;
        }

        FragmentManager fragmentManager = getSupportFragmentManager();
        Fragment fragment = fragmentManager.findFragmentById(R.id.flContent);

        if (fragment.getClass() == HomeFragment.class) {
            moveTaskToBack(true);
        } else {
            super.onBackPressed();
        }
    }

    @Override
    public boolean onItemClick(View view, int position, IDrawerItem drawerItem) {
        BaseFragment fragment = null;
        switch ((int) drawerItem.getIdentifier()) {
            case sHomeIdentifier:
                fragment = createHomeFragment();
                break;
            case sSupportIdentifier:
                fragment = createSupportCategoryFragment();
                break;
            case sSettingsIdentifier:
                fragment = createSettingsCategoryFragment();
                break;
            case sLogoutIdentifier:
                performLogout();
                break;

        }
        if (fragment == null) {
            return false;
        }

        replaceBaseFragment(fragment);
        return false;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // The action bar home/up action should open or close the drawer.
        switch (item.getItemId()) {
            case android.R.id.home:
                mDrawer.openDrawer();
                return true;
        }

        return super.onOptionsItemSelected(item);
    }

    private BaseFragment createHomeFragment() {
        HomeFragment homeFragment = new HomeFragment();
        homeFragment.setRoomSelectedListener(this);
        mHomeFragment = homeFragment;
        return homeFragment;
    }

    private BaseFragment createRoomDetaiFragment(CSRoom room) {
        RoomDetailFragment roomDetailFragment = new RoomDetailFragment();
        roomDetailFragment.setBookLaterListener(this);
        roomDetailFragment.setRoom(room);
        mRoomDetailFragment = roomDetailFragment;

        return roomDetailFragment;
    }

    private BaseFragment createBookLaterFragment(CSRoom room) {
        BookLaterFragment bookLaterFragment = new BookLaterFragment();
        bookLaterFragment.setRoom(room);
        bookLaterFragment.setBookLaterFragmentListener(this);
        return bookLaterFragment;
    }

    private BaseFragment createSettingsCategoryFragment() {
        SettingsFragment settingsFragment = new SettingsFragment();
        return settingsFragment;
    }

    private BaseFragment createSupportCategoryFragment() {
        SupportCategoryFragment supportCategoryFragment = new SupportCategoryFragment();
        supportCategoryFragment.setSupportCategorySelectListener(this);
        return supportCategoryFragment;
    }

    private BaseFragment createSupportFormFragment(int supportCategory) {
        SupportFormFragment supportFormFragment = new SupportFormFragment();
        supportFormFragment.setSupportFormFragmentListener(this);
        supportFormFragment.setCategory(supportCategory);

        switch (supportCategory) {
            case SupportCategoryFragment.SUPPORT_CATEGORY_TOO_COLD:
            case SupportCategoryFragment.SUPPORT_CATEGORY_TOO_HOT:
                supportFormFragment.setupTemperatureForm();
                break;
            case SupportCategoryFragment.SUPPORT_CATEGORY_MISSING_SUPPLIES:
            case SupportCategoryFragment.SUPPORT_CATEGORY_REQUIRES_CLEANING:
            case SupportCategoryFragment.SUPPORT_CATEGORY_EQUIPMENT_DAMAGE:
            case SupportCategoryFragment.SUPPORT_CATEGORY_OTHER:
                break;

        }

        return supportFormFragment;
    }

    private boolean replaceBaseFragment(Fragment fragment) {
        if (fragment != null && findViewById(R.id.flContent) != null) {
            String backStateName = fragment.getClass().getName();

            FragmentManager fragmentManager = getSupportFragmentManager();
            boolean fragmentPopped = fragmentManager.popBackStackImmediate(backStateName, 0);

            if (!fragmentPopped) {
                replaceFragment(fragment);
            }
        }
        return false;
    }

    private boolean replaceFragment(Fragment fragment) {
        if (fragment != null && findViewById(R.id.flContent) != null) {
            String backStateName = fragment.getClass().getName();

            // Insert the fragment by replacing any existing fragment
            FragmentManager fragmentManager = getSupportFragmentManager();
            FragmentTransaction ft = fragmentManager.beginTransaction();
            Fragment currentFragment = fragmentManager.findFragmentById(R.id.flContent);

            // Don't add a fragment if they are the same
            if (currentFragment != null && currentFragment.getClass().getName() == fragment.getClass().getName()) {
                return true;
            }

            // Don't add animation for first fragment
            if (fragmentManager.getFragments() != null) {
                ft.setCustomAnimations(R.anim.enter_from_right, R.anim.exit_to_left, R.anim.enter_from_left, R.anim.exit_to_right);
            }
            ft.addToBackStack(backStateName);
            ft.replace(R.id.flContent, fragment);
            ft.commit();

            return true;
        }
        return false;
    }

    private void refreshFragments() {
        if (mHomeFragment != null) {
            mHomeFragment.onRefresh();
        }

        if (mRoomDetailFragment != null) {
            mRoomDetailFragment.onRefresh();
        }
    }

    private void performLogout() {
        new AlertDialog.Builder(this)
                .setMessage("Are you sure you want to logout?")
                .setPositiveButton(R.string.yes, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        signOut();
                    }
                })
                .setNegativeButton(R.string.no, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        //do Nothing
                    }
                }).show();
    }

    private void signOut() {
        if (mGoogleApiClient.isConnected()) {
            Auth.GoogleSignInApi.signOut(mGoogleApiClient).setResultCallback(
                    new ResultCallback<Status>() {
                        @Override
                        public void onResult(Status status) {
                            if (status.isSuccess()) {
                                Toast.makeText(BaseActivity.this, "Logged out successfully", Toast.LENGTH_SHORT).show();
                                SharedPreferencesHelper.clearSharedPreferences(BaseActivity.this);
                                startAuthActivity();
                                MixpanelHelper.track(BaseActivity.this, "BaseActivity - User Logged Out");

                            }
                        }
                    });
        } else {
            Toast.makeText(this, "Google Api not connected", Toast.LENGTH_SHORT).show();
        }
    }

    private void startAuthActivity() {
        Intent intent = new Intent(this, AuthActivity.class);
        startActivity(intent);
        finish();
    }

    @Override
    public void onRoomSelected(int position) {
        List<CSRoom> roomList = CSRouter.getInstance().getRoomList();
        if (position > roomList.size()) {
            return;
        }
        CSRoom room = roomList.get(position);
        Fragment roomDetailFragment = createRoomDetaiFragment(room);
        replaceFragment(roomDetailFragment);
    }

    @Override
    public void onSupportCategorySelect(int supportCategory) {
        BaseFragment supportFormFragment = createSupportFormFragment(supportCategory);
        replaceFragment(supportFormFragment);
    }

    @Override
    public void returnHome() {
        BaseFragment homeFragment = createHomeFragment();
        replaceBaseFragment(homeFragment);

        final Handler handler = new Handler();
        handler.postDelayed(new Runnable() {
            @Override
            public void run() {
                refreshFragments();
            }
        }, 1000);
    }

    @Override
    public void onBookLaterSelect(CSRoom room) {
        BaseFragment bookLaterFragment = createBookLaterFragment(room);
        replaceFragment(bookLaterFragment);
    }

    @Override
    public void onChargeSpotRanged(CSChargeSpot chargeSpot) {
        Log.d(TAG, "Ranged Beacon - UUID: " + chargeSpot.UUID + " M:" + chargeSpot.Major + " m:" + chargeSpot.Minor);

        if (mLastChargeSpot != null && mLastChargeSpot.equals(chargeSpot)) {
            Log.d(TAG, "Action cancelled - Same chargespot.");
            return;
        }
        mLastChargeSpot = chargeSpot;
        CSRouter.getInstance().setCurrentChargeSpot(chargeSpot);
        final CSRoom room = CSRouter.getInstance().getRoomForChargeSpot(chargeSpot);

        if (room == null) {
            return;
        }
        Log.d(TAG, "Found room - " + room.getName());

        final int state = room.getState();
        switch (state) {
            case CSRoom.ROOM_STATE_AVAILABLE:
            case CSRoom.ROOM_STATE_AVAILABLE_LIMITED:
                createBookNowNotification(room);
                createBookNowSnackBar(room);
                break;
            case CSRoom.ROOM_STATE_OCCUPIED:
                createOccupiedNotification(room);
                createOccupiedSnackbar(room);
                break;
            case CSRoom.ROOM_STATE_UNAVAILABLE:
                break;
        }
    }

    @Override
    public void onChargeSpotEventSuccess(CSChargeSpotEvent event, ChargeSpotHookListModel hookListModel) {

    }

    @Override
    public void onChargeSpotEventFailure(CSException e) {

    }

    private void startChargeSpotManager() {
        if (mPermissionsHelper.hasSufficientPermissions()) {
            setupChargeSpotManager();
        } else {
            mPermissionsHelper.getPermissions();
        }
    }

    private void registerForChargeSpotEvent() {
        if (mChargeSpotManager != null) {
            try {
                mChargeSpotManager.registerForChargeSpotEvents(this, mChargeSpotOptions);
            } catch (Exception e) {
                e.printStackTrace();
            }
        } else {
            startChargeSpotManager();
        }
    }

    private void unregisterChargeSpotEvent() {
        if (mChargeSpotManager != null) {
            try {
                mChargeSpotManager.unregisterForChargeSpotEvents();
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }

    /**
     * Callback for requesting permissions
     * Starts ChargeSpot Manager if location permission is granted
     *
     * @param requestCode
     * @param permissions
     * @param grantResults
     */
    @Override
    public void onRequestPermissionsResult(int requestCode, String[] permissions, int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        if (requestCode == PermissionsHelper.FINE_LOCATION_REQUEST_CODE) {
            for (int i = 0; i < permissions.length; i++) {
                String permission = permissions[i];
                int grantResult = grantResults[i];

                if (permission.equals(android.Manifest.permission.ACCESS_FINE_LOCATION)) {
                    if (grantResult == PackageManager.PERMISSION_GRANTED) {
                        if (this.mChargeSpotManager == null) {
                            setupChargeSpotManager();
                        }
                    } else {
                        this.mChargeSpotManager = null;
                    }
                }
            }
        }
    }

    private void createBookNowNotification(CSRoom room) {
        String message = getString(R.string.welcome_text, room.getName());
        NotificationHelper.createBookNowNotification(this, getString(getApplicationInfo().labelRes), message, room);
    }

    private void createBookNowSnackBar(final CSRoom room) {
        String message = getString(R.string.welcome_text, room.getName());
        Snackbar snackBar = Snackbar.make(mCoordView, message, Snackbar.LENGTH_LONG);
        snackBar.setDuration(3500);
        snackBar.setAction(getString(R.string.book_now), new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                handleBookNow(room, new Callback<CSCalendarEvent>() {
                    @Override
                    public void onResponse(Call<CSCalendarEvent> call, Response<CSCalendarEvent> response) {
                        if (response.isSuccessful()) {
                            Toast.makeText(BaseActivity.this, getString(R.string.booking_success), Toast.LENGTH_SHORT).show();
                            MixpanelHelper.track(BaseActivity.this, "BaseActivity Snackbar - Book Now Successful");
                        } else {
                            Toast.makeText(BaseActivity.this, getString(R.string.booking_error), Toast.LENGTH_SHORT).show();
                            MixpanelHelper.track(BaseActivity.this, "BaseActivity Snackbar - Book Now Failed");
                        }

                        final Handler handler = new Handler();
                        handler.postDelayed(new Runnable() {
                            @Override
                            public void run() {
                                refreshFragments();
                            }
                        }, 1000);
                    }

                    @Override
                    public void onFailure(Call<CSCalendarEvent> call, Throwable t) {
                        Toast.makeText(BaseActivity.this, getString(R.string.booking_error), Toast.LENGTH_SHORT).show();
                        MixpanelHelper.track(BaseActivity.this, "BaseActivity Snackbar - Book Now Failed");
                    }
                });
            }
        });
        snackBar.setActionTextColor(ContextCompat.getColor(this, R.color.colorCSTeal));
        snackBar.show();
    }

    private void createOccupiedNotification(CSRoom room) {
        CSCalendarEvent event = room.getNextEvent();

        if (event != null && event.canEndNow()) {
            String message = getString(R.string.welcome_text, room.getName());
            NotificationHelper.createEndNowNotification(this, getString(getApplicationInfo().labelRes), message, room, event);
        } else {
            String message = getString(R.string.occupied_text, room.getName());
            NotificationHelper.createNotification(this,getString(getApplicationInfo().labelRes), message);
        }
    }

    private void createOccupiedSnackbar(final CSRoom room) {
        String message = getString(R.string.welcome_text, room.getName());
        Snackbar snackBar = Snackbar.make(mCoordView, message, Snackbar.LENGTH_LONG);
        CSCalendarEvent event = room.getNextEvent();
        if (event != null && event.canEndNow()) {
            snackBar.setAction(getString(R.string.end_now), new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    handleEndNow(room);
                }
            });
        } else {
            snackBar.setAction(getString(R.string.room_state_occupied), new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                }
            });
        }
        snackBar.setDuration(3500);
        snackBar.setActionTextColor(ContextCompat.getColor(this, R.color.colorCSRed));
        snackBar.show();
    }

    private void handleBookNow(final CSRoom room, final Callback<CSCalendarEvent> callback) {
        final List<Date> timeSlots = room.getAvailableTimeSlots();
        if (timeSlots.size() == 0) {
            Toast.makeText(this, "No time slots available.", Toast.LENGTH_SHORT).show();
            return;
        }

        final Date currentDate = new Date();
        CharSequence[] timeSlotText = DateHelper.dateStringsForList(timeSlots, "h:mm a");

        AlertDialog dialog = AlertDialogHelper.createBookNowDialog(this, "Book Until...", timeSlotText, new AlertDialogHelper.AlertDialogListener() {
            @Override
            public void OnAlertDialogSelect(int position) {
                Date bookingDate = timeSlots.get(position);
                CSCalendarEvent.createEvent(BaseActivity.this, room, currentDate, bookingDate, callback);
            }

            @Override
            public void OnAlertDialogCancel() {
                // Do Nothing
            }
        });
        dialog.setCanceledOnTouchOutside(false);
    }

    private void handleEndNow(final CSRoom room) {
        AlertDialogHelper.createEndNowDialog(this, room, new AlertDialogHelper.AlertDialogListener() {
            @Override
            public void OnAlertDialogSelect(int position) {

            }

            @Override
            public void OnAlertDialogCancel() {

            }
        }, new Callback<CSCalendarEvent>() {
            @Override
            public void onResponse(Call<CSCalendarEvent> call, Response<CSCalendarEvent> response) {
                if (response.isSuccessful()) {
                    Toast.makeText(BaseActivity.this, getString(R.string.end_now_success), Toast.LENGTH_SHORT).show();
                    createOccupiedNotification(room);
                    MixpanelHelper.track(BaseActivity.this, "BaseActivity Snackbar - End Now Successful");
                } else {
                    Toast.makeText(BaseActivity.this, getString(R.string.end_now_failure), Toast.LENGTH_SHORT).show();
                    MixpanelHelper.track(BaseActivity.this, "BaseActivity Snackbar - End Now Failed");
                }

                final Handler handler = new Handler();
                handler.postDelayed(new Runnable() {
                    @Override
                    public void run() {
                        refreshFragments();
                    }
                }, 1000);
            }

            @Override
            public void onFailure(Call<CSCalendarEvent> call, Throwable t) {
                Toast.makeText(BaseActivity.this, getString(R.string.end_now_failure), Toast.LENGTH_SHORT).show();
                MixpanelHelper.track(BaseActivity.this, "BaseActivity Snackbar - End Now Failed");
            }
        });
    }
}