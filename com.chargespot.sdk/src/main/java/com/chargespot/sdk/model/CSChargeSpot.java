/*
    Copyright (C) 2015 ChargeSpot
 */
package com.chargespot.sdk.model;

/**
 * Bluetooth LE Beacon ID
 * <p/>
 * Date: 10/12/2015
 *
 * @version 0.5
 */
public class CSChargeSpot {
    /**
     * Address of the BLE device
     */
    public String Address;

    /**
     * UUID of the ChargeSpot device
     */
    public String UUID;

    /**
     * Major version of the ChargeSpot device
     */
    public int Major;

    /**
     * Minor version of the ChargeSpot device
     */
    public int Minor;
}
