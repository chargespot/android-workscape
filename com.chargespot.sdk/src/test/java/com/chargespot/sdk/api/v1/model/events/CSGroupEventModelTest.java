/*
    Copyright (C) 2015 ChargeSpot
 */
package com.chargespot.sdk.api.v1.model.events;

import android.test.suitebuilder.annotation.SmallTest;

import com.chargespot.sdk.events.CSChargeSpotEvent;
import com.chargespot.sdk.events.CSGroupEvent;

import junit.framework.Assert;
import junit.framework.TestCase;

/**
 * CSGroupEventModelTest
 *
 * Date: 10/12/2015
 * @version 0.3
 */
public class CSGroupEventModelTest
    extends TestCase
{
    @SmallTest
    public void testAssignment()
        throws Exception
    {
        final String UUID = "UUID";
        final int Major = 1;
        final int Minor = 2;
        final CSGroupEvent Event = CSGroupEvent.CSEventEnteredGroup;

        CSGroupEventModel model = new CSGroupEventModel(UUID, Major, Minor, Event);

        Assert.assertEquals(UUID, model.UUID);
        Assert.assertEquals(Major, model.Major);
        Assert.assertEquals(Minor, model.Minor);
        Assert.assertEquals(Event, model.Event);
    }

    @SmallTest
    public void testAssignmentComplete()
            throws Exception
    {
        final String Address = "Address";
        final String UUID = "UUID";
        final int Major = 1;
        final int Minor = 2;
        final CSGroupEvent Event = CSGroupEvent.CSEventEnteredGroup;

        CSGroupEventModel model = new CSGroupEventModel(Address, UUID, Major, Minor, Event);

        Assert.assertEquals(Address, model.Address);
        Assert.assertEquals(UUID, model.UUID);
        Assert.assertEquals(Major, model.Major);
        Assert.assertEquals(Minor, model.Minor);
        Assert.assertEquals(Event, model.Event);
    }

    @SmallTest
    public void testIsValid()
        throws Exception
    {
        CSGroupEventModel model = new CSGroupEventModel();
        Assert.assertFalse(model.isValid());

        model.UUID = "";
        Assert.assertFalse(model.isValid());

        model.UUID = "UUID";
        Assert.assertFalse(model.isValid());

        model.Event = CSGroupEvent.CSEventEnteredGroup;
        Assert.assertTrue(model.isValid());
    }

    @SmallTest
    public void testIsValidWithoutAddress()
            throws Exception
    {
        CSGroupEventModel model = new CSGroupEventModel();
        model.Address = null;
        model.UUID = "UUID";
        model.Event = CSGroupEvent.CSEventEnteredGroup;
        Assert.assertTrue(model.isValid());

        model.Address = "";
        Assert.assertTrue(model.isValid());
    }
}