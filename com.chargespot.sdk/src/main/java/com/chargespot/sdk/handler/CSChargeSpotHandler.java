/*
    Copyright (C) 2015 ChargeSpot
 */
package com.chargespot.sdk.handler;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.BatteryManager;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.util.Log;

import com.chargespot.sdk.CSChargeSpotManager;
import com.chargespot.sdk.api.v1.model.ChargeSpotRegionListModel;
import com.chargespot.sdk.config.CSChargeSpotSDKConfig;
import com.chargespot.sdk.exceptions.CSBluetoothException;
import com.chargespot.sdk.manager.api.IChargeSpotApiManagerInternal;
import com.chargespot.sdk.api.v1.model.ChargeSpotGroupHookListModel;
import com.chargespot.sdk.api.v1.model.ChargeSpotHookListModel;
import com.chargespot.sdk.api.v1.model.events.CSChargeSpotEventModel;
import com.chargespot.sdk.api.v1.model.events.CSGroupEventModel;
import com.chargespot.sdk.manager.chargespot.CSChargeSpotManagerOptions;
import com.chargespot.sdk.manager.chargespot.CSChargeSpotRadioManager;
import com.chargespot.sdk.model.battery.BatteryStatus;
import com.chargespot.sdk.discovery.ICSChargeSpotDiscoveryCallback;
import com.chargespot.sdk.discovery.ICSChargeSpotDiscoveryService;
import com.chargespot.sdk.events.CSChargeSpotEvent;
import com.chargespot.sdk.events.CSGroupEvent;
import com.chargespot.sdk.events.ICSChargeSpotEventHandler;
import com.chargespot.sdk.events.ICSGroupEventHandler;
import com.chargespot.sdk.exceptions.CSInvalidParameterException;
import com.chargespot.sdk.exceptions.CSUnsupportedException;
import com.chargespot.sdk.model.CSChargeSpotRadio;
import com.chargespot.sdk.state.CSDeviceState;
import com.chargespot.sdk.state.ICSDeviceStateHandler;
import com.chargespot.sdk.trace.CSChargeSpotTraceLevel;
import com.chargespot.sdk.trace.ICSChargeSpotTraceLogger;

import java.util.Timer;
import java.util.TimerTask;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * ChargeSpot handler interface for handling
 * all of the events being received from the devices, and
 * executing proper actions related to them.
 *
 * @version 0.5
 */
public class CSChargeSpotHandler
        implements
        ICSChargeSpotHandler,
        ICSChargeSpotDiscoveryCallback {
    private Context mContext;

    private Object mMutext = new Object();

    private com.chargespot.sdk.handler.ICSChargeSpotEventHandler mDelayEventHandler;
    private ICSChargeSpotTraceLogger mTraceLogger;
    private IChargeSpotApiManagerInternal mInternalApiManager;
    private ICSChargeSpotDiscoveryService mDiscoveryService;
    private CSChargeSpotRadioManager mRadioManager;

    private ICSChargeSpotEventHandler mChargeSpotEventHandler;
    private ICSGroupEventHandler mGroupEventHandler;
    private ICSChargeSpotErrorHandler mErrorHandler;

    private CSChargeSpotRadio mChargeSpotClosestDevice;
    private BatteryStatus mBatteryStatus;

    private long mScanStartTime;
    private boolean mTracking;

    private Timer mDiscoveryTimer;
    private Timer mGroupTimer;

    {
        mContext = null;
        mDelayEventHandler = null;
        mTraceLogger = null;
        mInternalApiManager = null;
        mDiscoveryService = null;
        mRadioManager = null;
        mChargeSpotEventHandler = null;
        mGroupEventHandler = null;
        mErrorHandler = null;
        mChargeSpotClosestDevice = null;
        mBatteryStatus = null;
        mTracking = false;
        mGroupTimer = null;
    }

    //region Events
    private final BroadcastReceiver mBatteryReceiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            synchronized (CSChargeSpotHandler.this) {
                final String action = intent.getAction();
                if (Intent.ACTION_BATTERY_CHANGED.equals(action)) {
                    int status = intent.getIntExtra(BatteryManager.EXTRA_STATUS, -1);

                    boolean isCharged = status == BatteryManager.BATTERY_STATUS_FULL;
                    boolean isCharging = status == BatteryManager.BATTERY_STATUS_CHARGING;

                    if (CSChargeSpotHandler.this.mBatteryStatus == null) {
                        CSChargeSpotHandler.this.mBatteryStatus = new BatteryStatus();

                        CSChargeSpotHandler.this.mBatteryStatus.isCharged = isCharged;
                        CSChargeSpotHandler.this.mBatteryStatus.isCharging = isCharging;

                        try {
                            CSChargeSpotHandler.this.executeChargeSpotDiscovery();
                        } catch (CSBluetoothException e) {
                            CSChargeSpotHandler.this.mTraceLogger.Trace(CSChargeSpotTraceLevel.CHARGESPOT_TRACE_ERROR, e.getMessage());
                        }
                        return;
                    }

                    if (isCharged != CSChargeSpotHandler.this.mBatteryStatus.isCharged || isCharging != CSChargeSpotHandler.this.mBatteryStatus.isCharging) {
                        CSChargeSpotHandler.this.mBatteryStatus.isCharged = isCharged;
                        CSChargeSpotHandler.this.mBatteryStatus.isCharging = isCharging;

                        try {
                            CSChargeSpotHandler.this.executeChargeSpotDiscovery();
                        } catch (CSBluetoothException e) {
                            CSChargeSpotHandler.this.mTraceLogger.Trace(CSChargeSpotTraceLevel.CHARGESPOT_TRACE_ERROR, e.getMessage());
                        }
                    }
                }
            }
        }
    };
    //endregion

    //region Constructor

    /**
     * ChargeSpot handler
     */
    public CSChargeSpotHandler(final ICSChargeSpotTraceLogger traceLogger, final Context context, final IChargeSpotApiManagerInternal apiManagerInternal)
            throws CSUnsupportedException, CSBluetoothException {
        this.mTraceLogger = traceLogger;
        this.mInternalApiManager = apiManagerInternal;

        this.mContext = context;

        if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.LOLLIPOP) {
            this.mTraceLogger.Trace(CSChargeSpotTraceLevel.CHARGESPOT_TRACE_DEBUG, "Setting up Lollipop BLE discovery");
            this.mDiscoveryService = new com.chargespot.sdk.discovery.lollipop.CSChargeSpotDiscoveryService(this.mTraceLogger, this.mContext);
        } else {
            this.mTraceLogger.Trace(CSChargeSpotTraceLevel.CHARGESPOT_TRACE_DEBUG, "Setting up KitKat BLE discovery");
            this.mDiscoveryService = new com.chargespot.sdk.discovery.kitkat.CSChargeSpotDiscoveryService(this.mTraceLogger, this.mContext);
        }

        this.mContext.registerReceiver(mBatteryReceiver, makeBatteryUpdateIntentFilter());

        mDelayEventHandler = new com.chargespot.sdk.handler.CSChargeSpotEventHandler();
    }
    //endregion

    //region Internal

    /**
     * Execute a ChargeSpot discovery event
     */
    public synchronized void executeChargeSpotDiscovery()
            throws CSBluetoothException {

        if (CSChargeSpotManager.getInstance().getRegionList() != null) {
            this.startChargeSpotTracking();
        } else {
            // Retry the region list request since we can't scan without it
            try {
                Log.d("CHARGE_SPOT_SDK", "Starting list regions from handler");
                this.mInternalApiManager.listRegions(new Callback<ChargeSpotRegionListModel>() {
                    @Override
                    public void onResponse(Call<ChargeSpotRegionListModel> call, Response<ChargeSpotRegionListModel> response) {
                        if (response == null || !response.isSuccessful()) {
                            CSChargeSpotHandler.this.handleAPIResponseError(response);
                        } else {
                            CSChargeSpotManager.getInstance().setRegionList(response.body());
                            try {
                                CSChargeSpotHandler.this.startChargeSpotTracking();
                            } catch (Exception e) { }
                        }
                    }

                    @Override
                    public void onFailure(Call<ChargeSpotRegionListModel> call, Throwable t) {
                        CSChargeSpotHandler.this.handleFailedRequest(t);
                    }
                });
            } catch (Exception e) {}
        }
    }

    //endregion

    //region Callbacks

    /**
     * Register for ChargeSpot events to be received
     *
     * @param callback Callback to execute upon receiving a ChargeSpot event
     */
    @Override
    public synchronized void registerForChargeSpotEvents(@Nullable ICSChargeSpotEventHandler callback, @Nullable CSChargeSpotManagerOptions options) {
        synchronized (mMutext) {
            mChargeSpotEventHandler = callback;
            mDelayEventHandler.registerChargeSpotCallback(callback);
        }
    }

    /**
     * Unregister for ChargeSpot events to be received
     */
    @Override
    public synchronized void unregisterForChargeSpotEvents() {
        synchronized (mMutext) {
            mChargeSpotEventHandler = null;
            mDelayEventHandler.unregisterChargeSpotCallback();
        }
    }

    /**
     * Register for Group events to be received
     *
     * @param callback Callback to execute upon receiving a Group event
     */
    @Override
    public synchronized void registerForGroupEvents(@Nullable ICSGroupEventHandler callback, @Nullable CSChargeSpotManagerOptions options) {
        synchronized (mMutext) {
            mGroupEventHandler = callback;
        }
    }

    /**
     * Unregister for Group events to be received.
     */
    @Override
    public synchronized void unregisterForGroupEvents() {
        synchronized (mMutext) {
            mGroupEventHandler = null;
        }
    }

    /**
     * Register for Error to be received
     *
     * @param callback Callback to execute upon receiving an Error
     */
    @Override
    public synchronized void registerForChargeSpotErrors(@Nullable ICSChargeSpotErrorHandler callback) {
        synchronized (mMutext) {
            mErrorHandler = callback;
        }
    }

    /**
     * Unregister for Error to be received.
     */
    @Override
    public synchronized void unregisterForChargeSpotErrors() {
        synchronized (mMutext) {
            mErrorHandler = null;
        }
    }

    /**
     * Retrieve the current device state. This will return the last known
     * state of the device.
     *
     * @param callback Callback to be executed upon device state retrieval
     */
    @Override
    public synchronized void retrieveDeviceState(@Nullable ICSDeviceStateHandler callback) {
        if (null == callback) {
            return;
        }

        if (mChargeSpotClosestDevice == null) {
            callback.onDeviceStateRetrievedSuccess(CSDeviceState.CSDeviceNotOnChargeSpot, null);
            return;
        }

        if (mBatteryStatus == null) {
            callback.onDeviceStateRetrievedSuccess(CSDeviceState.CSDeviceStateUnknown, mChargeSpotClosestDevice.getChargeSpot());
            return;
        }

        if (mBatteryStatus.isCharging) {
            callback.onDeviceStateRetrievedSuccess(CSDeviceState.CSDeviceChargingOnChargeSpot, mChargeSpotClosestDevice.getChargeSpot());
            return;
        }

        if (mBatteryStatus.isCharged) {
            callback.onDeviceStateRetrievedSuccess(CSDeviceState.CSDeviceFullChargingOnChargeSpot, mChargeSpotClosestDevice.getChargeSpot());
            return;
        }

        callback.onDeviceStateRetrievedSuccess(CSDeviceState.CSDeviceOnChargeSpot, mChargeSpotClosestDevice.getChargeSpot());
    }

    protected void startMonitoringChargeSpotRegions() throws CSBluetoothException {
        this.executeChargeSpotDiscovery();

        mGroupTimer = new Timer();
        mGroupTimer.scheduleAtFixedRate(new TimerTask() {
            synchronized public void run() {
                try {
                    CSChargeSpotHandler.this.executeChargeSpotDiscovery();
                } catch (CSBluetoothException e) {
                }
            }
        }, CSChargeSpotSDKConfig.CHARGESPOT_GROUP_SCAN_PERIOD, CSChargeSpotSDKConfig.CHARGESPOT_GROUP_SCAN_PERIOD);
    }

    /**
     * Call the didReceiveError callback to inform the app that an API error has occured
     * Requires the application to implement the ICSChargeSpotErrorHandler interface and registerForChargeSpotErrors
     *
     * @param response Response from API request
     */
    public void handleAPIResponseError(Response response) {
        int errorCode = response.code();
        String errorDescription = response.message();
        this.mTraceLogger.Trace(CSChargeSpotTraceLevel.CHARGESPOT_TRACE_DEBUG, "Error: " + errorCode + " - " + errorDescription);

        if (this.mErrorHandler != null) {
            this.mErrorHandler.didReceivedError(errorCode, errorDescription);
        }
    }

    /**
     * Call the didReceiveError callback to inform the app had a failed request
     * Requires the application to implement the ICSChargeSpotErrorHandler interface and registerForChargeSpotErrors
     *
     * @param t Throwable from failed request
     */
    public void handleFailedRequest(Throwable t) {
        String errorDescription = t.getLocalizedMessage();
        if (errorDescription == null) {
            errorDescription = "Request failed";
        }
        this.mTraceLogger.Trace(CSChargeSpotTraceLevel.CHARGESPOT_TRACE_DEBUG, "Error: " + errorDescription);
        if (this.mErrorHandler != null) {
            this.mErrorHandler.didReceivedError(0, errorDescription);
        }
    }

    /**
     * Start ChargeSpot event tracking. This will start the
     * Bluetooth Manager tracking of the ChargeSpot device and
     * will generate events relating to the state of the device.
     */
    protected synchronized void startChargeSpotTracking()
            throws CSBluetoothException {
        if (!this.mTracking) {
            this.mDiscoveryTimer = new Timer();
            this.mDiscoveryTimer.schedule(new TimerTask() {
                synchronized public void run() {
                    stopChargeSpotTracking();

                    if (mChargeSpotClosestDevice == null && mErrorHandler != null) {
                        CSChargeSpotHandler.this.mErrorHandler.didReceivedError(0, "Could not locate ChargeSpot Beacon");
                    }
                }
            }, CSChargeSpotSDKConfig.CHARGESPOT_BLE_SCAN_TIMEOUT);

            this.mDiscoveryService.registerChargeSpotDiscoveryCallback(this);
            this.mDiscoveryService.startDiscovery();
            this.mTracking = true;
            this.mRadioManager = new CSChargeSpotRadioManager();
            this.mChargeSpotClosestDevice = null;
            this.mScanStartTime = System.nanoTime();
        }
    }

    /**
     * Stop ChargeSpot event tracking.
     */
    protected synchronized void stopChargeSpotTracking() {
        if (this.mTracking) {
            this.mDiscoveryService.stopDiscovery();
            this.mDiscoveryService.unregisterChargeSpotDiscoveryCallback();
            this.mTracking = false;
            this.mRadioManager = null;
            this.mDiscoveryTimer.cancel();
            this.mTraceLogger.Trace(CSChargeSpotTraceLevel.CHARGESPOT_TRACE_DEBUG, "Time scanned:" + ((System.nanoTime() - this.mScanStartTime) / 1000000000.0));
        }
    }

    /**
     * Check to see if ChargeSpot is tracking
     *
     * @return Returns whether the ChargeSpot SDK is tracking
     * events.
     */
    protected synchronized boolean isChargeSpotTracking() {
        return mTracking;
    }
    //endregion

    //region DiscoveryCallback

    /**
     * ChargeSpot device discovered
     */
    @Override
    public synchronized void discoveredChargeSpotDevice(@NonNull CSChargeSpotRadio connectedDevice) {

        // No previous device discovered
        if (mChargeSpotClosestDevice == null) {
            this.mTraceLogger.Trace(CSChargeSpotTraceLevel.CHARGESPOT_TRACE_DEBUG,
                    "Found Device - " +
                            "Addr:" + connectedDevice.getChargeSpot().Address +
                            " -M:" + connectedDevice.getChargeSpot().Major +
                            " -m:" + connectedDevice.getChargeSpot().Minor +
                            " -RSSI:" + connectedDevice.getRssi() +
                            " -Dist:" + connectedDevice.getDistance());

            this.mRadioManager.addRadio(connectedDevice);

            // If battery was changed, we should use ChargeSpot Event's Options. Otherwise use Group Event's options.
            CSChargeSpotManagerOptions options = CSChargeSpotManager.getInstance().getChargeSpotOptions();

            mChargeSpotClosestDevice = this.mRadioManager.getClosestValidRadio(options.getMaximumRangingDistance());
            if (mChargeSpotClosestDevice != null) {
                this.mTraceLogger.Trace(CSChargeSpotTraceLevel.CHARGESPOT_TRACE_DEBUG,
                        "Closest Device - " +
                                "Addr:" + mChargeSpotClosestDevice.getChargeSpot().Address +
                                " -M:" + mChargeSpotClosestDevice.getChargeSpot().Major +
                                " -m:" + mChargeSpotClosestDevice.getChargeSpot().Minor +
                                " -Dist:" + mChargeSpotClosestDevice.getDistance());


                // ChargeSpot Event
                onChargeSpotChargeEvent();

                // Group Event
                if (!mChargeSpotClosestDevice.getChargeSpot().Address.equalsIgnoreCase(connectedDevice.getChargeSpot().Address)) {
                    onDeviceGroupEvent(CSGroupEvent.CSEventEnteredGroup);
                }

                this.stopChargeSpotTracking();
            }
        }
    }
    //endregion

    private static IntentFilter makeBatteryUpdateIntentFilter() {
        final IntentFilter intentFilter = new IntentFilter();
        intentFilter.addAction(Intent.ACTION_BATTERY_CHANGED);
        return intentFilter;
    }

    private synchronized void onDeviceGroupEvent(@NonNull CSGroupEvent event) {
        if (null == mGroupEventHandler) {
            return;
        }

        final CSGroupEventModel model = new CSGroupEventModel(
                this.mChargeSpotClosestDevice.getChargeSpot().UUID,
                this.mChargeSpotClosestDevice.getChargeSpot().Major,
                this.mChargeSpotClosestDevice.getChargeSpot().Minor,
                event);

        try {
            this.mInternalApiManager.listGroupHooks(model, new Callback<ChargeSpotGroupHookListModel>() {
                @Override
                public void onResponse(Call<ChargeSpotGroupHookListModel> call, Response<ChargeSpotGroupHookListModel> response) {
                    synchronized (mMutext) {
                        if (!response.isSuccessful()) {
                            CSChargeSpotHandler.this.handleAPIResponseError(response);
                            return;
                        }

                        if (mGroupEventHandler == null) {
                            return;
                        }
                        mGroupEventHandler.onGroupEventSuccess(model.Event, response.body());
                    }
                }

                @Override
                public void onFailure(Call<ChargeSpotGroupHookListModel> call, Throwable t) {
                    synchronized (mMutext) {
                        if (mGroupEventHandler == null) {
                            return;
                        }

                        mGroupEventHandler.onGroupEventFailure(new CSUnsupportedException(t.getMessage()));
                    }
                }
            });
        } catch (CSInvalidParameterException e) {
            mGroupEventHandler.onGroupEventFailure(e);
        }
    }

    private synchronized void onChargeSpotChargeEvent() {
        if (this.mChargeSpotClosestDevice == null) {
            return;
        }

        mChargeSpotEventHandler.onChargeSpotRanged(this.mChargeSpotClosestDevice.getChargeSpot());

        CSChargeSpotEvent event = CSChargeSpotEvent.CSEventStoppedChargingOnChargeSpot;
        if (mBatteryStatus.isCharging) {
            event = CSChargeSpotEvent.CSEventStartedChargingOnChargeSpot;
        }

        final CSChargeSpotEventModel model = new CSChargeSpotEventModel(
                this.mChargeSpotClosestDevice.getChargeSpot().UUID,
                this.mChargeSpotClosestDevice.getChargeSpot().Major,
                this.mChargeSpotClosestDevice.getChargeSpot().Minor,
                event);


        CSChargeSpotManagerOptions options = CSChargeSpotManager.getInstance().getChargeSpotOptions();

        if (!options.shouldRetrieveHookOnRange()) {
            if (mDelayEventHandler == null) {
                return;
            }
            mDelayEventHandler.onChargeSpotEvent(model.Event, null);
            return;
        }

        try {
            this.mInternalApiManager.listChargeSpotHooks(model, new Callback<ChargeSpotHookListModel>() {
                @Override
                public void onResponse(Call<ChargeSpotHookListModel> call, Response<ChargeSpotHookListModel> response) {
                    synchronized (mMutext) {
                        if (!response.isSuccessful()) {
                            CSChargeSpotHandler.this.handleAPIResponseError(response);
                            return;
                        }

                        if (mDelayEventHandler == null) {
                            return;
                        }
                        mDelayEventHandler.onChargeSpotEvent(model.Event, response.body());
                    }
                }

                @Override
                public void onFailure(Call<ChargeSpotHookListModel> call, Throwable t) {
                    synchronized (mMutext) {
                        if (mDelayEventHandler == null) {
                            return;
                        }
                        mChargeSpotEventHandler.onChargeSpotEventFailure(new CSUnsupportedException(t.getMessage()));
                    }
                }
            });
        } catch (CSInvalidParameterException e) {
            if (null != mChargeSpotEventHandler) {
                mChargeSpotEventHandler.onChargeSpotEventFailure(e);
            }
        }
    }
}
