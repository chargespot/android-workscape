package com.chargespot.workscape.Helpers;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;

import com.chargespot.workscape.R;
import com.chargespot.workscape.Requests.CSRequestManager;
import com.chargespot.workscape.Models.CSCalendarEvent;
import com.chargespot.workscape.Models.CSRoom;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;


/**
 * Created by kelvinlau on 2016-08-24.
 */

public class EndNowReceiver extends BroadcastReceiver {

    @Override
    public void onReceive(final Context context, Intent intent) {
        NotificationHelper.cancelNotification(context, intent);

        final CSRoom room = CSRoom.fromJson(intent.getStringExtra(NotificationHelper.NOTIFICATION_ROOM_KEY));
        if (room == null) {
            handleError(context);
            return;
        }

        final CSCalendarEvent event = CSCalendarEvent.fromJson(intent.getStringExtra(NotificationHelper.NOTIFICATION_EVENT_KEY));
        if (event == null) {
            handleError(context);
            return;
        }

        endBooking(context, room, event);
    }

    private void endBooking(final Context context, CSRoom room, CSCalendarEvent event) {
        try {
            CSRequestManager.getInstance(context).endNow(room.getIdentifier(), event.identifier, new Callback<CSCalendarEvent>() {
                @Override
                public void onResponse(Call<CSCalendarEvent> call, Response<CSCalendarEvent> response) {
                    NotificationHelper.createNotification(context, context.getString(context.getApplicationInfo().labelRes), context.getString(R.string.end_now_success));
                }

                @Override
                public void onFailure(Call<CSCalendarEvent> call, Throwable t) {
                    handleError(context);
                }
            });
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void handleError(Context context) {
        NotificationHelper.createNotification(context, context.getString(context.getApplicationInfo().labelRes), context.getString(R.string.booking_error));
    }
}
