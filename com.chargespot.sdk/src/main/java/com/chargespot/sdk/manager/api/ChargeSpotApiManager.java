/*
    Copyright (C) 2015 ChargeSpot
 */
package com.chargespot.sdk.manager.api;

import android.support.annotation.NonNull;
import android.support.annotation.Nullable;

import com.chargespot.sdk.BuildConfig;
import com.chargespot.sdk.CSChargeSpotManager;
import com.chargespot.sdk.api.v1.IChargeSpotApi;
import com.chargespot.sdk.api.v1.model.ChargeSpotGroupHookListModel;
import com.chargespot.sdk.api.v1.model.ChargeSpotGroupListModel;
import com.chargespot.sdk.api.v1.model.ChargeSpotHookListModel;
import com.chargespot.sdk.api.v1.model.ChargeSpotRegionListModel;
import com.chargespot.sdk.api.v1.model.events.CSChargeSpotEventModel;
import com.chargespot.sdk.api.v1.model.events.CSGroupEventModel;
import com.chargespot.sdk.config.CSChargeSpotConfig;
import com.chargespot.sdk.exceptions.CSInvalidParameterException;
import com.chargespot.sdk.helper.IDeviceInformationHelper;
import com.chargespot.sdk.helper.BeaconHelper;
import com.chargespot.sdk.manager.chargespot.ICSChargeSpotManagerInstance;
import com.chargespot.sdk.trace.CSChargeSpotTraceLevel;
import com.chargespot.sdk.trace.CSChargeSpotTraceLogger;

import java.io.IOException;
import java.util.concurrent.TimeUnit;

import okhttp3.Interceptor;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.Response;
import retrofit2.Callback;
import retrofit2.converter.gson.GsonConverterFactory;
import retrofit2.Retrofit;

/**
 * ChargeSpot SDK API interface. Used to execute general
 * API calls to retrieve information about the ChargeSpot groups
 * and regions. Also includes the internal APIs that are not exposed
 * through the SDK to the consumer.
 * <p/>
 * Date: 10/12/2015
 *
 * @version 0.3
 */
public final class ChargeSpotApiManager
        implements IChargeSpotApiManagerInternal {
    //region Internal
    private CSChargeSpotTraceLogger mTraceLogger = new CSChargeSpotTraceLogger();
    private String mToken;
    private Retrofit mRetroFit;
    private IDeviceInformationHelper mDeviceInformationHelper;
    private ICSChargeSpotManagerInstance mChargeSpotManager;

    private static final String sRequestOrigin = "Android SDK";
    //endregion

    //region API
    private IChargeSpotApi mChargeSpotApiService;
    //endregion

    {
        mToken = null;
        mRetroFit = null;
        mDeviceInformationHelper = null;
        mChargeSpotApiService = null;
    }

    /**
     * Setup the ChargeSpot API manager
     */
    public ChargeSpotApiManager(@NonNull final IDeviceInformationHelper deviceInformationHelper) {
        CSChargeSpotConfig config = CSChargeSpotManager.getInstance().getChargeSpotConfig();

        if (config == null) {
            this.mTraceLogger.Trace(CSChargeSpotTraceLevel.CHARGESPOT_TRACE_DEBUG, "Trying to instantiate APIManager but config was null.");
            return;
        }

        String APIEndpointUrl = config.getAPIEndpointUrlForEnvironment();
        this.mToken = config.getAppTokenForEnvironment();

        this.mTraceLogger.Trace(CSChargeSpotTraceLevel.CHARGESPOT_TRACE_DEBUG, "Endpoint: " + APIEndpointUrl);
        this.mTraceLogger.Trace(CSChargeSpotTraceLevel.CHARGESPOT_TRACE_DEBUG, "API Token: " + this.mToken);

        this.mDeviceInformationHelper = deviceInformationHelper;

        OkHttpClient okHttpClient = this.clientWithRetryInterceptor();

        this.mRetroFit = new Retrofit.Builder()
                .client(okHttpClient)
                .baseUrl(APIEndpointUrl)
                .addConverterFactory(GsonConverterFactory.create())
                .build();

        this.mChargeSpotApiService = this.mRetroFit.create(IChargeSpotApi.class);
    }

    /**
     * An interceptor that runs before every request. Retries failed requests 3 times before it passes a response
     *
     * @return OkHttpClient with an initialized interceptor that will retry requests
     */
    private OkHttpClient clientWithRetryInterceptor() {
        return new OkHttpClient.Builder()
                .connectTimeout(15, TimeUnit.SECONDS)
                .readTimeout(15, TimeUnit.SECONDS)
                .writeTimeout(15, TimeUnit.SECONDS)
                .addInterceptor(new Interceptor() {
            @Override
            public Response intercept(Interceptor.Chain chain) throws IOException {
                Request request = chain.request();
                // try the request
                Response response = chain.proceed(request);
                ChargeSpotApiManager.this.mTraceLogger.Trace(CSChargeSpotTraceLevel.CHARGESPOT_TRACE_DEBUG, "URL - " + request.url() + "\n" + "HEADERS - " + request.headers());

                int tryCount = 0;
                while (!response.isSuccessful() && shouldRetryRequest(response.code()) && tryCount < 3) {
                    tryCount++;

                    // retry the request
                    response = chain.proceed(request);
                    ChargeSpotApiManager.this.mTraceLogger.Trace(CSChargeSpotTraceLevel.CHARGESPOT_TRACE_DEBUG, "Retrying Request - " + tryCount);
                }

                // otherwise just pass the original response on
                return response;
            }
        }).build();
    }

    private boolean shouldRetryRequest(int errorCode) {
        boolean shouldRetry = true;
        switch (errorCode) {
            case 401:
            case 404:
                shouldRetry = false;
                break;
        }

        return shouldRetry;
    }

    //region PublicAPI

    /**
     * Retrieve the global list of ChargeSpot Regions
     *
     * @param callback Callback to return the list of chargespot regions
     * @throws CSInvalidParameterException
     */
    @Override
    public void listRegions(@Nullable Callback<ChargeSpotRegionListModel> callback)
            throws CSInvalidParameterException {
        if (null == callback) {
            throw new CSInvalidParameterException("callback");
        }

        this.mChargeSpotApiService.listRegions(
                this.mToken,
                this.mDeviceInformationHelper.getDeviceUID(),
                this.mDeviceInformationHelper.getFormattedDeviceInformation(),
                BuildConfig.VERSION_NAME,
                this.mDeviceInformationHelper.getDeviceLanguagePreference(),
                sRequestOrigin).enqueue(callback);
    }

    /**
     * Retrieve the global list of ChargeSpot groups.
     *
     * @param callback Callback to return the list of chargespot groups
     * @throws CSInvalidParameterException
     */
    @Override
    public void listGroups(@Nullable Callback<ChargeSpotGroupListModel> callback)
            throws CSInvalidParameterException {
        if (null == callback) {
            throw new CSInvalidParameterException("callback");
        }

        this.mChargeSpotApiService.listGroups(
                this.mToken,
                this.mDeviceInformationHelper.getDeviceUID(),
                this.mDeviceInformationHelper.getFormattedDeviceInformation(),
                BuildConfig.VERSION_NAME,
                this.mDeviceInformationHelper.getDeviceLanguagePreference(),
                sRequestOrigin).enqueue(callback);
    }
    //endregion

    //region InternalAPI

    /**
     * listGroupHooks returns the group hooks based on a specific
     * ChargeSpot instance
     *
     * @param groupEvent Group event that occurred
     * @param callback   Callback to return the list of chargespot regions
     */
    @Override
    public void listGroupHooks(@Nullable CSGroupEventModel groupEvent, @Nullable Callback<ChargeSpotGroupHookListModel> callback)
            throws CSInvalidParameterException {
        if (null == callback) {
            throw new CSInvalidParameterException("callback");
        }

        if (null == groupEvent || !groupEvent.isValid()) {
            throw new CSInvalidParameterException("groupEvent");
        }

        this.mChargeSpotApiService.listGroupHooks(
                this.mToken,
                this.mDeviceInformationHelper.getDeviceUID(),
                this.mDeviceInformationHelper.getFormattedDeviceInformation(),
                BuildConfig.VERSION_NAME,
                this.mDeviceInformationHelper.getDeviceLanguagePreference(),
                sRequestOrigin,
                BeaconHelper.formatUUID(groupEvent.UUID),
                groupEvent.Major,
                groupEvent.Minor,
                groupEvent.Event.getValue()).enqueue(callback);
    }

    /**
     * listChargeSpotHooks returns the chargespot hooks based on a specific
     * ChargeSpot instance
     *
     * @param chargeSpotEvent ChargeSpot event that occurred
     * @param callback        Callback to return the list of chargespot regions
     */
    @Override
    public void listChargeSpotHooks(@Nullable CSChargeSpotEventModel chargeSpotEvent, @Nullable Callback<ChargeSpotHookListModel> callback)
            throws CSInvalidParameterException {
        if (null == callback) {
            throw new CSInvalidParameterException("callback");
        }

        if (null == chargeSpotEvent || !chargeSpotEvent.isValid()) {
            throw new CSInvalidParameterException("groupEvent");
        }

        this.mChargeSpotApiService.listChargeSpotHooks(
                this.mToken,
                this.mDeviceInformationHelper.getDeviceUID(),
                this.mDeviceInformationHelper.getFormattedDeviceInformation(),
                BuildConfig.VERSION_NAME,
                this.mDeviceInformationHelper.getDeviceLanguagePreference(),
                sRequestOrigin,
                BeaconHelper.formatUUID(chargeSpotEvent.UUID),
                chargeSpotEvent.Major,
                chargeSpotEvent.Minor,
                chargeSpotEvent.Event.getValue()).enqueue(callback);
    }
    //endregion
}
