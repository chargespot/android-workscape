/*
    Copyright (C) 2015 ChargeSpot
 */
package com.chargespot.sdk.exceptions;

/**
 * ChargeSpot Exception
 *
 * @version 0.3
 */
public abstract class CSException
        extends Exception {
    /**
     * ChargeSpot exception
     */
    public CSException(final String message) {
        super(message);
    }
}
