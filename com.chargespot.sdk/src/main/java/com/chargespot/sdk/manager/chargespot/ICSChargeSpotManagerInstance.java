/*
    Copyright (C) 2015 ChargeSpot
 */
package com.chargespot.sdk.manager.chargespot;

import android.content.Context;
import android.support.annotation.Nullable;

import com.chargespot.sdk.api.v1.model.ChargeSpotRegionListModel;
import com.chargespot.sdk.config.CSChargeSpotConfig;
import com.chargespot.sdk.exceptions.CSBluetoothException;
import com.chargespot.sdk.exceptions.CSInvalidParameterException;
import com.chargespot.sdk.exceptions.CSNotInitializedException;
import com.chargespot.sdk.exceptions.CSUnsupportedException;
import com.chargespot.sdk.handler.ICSChargeSpotHandler;
import com.chargespot.sdk.manager.api.IChargeSpotApiManager;

/**
 * ChargeSpot manager interface, responsible
 * for handling all communications with the ChargeSpot SDK.
 *
 * @version 0.3
 */
public interface ICSChargeSpotManagerInstance
        extends ICSChargeSpotHandler {
    //region SDKSetup

    /**
     * Setup ChargeSpot SDK, and instantiate all the services to be
     * used in the SDK.
     *
     * @param context                    Context of the main activity
     * @param CSChargeSpotConfig         Config file with initiation informations
     * @param CSChargeSpotManagerOptions Options object with configurable parameters
     * @throws CSInvalidParameterException
     * @throws CSUnsupportedException
     * @throws CSBluetoothException
     */
    void setupChargeSpotSDK(@Nullable Context context, @Nullable CSChargeSpotConfig config) throws CSInvalidParameterException, CSUnsupportedException, CSBluetoothException;

    void setContext(Context context);

    /**
     * Check to see if the SDK is initialized
     *
     * @return Returns whether the ChargeSpot SDK has
     * been successfully initialized.
     */
    boolean isChargeSpotSDKInitialized();
    //endregion

    //region APIManager

    /**
     * Get the API manager for CS APIs
     *
     * @return Returns the ChargeSpot API manager
     * that can be used to communication with the ChargeSpot
     * servers directly.
     * @throws CSNotInitializedException
     */
    IChargeSpotApiManager getChargeSpotApiManager() throws CSNotInitializedException;
    //endregion

    /**
     * Get the config settings for ChargeSpot Manager
     *
     * @return Returns CSChargeSpotConfig object that is set from setup
     */
    CSChargeSpotConfig getChargeSpotConfig();


    /**
     * Get the region list that is requested when setupChargeSpotSDK is called
     *
     * @return ChargeSpotRegionListModel An object that contains an array of ChargeSpotRegionModel
     */
    ChargeSpotRegionListModel getRegionList();

    /**
     * Set the region list for the BLE scanner filter
     *
     * @param regionList
     */
    void setRegionList(ChargeSpotRegionListModel regionList);

    /**
     * Get the current registered ChargeSpot options
     *
     * @return CSChargeSpotManagerOptions An object that contains options for ChargeSpot Events
     */
    CSChargeSpotManagerOptions getChargeSpotOptions();

    /**
     * Get the current registered Group options
     *
     * @return CSChargeSpotManagerOptions An object that contains options for Group Events
     */
    CSChargeSpotManagerOptions getGroupOptions();

    /**
     * Start ranging for chargespot beacon
     * Will trigger a chargespot event if a beacon is found
     *
     */

    void startRangingForChargeSpotBeacons();
}
