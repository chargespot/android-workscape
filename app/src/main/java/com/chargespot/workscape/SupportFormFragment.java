package com.chargespot.workscape;


import android.animation.Animator;
import android.animation.AnimatorListenerAdapter;
import android.annotation.TargetApi;
import android.app.Activity;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;

import android.net.Uri;
import android.os.Bundle;
import android.os.Environment;
import android.provider.MediaStore;
import android.support.annotation.Nullable;
import android.support.v4.content.ContextCompat;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.ScrollView;
import android.widget.TextView;
import android.widget.Toast;

import com.chargespot.workscape.Requests.CSRequestManager;

import java.io.File;
import java.io.IOException;

import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Created by kelvinlau on 2016-07-27.
 */

public class SupportFormFragment extends BaseFragment {

    public interface SupportFormFragmentListener {
        void returnHome();
    }

    private SupportFormFragmentListener mListener;

    public static final String TAG = SupportFormFragment.class.getCanonicalName();
    private static final int CAPTURE_IMAGE_ACTIVITY_REQUEST_CODE = 1888;
    private static final int PERMISSIONS_REQUEST_EXTERNAL_STORAGE = 1889;

    private static final String tempImageName = "temp.jpg";

    private boolean mIsTemperatureForm;

    private Button mSubmitButton;
    private TextView mLocationTextView;
    private TextView mDetailsTextView;

    private ImageButton mCameraButton;
    private ImageView mCameraImageView;
    private Bitmap mSubmissionImage;

    private ScrollView mScrollView;
    private RelativeLayout mCompletionView;
    private Button mHomeButton;

    private View mMaskView;
    private ProgressBar mProgressSpinner;

    private int mCategory;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_support_form, container, false);
        mSubmitButton = (Button)view.findViewById(R.id.submitButton);
        mLocationTextView = (TextView)view.findViewById(R.id.locationTextView);
        mDetailsTextView = (TextView)view.findViewById(R.id.detailsTextView);
        mCameraButton = (ImageButton)view.findViewById(R.id.cameraButton);
        mCameraImageView = (ImageView)view.findViewById(R.id.cameraImageView);

        mScrollView = (ScrollView)view.findViewById(R.id.supportScrollView);
        mCompletionView = (RelativeLayout)view.findViewById(R.id.completionView);
        mHomeButton = (Button)view.findViewById(R.id.homeButton);


        mMaskView = view.findViewById(R.id.maskView);
        mProgressSpinner = (ProgressBar)view.findViewById(R.id.progressSpinner);

        mLocationTextView.setText(getString(R.string.my_desk));

        if (mIsTemperatureForm) {
            view.findViewById(R.id.detailsLinearLayout).setVisibility(View.GONE);
            mCameraButton.setVisibility(View.GONE);
        }

        mSubmitButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                submitSupportTicket();
            }
        });

        mCameraButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (requestPermissions()) {
                    startGalleryIntent();
                }
            }
        });

        mHomeButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mListener.returnHome();
            }
        });

        return view;
    }


    private void submitSupportTicket() {
        setLoading(true);

        String location = mLocationTextView.getText().toString();
        if (location.length() == 0) {
            Toast.makeText(getActivity(), getString(R.string.location_required), Toast.LENGTH_SHORT).show();
            return;
        }

        String message = mDetailsTextView.getText().toString();

        try {
            CSRequestManager.getInstance(getActivity()).submitSupportTicket(mSubmissionImage, location, message, mCategory, new Callback<ResponseBody>() {
                @Override
                public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                    Log.d(TAG, "Support ticket request successful - " + response.code() + " - " + response.message());
                    setLoading(false);
                    finishSubmission();
                }

                @Override
                public void onFailure(Call<ResponseBody> call, Throwable t) {
                    Log.d(TAG, "Support ticket request failed - " + t.getLocalizedMessage());
                    Toast.makeText(getActivity(), "There was a problem submitting your ticket, please try again later.", Toast.LENGTH_SHORT);
                    setLoading(false);
                }
            });
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void setLoading(boolean loading) {
        if (loading) {
            mLocationTextView.setEnabled(false);
            mDetailsTextView.setEnabled(false);
            mCameraButton.setEnabled(false);
            mSubmitButton.setEnabled(false);

            mMaskView.setVisibility(View.VISIBLE);
            mProgressSpinner.setVisibility(View.VISIBLE);
        } else {
            mLocationTextView.setEnabled(true);
            mDetailsTextView.setEnabled(true);
            mCameraButton.setEnabled(true);
            mSubmitButton.setEnabled(true);

            mMaskView.setVisibility(View.GONE);
            mProgressSpinner.setVisibility(View.GONE);
        }
    }

    private void finishSubmission() {
        mScrollView.animate()
                .alpha(0f)
                .setDuration(600)
                .setListener(new AnimatorListenerAdapter() {
                    @Override
                    public void onAnimationEnd(Animator animation) {
                        mScrollView.setVisibility(View.GONE);
                    }
                });

        mCompletionView.setAlpha(0);
        mCompletionView.setVisibility(View.VISIBLE);
        mCompletionView.animate()
                .alpha(1f)
                .setDuration(600)
                .setListener(null);

    }

    private void startGalleryIntent() {
        Intent galleryIntent = new Intent(Intent.ACTION_PICK, MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
        Intent cameraIntent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
        File f = new File(android.os.Environment.getExternalStorageDirectory(), tempImageName);
        cameraIntent.putExtra(MediaStore.EXTRA_OUTPUT, Uri.fromFile(f));

        Intent chooser = new Intent(Intent.ACTION_CHOOSER);
        chooser.putExtra(Intent.EXTRA_INTENT, galleryIntent);
        chooser.putExtra(Intent.EXTRA_TITLE, "Upload photo");

        Intent[] intentArray = {cameraIntent};
        chooser.putExtra(Intent.EXTRA_INITIAL_INTENTS, intentArray);
        startActivityForResult(chooser, CAPTURE_IMAGE_ACTIVITY_REQUEST_CODE);
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (requestCode == CAPTURE_IMAGE_ACTIVITY_REQUEST_CODE && resultCode == Activity.RESULT_OK) {
            if (data.getData() != null) {
                processGalleryImage(data);
            } else {
                File f = fetchTempPhoto(tempImageName);
                try {
                    BitmapFactory.Options bitmapOptions = new BitmapFactory.Options();
                    mSubmissionImage = BitmapFactory.decodeFile(f.getAbsolutePath(),
                            bitmapOptions);

                    updateCameraImageView(mSubmissionImage);
                    f.delete();

                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        }
    }

    private void processGalleryImage(Intent data) {
        try {
            Uri uri = data.getData();

            mSubmissionImage = MediaStore.Images.Media.getBitmap(getActivity().getContentResolver(), uri);
            updateCameraImageView(mSubmissionImage);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    private File fetchTempPhoto(String filename) {
        File f = new File(Environment.getExternalStorageDirectory().toString());
        for (File temp : f.listFiles()) {
            if (temp.getName().equals(filename)) {
                return temp;
            }
        }

        return null;
    }

    private void updateCameraImageView(Bitmap bitmap) {
        mCameraImageView.setImageBitmap(bitmap);
    }

    @TargetApi(23)
    private boolean hasPermissions(String string) {
        return ContextCompat.checkSelfPermission(getActivity(), string) == PackageManager.PERMISSION_GRANTED;
    }

    private boolean requestPermissions() {
        boolean permissions = true;
        if (!hasPermissions(android.Manifest.permission.READ_EXTERNAL_STORAGE)) {
            requestPermissions(new String[]{android.Manifest.permission.READ_EXTERNAL_STORAGE,
                            android.Manifest.permission.WRITE_EXTERNAL_STORAGE},
                    PERMISSIONS_REQUEST_EXTERNAL_STORAGE);
            permissions = false;
        }

        return permissions;
    }

    @Override
    public void onRequestPermissionsResult(int requestCode,
                                           String permissions[], int[] grantResults) {
        switch (requestCode) {
            case PERMISSIONS_REQUEST_EXTERNAL_STORAGE: {
                // If request is cancelled, the result arrays are empty.
                if (grantResults.length > 0
                        && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    startGalleryIntent();
                } else {

                    // permission denied, boo! Disable the
                    // functionality that depends on this permission.
                }
                return;
            }
        }
    }

    public void setupTemperatureForm() {
        mIsTemperatureForm = true;
    }

    public void setCategory(int category) {
        mCategory = category;
    }

    public void setSupportFormFragmentListener(SupportFormFragmentListener listener) {
        mListener = listener;
    }
}
