/*
    Copyright (C) 2015 ChargeSpot
 */
package com.chargespot.sdk.api.v1.model.events;

import android.test.suitebuilder.annotation.SmallTest;

import com.chargespot.sdk.events.CSChargeSpotEvent;

import junit.framework.Assert;
import junit.framework.TestCase;

/**
 * CSChargeSpotEventModelTest
 *
 * Date: 10/12/2015
 * @version 0.3
 */
public class CSChargeSpotEventModelTest
    extends TestCase
{
    @SmallTest
    public void testAssignment()
        throws Exception
    {
        final String UUID = "UUID";
        final int Major = 1;
        final int Minor = 2;
        final CSChargeSpotEvent Event = CSChargeSpotEvent.CSEventStartedChargingOnChargeSpot;

        CSChargeSpotEventModel model = new CSChargeSpotEventModel(UUID, Major, Minor, Event);

        Assert.assertEquals(UUID, model.UUID);
        Assert.assertEquals(Major, model.Major);
        Assert.assertEquals(Minor, model.Minor);
        Assert.assertEquals(Event, model.Event);
    }

    @SmallTest
    public void testAssignmentComplete()
            throws Exception
    {
        final String Address = "Address";
        final String UUID = "UUID";
        final int Major = 1;
        final int Minor = 2;
        final CSChargeSpotEvent Event = CSChargeSpotEvent.CSEventStartedChargingOnChargeSpot;

        CSChargeSpotEventModel model = new CSChargeSpotEventModel(Address, UUID, Major, Minor, Event);

        Assert.assertEquals(Address, model.Address);
        Assert.assertEquals(UUID, model.UUID);
        Assert.assertEquals(Major, model.Major);
        Assert.assertEquals(Minor, model.Minor);
        Assert.assertEquals(Event, model.Event);
    }

    @SmallTest
    public void testIsValid()
        throws Exception
    {
        CSChargeSpotEventModel model = new CSChargeSpotEventModel();
        Assert.assertFalse(model.isValid());

        model.UUID = "";
        Assert.assertFalse(model.isValid());

        model.UUID = "UUID";
        Assert.assertFalse(model.isValid());

        model.Event = CSChargeSpotEvent.CSEventStartedChargingOnChargeSpot;
        Assert.assertTrue(model.isValid());
    }

    @SmallTest
    public void testIsValidWithoutAddress()
            throws Exception
    {
        CSChargeSpotEventModel model = new CSChargeSpotEventModel();
        model.Address = null;
        model.UUID = "UUID";
        model.Event = CSChargeSpotEvent.CSEventStartedChargingOnChargeSpot;
        Assert.assertTrue(model.isValid());

        model.Address = "";
        Assert.assertTrue(model.isValid());
    }
}