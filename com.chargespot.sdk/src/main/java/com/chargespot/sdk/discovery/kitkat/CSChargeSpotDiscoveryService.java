/*
    Copyright (C) 2015 ChargeSpot
 */
package com.chargespot.sdk.discovery.kitkat;

import android.annotation.TargetApi;
import android.bluetooth.BluetoothAdapter;
import android.bluetooth.BluetoothDevice;
import android.bluetooth.BluetoothManager;
import android.content.Context;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;

import com.chargespot.sdk.discovery.ICSChargeSpotDiscoveryCallback;
import com.chargespot.sdk.discovery.ICSChargeSpotDiscoveryService;
import com.chargespot.sdk.exceptions.CSBluetoothErrors;
import com.chargespot.sdk.exceptions.CSBluetoothException;
import com.chargespot.sdk.exceptions.CSInvalidParameterException;
import com.chargespot.sdk.exceptions.CSUnsupportedException;
import com.chargespot.sdk.model.CSChargeSpotRadio;
import com.chargespot.sdk.trace.CSChargeSpotTraceLevel;
import com.chargespot.sdk.trace.ICSChargeSpotTraceLogger;

import java.util.Timer;
import java.util.TimerTask;

/**
 * ChargeSpot handler interface for handling
 * all of the events being received from the devices, and
 * executing proper actions related to them.
 *
 * @version 0.5
 */
@TargetApi(18)
public final class CSChargeSpotDiscoveryService
        implements ICSChargeSpotDiscoveryService {
    private Context mContext;

    //region Bluetooth
    private BluetoothManager mBluetoothManager;
    private BluetoothAdapter mBluetoothAdapter;
    //endregion

    private Timer mScanTimer;

    //region Interfaces
    private ICSChargeSpotDiscoveryCallback mChargeSpotCallback;
    private ICSChargeSpotTraceLogger mTraceLogger;
    //endregion

    private boolean mScanning;

    {
        mTraceLogger = null;
        mContext = null;
        mChargeSpotCallback = null;
        mBluetoothAdapter = null;
        mBluetoothManager = null;
        mScanning = false;
    }

    /**
     * BLE scanner callback.
     */
    private final BluetoothAdapter.LeScanCallback mBleScanCallback = new BluetoothAdapter.LeScanCallback() {
        /**
         * LE scan completed
         * @param device Bluetooth device broadcasting
         * @param rssi Signal strength
         * @param scanRecord Scan record
         */
        @Override
        public void onLeScan(final BluetoothDevice device, int rssi, byte[] scanRecord) {
            if (null == device) {
                return;
            }

            if (CSChargeSpotDiscoveryService.this.mChargeSpotCallback != null) {
                try {
                    CSChargeSpotDiscoveryService.this.onDeviceDiscovered(device.getAddress(), scanRecord, rssi);
                } catch (CSInvalidParameterException e) {
                    CSChargeSpotDiscoveryService.this.mTraceLogger.Trace(CSChargeSpotTraceLevel.CHARGESPOT_TRACE_ERROR, e.getMessage());
                }
            }
        }
    };

    /**
     * Create a CS discovery service
     *
     * @param traceLogger Trace logger user to handle the events
     * @param context     Application context
     * @throws CSUnsupportedException
     */
    public CSChargeSpotDiscoveryService(@NonNull final ICSChargeSpotTraceLogger traceLogger, @NonNull final Context context)
            throws CSUnsupportedException {
        this.mTraceLogger = traceLogger;
        this.mContext = context;

        this.mBluetoothManager = (BluetoothManager) this.mContext.getSystemService(Context.BLUETOOTH_SERVICE);
        if (this.mBluetoothManager == null) {
            this.mTraceLogger.Trace(CSChargeSpotTraceLevel.CHARGESPOT_TRACE_ERROR, "No BlueTooth Manager.");
            throw new CSUnsupportedException("BluetoothManager");
        }

        this.mBluetoothAdapter = mBluetoothManager.getAdapter();
        if (this.mBluetoothAdapter == null) {
            this.mTraceLogger.Trace(CSChargeSpotTraceLevel.CHARGESPOT_TRACE_ERROR, "No BlueTooth Adapter.");
            throw new CSUnsupportedException("BluetoothAdapter");
        }
    }

    //region DiscoveryInterface

    /**
     * Register a ChargeSpot discovery
     * callback.
     *
     * @param callback Callback for handling discovery events
     */
    @Override
    public synchronized void registerChargeSpotDiscoveryCallback(@Nullable ICSChargeSpotDiscoveryCallback callback) {
        this.mTraceLogger.Trace(CSChargeSpotTraceLevel.CHARGESPOT_TRACE_DEBUG, "Callback registered");
        this.mChargeSpotCallback = callback;
    }

    /**
     * Unregister a ChargeSpot discovery callback
     */
    @Override
    public synchronized void unregisterChargeSpotDiscoveryCallback() {
        this.mTraceLogger.Trace(CSChargeSpotTraceLevel.CHARGESPOT_TRACE_DEBUG, "Callback unregistered");
        this.mChargeSpotCallback = null;
    }

    /**
     * Start the discovery
     */
    @Override
    public synchronized void startDiscovery() throws CSBluetoothException {
        if (!mScanning) {
            this.mTraceLogger.Trace(CSChargeSpotTraceLevel.CHARGESPOT_TRACE_DEBUG, "Discovery started");
            if (!this.mBluetoothAdapter.isEnabled()) {
                if (!this.mBluetoothAdapter.enable()) {
                    throw new CSBluetoothException("Bluetooth not enabled", CSBluetoothErrors.BluetoothNotEnabled);
                }
            }
            mBluetoothAdapter.startLeScan(mBleScanCallback);
            this.scheduleScan();
            mScanning = true;
        }
    }

    /**
     * Stop the discovery
     */
    @Override
    public synchronized void stopDiscovery() {
        if (mScanning) {
            this.mTraceLogger.Trace(CSChargeSpotTraceLevel.CHARGESPOT_TRACE_DEBUG, "Discovery stopped");
            this.mScanTimer.cancel();
            mBluetoothAdapter.stopLeScan(mBleScanCallback);
            mScanning = false;
        }
    }
    //endregion

    /**
     * Device has been discovered
     *
     * @param address    Address of the device
     * @param scanRecord BLE scan record
     * @param rssi       Strength of the signal
     * @throws CSInvalidParameterException
     */
    private synchronized void onDeviceDiscovered(final String address, final byte[] scanRecord, final int rssi)
            throws CSInvalidParameterException {
        CSChargeSpotRadio deviceConnection = DeviceDiscoveryHelper.parseBeaconRecord(address, scanRecord, rssi);
        if (deviceConnection != null) {
            this.mChargeSpotCallback.discoveredChargeSpotDevice(deviceConnection);
        }
    }

    /**
     * Fix for KitKat where LeScan will stop receiving new devices in callback
     * Restarting it seems to stop it from getting stuck
     */
    private void scheduleScan() {

        this.mScanTimer = new Timer();
        TimerTask scanTask = new TimerTask() {
            @Override
            public void run() {
                mBluetoothAdapter.stopLeScan(mBleScanCallback);
                mBluetoothAdapter.startLeScan(mBleScanCallback);
            }
        };
        this.mScanTimer.schedule(scanTask, 1100, 1100);
    }
}
