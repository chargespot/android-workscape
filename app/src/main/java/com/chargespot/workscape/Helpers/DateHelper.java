package com.chargespot.workscape.Helpers;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

/**
 * Created by kelvinlau on 2016-08-02.
 */

public class DateHelper {
    static final long ONE_MINUTE_IN_MILLIS=60000;//millisecs

    public static Date roundDateToNearestFifteenMinutes(Date date) {
        Calendar calendar = Calendar.getInstance();
        calendar.setTime(date);

        int unroundedMinutes = calendar.get(Calendar.MINUTE);
        int mod = unroundedMinutes % 15;
        calendar.add(Calendar.MINUTE, 30 - mod);
        calendar.set(Calendar.SECOND, 0);

        return calendar.getTime();
    }

    public static Date addMinutesToDate(Date date, int minutes) {
        Calendar calendar = Calendar.getInstance();
        calendar.setTime(date);

        long currentDateMilliSeconds = date.getTime();

        return new Date(currentDateMilliSeconds + (minutes * ONE_MINUTE_IN_MILLIS));
    }

    public static String stringForDateFormat(Date date, String format) {
        DateFormat formatter = new SimpleDateFormat(format);
        return formatter.format(date);
    }

    public static CharSequence[] dateStringsForList(List<Date> dateList, String format) {
        ArrayList<String> dateStringList = new ArrayList<>();
        for (Date date : dateList) {
            dateStringList.add(stringForDateFormat(date, format));
        }

        return dateStringList.toArray(new CharSequence[dateStringList.size()]);
    }
}
