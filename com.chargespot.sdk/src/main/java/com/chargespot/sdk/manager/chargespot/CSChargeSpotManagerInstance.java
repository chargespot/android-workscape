/*
    Copyright (C) 2015 ChargeSpot
 */
package com.chargespot.sdk.manager.chargespot;

import android.content.Context;
import android.content.pm.PackageManager;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.util.Log;

import com.chargespot.sdk.api.v1.model.ChargeSpotRegionListModel;
import com.chargespot.sdk.config.CSChargeSpotConfig;
import com.chargespot.sdk.events.ICSChargeSpotEventHandler;
import com.chargespot.sdk.events.ICSGroupEventHandler;
import com.chargespot.sdk.exceptions.CSBluetoothException;
import com.chargespot.sdk.exceptions.CSInvalidParameterException;
import com.chargespot.sdk.exceptions.CSNotInitializedException;
import com.chargespot.sdk.exceptions.CSUnsupportedException;
import com.chargespot.sdk.handler.CSChargeSpotHandler;
import com.chargespot.sdk.handler.ICSChargeSpotErrorHandler;
import com.chargespot.sdk.helper.DeviceInformationHelper;
import com.chargespot.sdk.manager.api.ChargeSpotApiManager;
import com.chargespot.sdk.manager.api.IChargeSpotApiManager;
import com.chargespot.sdk.manager.api.IChargeSpotApiManagerInternal;
import com.chargespot.sdk.state.ICSDeviceStateHandler;
import com.chargespot.sdk.trace.CSChargeSpotTraceLevel;
import com.chargespot.sdk.trace.CSChargeSpotTraceLogger;
import com.chargespot.sdk.trace.ICSChargeSpotTraceLogger;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * ChargeSpot manager interface, responsible
 * for handling all communications with the ChargeSpot SDK.
 *
 * @version 0.3
 */
public final class CSChargeSpotManagerInstance
        implements ICSChargeSpotManagerInstance {
    //region Trace
    private ICSChargeSpotTraceLogger mTraceLogger;
    //endregion

    //region Setup
    private Context mContext;
    private String mAPIToken;
    private ChargeSpotRegionListModel mRegionList;
    //endregion

    private IChargeSpotApiManagerInternal mChargeSpotApiManager;
    private CSChargeSpotHandler mChargeSpotHandler;
    private CSChargeSpotConfig mChargeSpotConfig;
    private CSChargeSpotManagerOptions mChargeSpotOptions;
    private CSChargeSpotManagerOptions mGroupOptions;

    //endregion

    //region Callbacks
    //endregion

    //region Initialization
    {
        mTraceLogger = null;
        mContext = null;
        mAPIToken = null;
        mChargeSpotApiManager = null;
    }
    //endregion

    /**
     * Setup ChargeSpot SDK, and instantiate all the services to be
     * used in the SDK.
     */
    public CSChargeSpotManagerInstance() {
        mTraceLogger = new CSChargeSpotTraceLogger();
    }

    //region SDKSetup

    /**
     * Setup ChargeSpot SDK, and instantiate all the services to be
     * used in the SDK.
     *
     * @param context                    Context of the main activity
     * @param CSChargeSpotConfig         Config object with initiation informations
     * @param CSChargeSpotManagerOptions Options object with configurable parameters
     * @throws CSInvalidParameterException
     * @throws CSUnsupportedException
     * @throws CSBluetoothException
     */
    @Override
    public synchronized void setupChargeSpotSDK(@Nullable Context context, @NonNull CSChargeSpotConfig config)
            throws CSInvalidParameterException,
            CSUnsupportedException,
            CSBluetoothException {
        if (config == null ) {
            throw new CSInvalidParameterException("config");
        }

        this.mChargeSpotConfig = config;
        this.mAPIToken = this.mChargeSpotConfig.getAppTokenForEnvironment();

        if (null == this.mAPIToken || this.mAPIToken.isEmpty()) {
            this.mTraceLogger.Trace(CSChargeSpotTraceLevel.CHARGESPOT_TRACE_ERROR, "Token is empty");
            throw new CSInvalidParameterException("token");
        }

        this.mTraceLogger.Trace(CSChargeSpotTraceLevel.CHARGESPOT_TRACE_DEBUG, "ChargeSpot SDK Setup Start");
        this.mTraceLogger.Trace(CSChargeSpotTraceLevel.CHARGESPOT_TRACE_DEBUG, "Token: " + this.mAPIToken);

        if (null == context) {
            this.mTraceLogger.Trace(CSChargeSpotTraceLevel.CHARGESPOT_TRACE_ERROR, "Context is NULL");
            throw new CSInvalidParameterException("context");
        }

        this.mContext = context;

        if (!this.mContext.getPackageManager().hasSystemFeature(PackageManager.FEATURE_BLUETOOTH_LE)) {
            this.mTraceLogger.Trace(CSChargeSpotTraceLevel.CHARGESPOT_TRACE_ERROR, "BLE is not supported");
            throw new CSUnsupportedException("BLE");
        }

        if (!isChargeSpotSDKInitialized()) {
            this.mChargeSpotApiManager = new ChargeSpotApiManager(new DeviceInformationHelper(context, mTraceLogger));
            this.mChargeSpotHandler = new CSChargeSpotHandler(this.mTraceLogger, this.mContext, this.mChargeSpotApiManager);
            Log.d("CHARGE_SPOT_SDK", "Starting list regions from manager");

            this.mChargeSpotApiManager.listRegions(new Callback<ChargeSpotRegionListModel>() {
                @Override
                public void onResponse(Call<ChargeSpotRegionListModel> call, Response<ChargeSpotRegionListModel> response) {

                    if (response == null || !response.isSuccessful()) {
                        CSChargeSpotManagerInstance.this.mChargeSpotHandler.handleAPIResponseError(response);
                    } else {
                        CSChargeSpotManagerInstance.this.mRegionList = response.body();
                        try {
                            CSChargeSpotManagerInstance.this.mChargeSpotHandler.executeChargeSpotDiscovery();
                        } catch (Exception e) { }
                    }
                }

                @Override
                public void onFailure(Call<ChargeSpotRegionListModel> call, Throwable t) {
                    CSChargeSpotManagerInstance.this.mChargeSpotHandler.handleFailedRequest(t);
                }
            });
        }
        this.mTraceLogger.Trace(CSChargeSpotTraceLevel.CHARGESPOT_TRACE_DEBUG, "ChargeSpot SDK Setup Done");
    }

    public void setContext(Context context) {
        mContext = context;
    }

    /**
     * Check to see if the SDK is initialized
     *
     * @return Returns whether the ChargeSpot SDK has
     * been successfully initialized.
     */
    @Override
    public synchronized boolean isChargeSpotSDKInitialized() {
        // Check initialization
        if (this.mContext == null || this.mAPIToken == null || this.mAPIToken.isEmpty() || this.mChargeSpotConfig == null) {
            return false;
        }

        // Check preconditions
        return this.mChargeSpotApiManager != null && mChargeSpotHandler != null;
    }
    //endregion

    //region APIManager

    /**
     * Get the API manager for CS APIs
     *
     * @return Returns the ChargeSpot API manager
     * that can be used to communication with the ChargeSpot
     * servers directly.
     * @throws CSNotInitializedException
     */
    @Override
    public synchronized IChargeSpotApiManager getChargeSpotApiManager()
            throws CSNotInitializedException {
        checkChargeSpotSDKInitialized();

        return this.mChargeSpotApiManager;
    }
    //endregion

    //region ChargeSpotEvents

    /**
     * Register for ChargeSpot events to be received
     *
     * @param callback Callback to execute upon receiving a ChargeSpot event
     * @throws CSNotInitializedException
     */
    public synchronized void registerForChargeSpotEvents(@Nullable ICSChargeSpotEventHandler callback, @Nullable CSChargeSpotManagerOptions options)
            throws CSNotInitializedException {
        checkChargeSpotSDKInitialized();

        this.mChargeSpotOptions = (options != null) ? options : new CSChargeSpotManagerOptions();
        this.mChargeSpotHandler.registerForChargeSpotEvents(callback, options);
    }

    /**
     * Unregister for ChargeSpot events to be received
     *
     * @throws CSNotInitializedException
     */
    public synchronized void unregisterForChargeSpotEvents()
            throws CSNotInitializedException {
        checkChargeSpotSDKInitialized();

        this.mChargeSpotHandler.unregisterForChargeSpotEvents();
    }
    //endregion

    //region GroupEvent

    /**
     * Register for Group events to be received
     *
     * @param callback Callback to execute upon receiving a Group event
     * @throws CSNotInitializedException
     */
    public synchronized void registerForGroupEvents(@Nullable ICSGroupEventHandler callback, @Nullable CSChargeSpotManagerOptions options)
            throws CSNotInitializedException {
        checkChargeSpotSDKInitialized();

        this.mGroupOptions = (options != null) ? options : new CSChargeSpotManagerOptions();
        this.mChargeSpotHandler.registerForGroupEvents(callback, options);
    }

    /**
     * Unregister for Group events to be received.
     *
     * @throws CSNotInitializedException
     */
    public synchronized void unregisterForGroupEvents()
            throws CSNotInitializedException {
        checkChargeSpotSDKInitialized();

        this.mChargeSpotHandler.unregisterForGroupEvents();
    }
    //endregion

    //region

    /**
     * Register for SDK error events
     *
     * @param callback Callback to execute upon receiving a error
     */
    public synchronized void registerForChargeSpotErrors(ICSChargeSpotErrorHandler callback) {
        this.mChargeSpotHandler.registerForChargeSpotErrors(callback);
    }

    /**
     * Unregister for SDK errors
     */
    public synchronized void unregisterForChargeSpotErrors() {
        this.mChargeSpotHandler.unregisterForChargeSpotErrors();
    }

    //endregion


    //region DeviceState

    /**
     * Retrieve the current device state. This will return the last known
     * state of the device.
     *
     * @param callback Callback to be executed upon device state retrieval
     * @throws CSNotInitializedException
     */
    public synchronized void retrieveDeviceState(@Nullable ICSDeviceStateHandler callback)
            throws CSNotInitializedException {
        checkChargeSpotSDKInitialized();

        this.mChargeSpotHandler.retrieveDeviceState(callback);
    }
    //endregion

    /**
     * Handle the ChargeSpot SDK not initialized event
     */
    private void checkChargeSpotSDKInitialized() throws CSNotInitializedException {
        if (!this.isChargeSpotSDKInitialized()) {
            this.mTraceLogger.Trace(CSChargeSpotTraceLevel.CHARGESPOT_TRACE_ERROR, "ChargeSpot SDK not initialized");
            throw new CSNotInitializedException("ChargeSpot SDK not initialized");
        }
    }

    /**
     * Get the config settings for ChargeSpot Manager
     *
     * @return Returns CSChargeSpotConfig object that is set from setup
     */
    public CSChargeSpotConfig getChargeSpotConfig() {
        return this.mChargeSpotConfig;
    }

    /**
     * Get the region list that is requested when setupChargeSpotSDK is called
     *
     * @return ChargeSpotRegionListModel An object that contains an array of ChargeSpotRegionModel
     */
    public ChargeSpotRegionListModel getRegionList() {
        return this.mRegionList;
    }

    public void setRegionList(ChargeSpotRegionListModel regionList) {
        this.mRegionList = regionList;
    }

    /**
     * Get the current registered ChargeSpot options
     *
     * @return CSChargeSpotManagerOptions An object that contains options for ChargeSpot Events
     */
    public CSChargeSpotManagerOptions getChargeSpotOptions() {
        if (this.mChargeSpotOptions == null) {
            this.mChargeSpotOptions = new CSChargeSpotManagerOptions();
        }
        return this.mChargeSpotOptions;
    }

    /**
     * Get the current registered Group options
     *
     * @return CSChargeSpotManagerOptions An object that contains options for Group Events
     */
    public CSChargeSpotManagerOptions getGroupOptions() {
        if (this.mGroupOptions == null) {
            this.mGroupOptions = new CSChargeSpotManagerOptions();
        }
        return this.mGroupOptions;
    }

    public void startRangingForChargeSpotBeacons() {
        if (this.isChargeSpotSDKInitialized()) {
            try {
                this.mChargeSpotHandler.executeChargeSpotDiscovery();
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }
}
