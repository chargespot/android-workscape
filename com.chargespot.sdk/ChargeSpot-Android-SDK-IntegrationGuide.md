# ChargeSpot Android SDK API Reference
_Version 1.2.3 (v1.2.3)_

-----------------------------------------

## Integration Guide
### Summary
ChargeSpotSDK is a drop in Android SDK to allow any app to interact with ChargeSpots by responding to events and allowing the consumer to make general API calls to retrieve information about the ChargeSpot. The main class, _CSChargeSpotManager_, is used to register for ChargeSpot and Group events. Please refer to the javadocs for detailed usage and class definitions. 

Please note that the minimal Android OS version for the ChargeSpotSDK is KitKat 4.3 (API level 18).

The SDK is built using Android Studio 2.0. If you require a build for an older version of Android Studio or Eclipse, please contact us at [connect@chargespot.ca](mailto:connect@chargespot.ca)

###Definitions
#####ChargeSpot
A wireless charging station that contains an iBeacon. Bluetooth needs to be enabled on a BLE compatible device to detect when a device is charging on a ChargeSpot. It is defined by the _CSChargeSpot_ class.

#####Group
A location that can contain multiple ChargeSpot devices. It is defined by the _CSGroup_ class.

#####Hook
A custom action that can be attached to ChargeSpot or Group events. The application can make use of the returned Hooks or use their own internal event engine. It is defined by _CSHook_ class.

###Prerequisites
Before using the SDK, the consumer will need to obtain an application token provided by ChargeSpot. Please contact us at [connect@chargespot.ca](mailto:connect@chargespot.ca) if you require access.

---
##Integration Steps
1. Add the ChargeSpot SDK module _(com.chargespot.sdk-release.#.#.#.aar)_ to the Android Project.
 1. In Android Studio - File -> New -> New Module -> Import .JAR/.AAR Package
 2. Add the following to the app's build.gradle file with the appropriate package name along with the required support libraries
  ```java
	dependencies {
	...
	compile project(':com.chargespot.sdk_<VERSION.NUMBER>')
    compile 'com.android.support:multidex:1.0.0'
    compile 'com.squareup.retrofit2:retrofit:2.0.0-beta4'
    compile 'com.squareup.retrofit2:converter-gson:2.0.0-beta4'
	...
	}
  ```

2. Include the ChargeSpot SDK application token in the Android Project (e.g., stored in the string.xml file).

 ```
 <string name="chargespot_token">MyAppToken</string>
 ```

3. Retrieve the shared instance of the _ICSChargeSpotManagerInstance_. It's a good idea to keep a reference of it for future use.

 ```
 this.mChargeSpotManager = CSChargeSpotManager.getInstance();
 ```
4. Create _CSChargeSpotConfig_ that will be passed into _CSChargeSpotManager_ for setup. The basic implementation of the CSChargeSpotConfig object keeps track of a development app token, a production app token, and the production state. If you require an app token, please contact ChargeSpot. Please see the javadocs for more information.
```
CSChargeSpotConfig csChargeSpotConfig = new CSChargeSpotConfig(mDevelopmentAppToken, mProductionAppToken, false);
```

5. Initialize _CSChargeSpotManager_ with the an application context and a _CSChargeSpotConfig_ instantiated with the correct environment keys. If no exceptions are thrown, then the SDK is ready. See the documentation for more information on different exceptions that can be thrown when initializing the SDK.

 ```
 this.mChargeSpotManager.setupChargeSpotSDK(this.getActivity(), csChargeSpotConfig);
 ```

6. In order to use ChargeSpot API calls, the consumer will need to use _getChargeSpotApiManager_ on the _IChargeSpotManagerInstance_. The API Manager provides an interface for retrieving generic information about ChargeSpot.
 1. Please note that Bluetooth will be required to be turned on. Failure to do so may cause the SDK to throw a _No BlueTooth LE Scanner_ Exception.
 2. As of Marshmallow 6.0 (SDK level 23), precise location permission (android.permission.ACCESS_FINE_LOCATION) is required. This permission will be requested at install time from the Google Play Store but may need to be manually requested during development. For a list of the required permissions, please see Permissions below.

7. Register for ChargeSpot or Group events for _CSChargeSpotManager_  using _registerForChargeSpotEvents_ and _registerForGroupEvents_ that provides a call back for each respective event. Pass in optional _CSChargeSpotManagerOptions_ object if you wish to configure event behaviours.

 ```
CSChargeSpotManagerOptions chargeSpotEventOptions = new CSChargeSpotManagerOptions();
chargeSpotEventOptions.maximumRangingDistance = CSDistance.IMMEDIATE;

 this.mChargeSpotManager.registerForChargeSpotEvents(this, chargeSpotEventOptions);
 this.mChargeSpotManager.registerForGroupEvents(this, null);
 ```
 You will also need to implement the required interface callback methods
 ```
     /**
     * Success callback for chargespot events
     * @param csChargeSpotEvent
     * @param chargeSpotHookListModel
     */
    @Override
    public void onChargeSpotEventSuccess(CSChargeSpotEvent csChargeSpotEvent, ChargeSpotHookListModel chargeSpotHookListModel) {

    }
    
    /**
     * Failure callback for chargespot events
     * @param e
     */
    @Override
    public void onChargeSpotEventFailure(CSException e) {
    }

    /**
     * Success callback for group events
     * @param csGroupEvent
     * @param chargeSpotGroupHookListModel
     */
    @Override
    public void onGroupEventSuccess(CSGroupEvent csGroupEvent, ChargeSpotGroupHookListModel chargeSpotGroupHookListModel) {

    }

    /**
     * Failure callback for group events
     * @param e
     */
    @Override
    public void onGroupEventFailure(CSException e) {

    }
 ```

8. Register for ChargeSpot error events using _registerForChargeSpotErrors_ to be notified of SDK errors and respond to them. 
 ```
 this.mChargeSpotManager.registerForChargeSpotErrors(this);
 ```
Implement the interface callback method
```
    @Override
    public void didReceivedError(int errorCode, String errorDescription) {
        String errorString = errorCode + " - " + errorDescription;

        // Show toast with error message
        Toast toast = Toast.makeText(this, errorString, Toast.LENGTH_LONG);
        toast.show();
    }
```


###External References
The following external libraries need to be added (select proper version based on your application)

```
com.android.support:appcompat-v7:23.1.1
com.squareup.retrofit:retrofit2:2.0.0-beta4
com.squareup.retrofit2:converter-gson:2.0.0-beta4
```

###Permissions
The following permissions need to be added to the _AndroidManifest.xml_

```
<uses-feature android:name="android.hardware.bluetooth_le" android:required="false"/>
<uses-permission android:name="android.permission.ACCESS_COARSE_LOCATION"/>
<uses-permission android:name="android.permission.ACCESS_FINE_LOCATION"/>
<uses-permission android:name="android.permission.BLUETOOTH"/>
<uses-permission android:name="android.permission.BLUETOOTH_ADMIN"/>
<uses-permission android:name="android.permission.BATTERY_STATS"/>
<uses-permission android:name="android.permission.INTERNET" />
```
---
## Sample App
ChargeSpot events can be used to trigger different actions on the application side. The following sample actions presented below are:

1. System Event Notifications
2. Web Browser URL Redirect

###System Event Notifications
The notification text can be retrieved from _Hooks_ in the _ChargeSpotHookListModel_ returned from the event. 

```
notificationText = hookListModel.hook[0].notification_text;
this.onShowNotification(notificationText);

private void onShowNotification(String notificationTitle, String notification, String url) {
    android.support.v4.app.NotificationCompat.Builder mBuilder =
                new NotificationCompat.Builder(this)
                        .setSmallIcon(R.drawable.ic_flash)
                        .setContentTitle(notificationTitle)
                        .setAutoCancel(true)
                        .setContentText(notification);

    Intent resultIntent = new Intent(this, MainActivity.class);
    resultIntent.setAction(Intent.ACTION_MAIN);
    resultIntent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_SINGLE_TOP);

    if (url != null) {
        resultIntent.putExtra("CHARGESPOT_URL", url);
    }

    PendingIntent resultPendingIntent = PendingIntent.getActivity(this, 0, resultIntent, PendingIntent.FLAG_UPDATE_CURRENT);
    mBuilder.setContentIntent(resultPendingIntent);

    this.mNotificationManager.notify(NOTIFICATION_ID, mBuilder.build());
}
```

###Web Browser URL Redirect
The URL link can be retrieved from _Hooks_ in the _ChargeSpotHookListModel_ returned from the event.

```
savedUrl = hookListModel.hook[0].action_url;
this.openBrowser(this.SavedUrl);

private void openBrowser(String url) {
    Intent i = new Intent(Intent.ACTION_VIEW);
    i.setData(Uri.parse(url));
    startActivity(i);
}
```

###Known Issues

####Chrome Redirect
There is a known issue with web browser URL redirects using Chrome. When an intent to open a web-browser is created soon after the device is unlocked (i.e., system notification is activated on the lock screen), the intent is not handled by Chrome properly. As a workaround adding a delay of at least 1.0s* before firing the intent resolves this race condition on impacted devices. Please note this only affects certain Android devices and only with the Chrome browser set as a default.

*It is possible to experiment with reducing this time to improve responsiveness.
```java

@Override
public void onChargeSpotEventSuccess(CSChargeSpotEvent event, ChargeSpotHookListModel hookListModel) {
    ...
    String notificationText = hookListModel.hook[0].notification_text;
    String hookUrl = hookListModel.hook[0].action_url;

    if (notificationText != null && notificationText.length() > 0) {
        this.onShowNotification(notificationTitle, notificationText, hookUrl);
    }
    ...
}

private void onShowNotification(String notification, String url) {
    // Set up notification
    ...

    Intent resultIntent = new Intent(this, MainActivity.class);
    resultIntent.setAction(Intent.ACTION_MAIN);
    resultIntent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_SINGLE_TOP);

    if (url != null) {
        resultIntent.putExtra("CHARGESPOT_URL", url);
    }
    ...
}

@Override
protected void onNewIntent(Intent intent) {
    super.onNewIntent(intent);

    Bundle extras = intent.getExtras();
    if (extras != null && extras.containsKey("CHARGESPOT_URL")) {
        if (extras != null && extras.containsKey("CHARGESPOT_URL")) {
            this.savedUrl = extras.getString("CHARGESPOT_URL");

            // Set a timer to delay triggering the intent for Chrome browsers
            Timer intentTimer = new Timer();
            TimerTask intentTimerTask = new TimerTask() {
                @Override
                public void run() {
                    MainActivity.this.openBrowser(MainActivity.this.savedUrl);
                    MainActivity.this.savedUrl = null;
                }
            };
            intentTimer.schedule(intentTimerTask, 1000);
        }
    }
}

```

####User disabling bluetooth in app
If the user turns off bluetooth while the app is in used, it's possible that the app will throw an exception. In order to remedy this, a broadcast listener can be used to detect if a change was made to the bluetooth. Once it catches the event, the app can then request the user to turn on the bluetooth or unregister the ChargeSpotManager so bluetooth updates will cease.

```java
private final BroadcastReceiver mReceiver = new BroadcastReceiver() {
    public void onReceive(Context context, Intent intent) {
        String action = intent.getAction();

        if (BluetoothAdapter.ACTION_STATE_CHANGED.equals(action)) {
            if (intent.getIntExtra(BluetoothAdapter.EXTRA_STATE, -1)
                    == BluetoothAdapter.STATE_OFF) {
                //request permission or unregister chargespotmanager;
             }
        }
    }
};
```

##Documentation
Please refer to accompanying javadocs for additional documentation.

-----------------------------------------
##Troubleshooting

####401 - Unauthorized Error 
ChargeSpot's API backend is secured by a token authentication system. Please use the provided token when initializing ChargeSpotManager. If you are still experiencing problems with authentication, please contact us at [connect@chargespot.ca](mailto:connect@chargespot.ca).

####Device does not trigger events when placed onto ChargeSpot
ChargeSpot requires the following services to operate:
- Location Services
- Bluetooth (4.0+)
- Internet connection

####Bluetooth
Bluetooth is critical to the operation of the SDK and must be turned on for the app. Please refer to the above code to implement a receiver to handle bluetooth events.

To check if Bluetooth is available on the device:
```
private boolean hasBluetooth() {
    return BluetoothAdapter.getDefaultAdapter() != null;
}
```

To check if Bluetooth is enabled:
```
private boolean bluetoothEnabled() {
    return BluetoothAdapter.getDefaultAdapter().isEnabled();
}
```

Both of these things should be done before registering for events with CSChargeSpotManager

####Location Services

Location Services must also be turned on but will not cause an exception if not enabled from the beginning. The device will not be able to communicate with the ChargeSpot until location services is turned on.

To check if location services is enabled:
```
private boolean isLocationEnabled() {
    int locationMode;
    String locationProviders;

    if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.KITKAT){
        try {
            locationMode = Settings.Secure.getInt(this.getContentResolver(), 
                                                  Settings.Secure.LOCATION_MODE);
        } catch (Settings.SettingNotFoundException e) {
            return false;
        }
        return locationMode != Settings.Secure.LOCATION_MODE_OFF;
    } else{
        locationProviders = Settings.Secure.getString(this.getContentResolver(), 
                                                      Settings.Secure.LOCATION_PROVIDERS_ALLOWED);
        return !TextUtils.isEmpty(locationProviders);
    }
}
```

If location services is not enabled, the app should prompt the user to enable location services in order to use the ChargeSpot.

####Troubleshooting ChargeSpot iBeacon
ChargeSpot uses iBeacon technology to communicate with compatible devices and sends identification information that will trigger a response from the ChargeSpot servers to the device. To see this information, please download an iBeacon identification app such as:

nRF Master Control Panel (BLE)
https://play.google.com/store/apps/details?id=no.nordicsemi.android.mcp&hl=en

When scanning for iBeacons, you should be able to detect the ChargeSpot iBeacons along with their UUID, major value, and minor value.

The UUID of ChargeSpot iBeacons should be `64f941fa-0871-11e5-a6c0-1697f925ec7b`

If you have any questions regarding the values or are unable to find an iBeacon, please contact us at [connect@chargespot.ca](mailto:connect@chargespot.ca).

-----------------------------------------
