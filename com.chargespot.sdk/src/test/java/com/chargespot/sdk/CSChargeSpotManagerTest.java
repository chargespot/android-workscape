/*
    Copyright (C) 2015 ChargeSpot
 */
package com.chargespot.sdk;

import android.test.suitebuilder.annotation.SmallTest;

import com.chargespot.sdk.manager.chargespot.ICSChargeSpotManagerInstance;

import junit.framework.Assert;
import junit.framework.TestCase;

/**
 * CSChargeSpotManagerTest
 *
 * Date: 10/12/2015
 * @version 0.3
 */
public class CSChargeSpotManagerTest
    extends TestCase
{
    @SmallTest
    public void testGetInstance()
        throws Exception
    {
        ICSChargeSpotManagerInstance instance1 = CSChargeSpotManager.getInstance();
        ICSChargeSpotManagerInstance instance2 = CSChargeSpotManager.getInstance();

        // Make sure an instance is always returned
        Assert.assertNotNull(instance1);
        Assert.assertNotNull(instance2);

        // Make sure it's always the same instance returned
        Assert.assertEquals(instance1, instance2);
    }
}
