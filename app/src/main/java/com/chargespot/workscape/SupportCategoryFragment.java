package com.chargespot.workscape;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;


/**
 * Created by kelvinlau on 2016-07-27.
 */

public class SupportCategoryFragment extends BaseFragment
    implements View.OnClickListener {

    public interface SupportCategorySelectListener {
        void onSupportCategorySelect(int supportCategory);
    }

    public final static String TAG = SupportCategoryFragment.class.getCanonicalName();

    private LinearLayout mTooColdLinearLayout;
    private LinearLayout mTooHotLinearLayout;
    private LinearLayout mMissingSuppliesLinearLayout;
    private LinearLayout mRequiresCleaningLinearLayout;
    private LinearLayout mEquipmentDamageLinearLayout;
    private LinearLayout mOtherLinearLayout;

    public SupportCategorySelectListener mListener;

    public static final int SUPPORT_CATEGORY_TOO_COLD = 1;
    public static final int SUPPORT_CATEGORY_TOO_HOT = 2;
    public static final int SUPPORT_CATEGORY_MISSING_SUPPLIES = 3;
    public static final int SUPPORT_CATEGORY_REQUIRES_CLEANING = 4;
    public static final int SUPPORT_CATEGORY_EQUIPMENT_DAMAGE = 5;
    public static final int SUPPORT_CATEGORY_OTHER = 6;

    public SupportCategoryFragment() {
    }

    @Override
    public void onResume() {
        super.onResume();

        Toolbar toolbar = (Toolbar) getActivity().findViewById(R.id.toolbar);
        toolbar.setTitle(getTitle());
    }


    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_support_category, container, false);

        mTooColdLinearLayout = (LinearLayout)view.findViewById(R.id.tooColdLinearLayout);
        mTooHotLinearLayout = (LinearLayout)view.findViewById(R.id.tooHotLinearLayout);
        mMissingSuppliesLinearLayout = (LinearLayout)view.findViewById(R.id.missingSuppliesLinearLayout);
        mRequiresCleaningLinearLayout = (LinearLayout)view.findViewById(R.id.requiresCleaningLinearLayout);
        mEquipmentDamageLinearLayout = (LinearLayout)view.findViewById(R.id.equipmentDamageLinearLayout);
        mOtherLinearLayout = (LinearLayout)view.findViewById(R.id.otherLinearLayout);

        mTooColdLinearLayout.setOnClickListener(this);
        mTooHotLinearLayout.setOnClickListener(this);
        mMissingSuppliesLinearLayout.setOnClickListener(this);
        mRequiresCleaningLinearLayout.setOnClickListener(this);
        mEquipmentDamageLinearLayout.setOnClickListener(this);
        mOtherLinearLayout.setOnClickListener(this);

        return view;
    }

    @Override
    public void onClick(View v) {
        if (mListener == null) {
            Log.d(TAG, "Listener for Support Category Fragment not set!");
            return;
        }

        int category = 0;
        switch(v.getId()) {
            case R.id.tooColdLinearLayout:
                category = SUPPORT_CATEGORY_TOO_COLD;
                break;
            case R.id.tooHotLinearLayout:
                category = SUPPORT_CATEGORY_TOO_HOT;
                break;
            case R.id.missingSuppliesLinearLayout:
                category = SUPPORT_CATEGORY_MISSING_SUPPLIES;
                break;
            case R.id.requiresCleaningLinearLayout:
                category = SUPPORT_CATEGORY_REQUIRES_CLEANING;
                break;
            case R.id.equipmentDamageLinearLayout:
                category = SUPPORT_CATEGORY_EQUIPMENT_DAMAGE;
                break;
            case R.id.otherLinearLayout:
                category = SUPPORT_CATEGORY_OTHER;
                break;
        }

        if (category > 0) {
            mListener.onSupportCategorySelect(category);
        }
    }

    public void setSupportCategorySelectListener(SupportCategorySelectListener listener) {
        mListener = listener;
    }

    public String getTitle() {
        return getString(R.string.support_title);
    }
}
