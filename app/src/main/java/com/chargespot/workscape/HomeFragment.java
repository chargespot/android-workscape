package com.chargespot.workscape;

import android.os.Bundle;
import android.os.Handler;
import android.support.annotation.Nullable;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.chargespot.workscape.Helpers.AlertDialogHelper;
import com.chargespot.workscape.Helpers.DateHelper;
import com.chargespot.workscape.Helpers.MixpanelHelper;
import com.chargespot.workscape.Helpers.NotificationHelper;
import com.chargespot.workscape.Requests.CSRequestManager;
import com.chargespot.workscape.Models.CSCalendarEvent;
import com.chargespot.workscape.Models.CSRoom;
import com.google.android.gms.auth.api.signin.GoogleSignInAccount;
import com.squareup.picasso.Picasso;

import java.util.Date;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;


/**
 * Created by kelvinlau on 2016-07-20.
 */

public class HomeFragment extends BaseFragment
        implements SwipeRefreshLayout.OnRefreshListener,
                    RoomListViewAdapter.BookNowListener {
    public static String TAG = HomeFragment.class.getCanonicalName();

    private TextView mNameTextView;
    private TextView mSubtitleTextView;
    private ImageView mHeaderImageView;
    private ListView mRoomListView;
    private RoomListViewAdapter mRoomListViewAdapter;
    private GoogleSignInAccount mGoogleAccount;
    private RoomListSelectListener mRoomSelectedCallback;
    private SwipeRefreshLayout mSwipeRefresh;

    public interface RoomListSelectListener {
        void onRoomSelected(int position);
    }


    @Override
    public void onResume() {
        super.onResume();

        Toolbar toolbar = (Toolbar) getActivity().findViewById(R.id.toolbar);
        toolbar.setTitle(getTitle());
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_home, container, false);
        List<CSRoom> roomList = CSRouter.getInstance().getRoomList();
        mGoogleAccount = CSRouter.getInstance().getGoogleAccount();

        mNameTextView = (TextView)view.findViewById(R.id.nameTextView);
        mNameTextView.setText(mGoogleAccount.getDisplayName().toUpperCase());

        mSubtitleTextView = (TextView)view.findViewById(R.id.subtitleTextView);
        mSubtitleTextView.setText(CSRouter.getInstance().getSession().getClient().getName());

        mHeaderImageView = (ImageView)view.findViewById(R.id.headerImageView);
        String imageUrl = CSRouter.getInstance().getSession().getClient().getImageUrl();
        Picasso.with(getActivity()).load(imageUrl).into(mHeaderImageView);

        mRoomListView = (ListView) view.findViewById(R.id.roomListView);
        mRoomListViewAdapter = new RoomListViewAdapter(getContext(), R.layout.listview_room, roomList);
        mRoomListViewAdapter.setBookNowListener(this);
        mRoomListView.setAdapter(mRoomListViewAdapter);

        mSwipeRefresh = (SwipeRefreshLayout)view.findViewById(R.id.swipe_refresh_layout);
        mSwipeRefresh.setColorSchemeResources(R.color.colorCSTeal);
        mSwipeRefresh.setOnRefreshListener(this);

        mRoomListView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                mRoomSelectedCallback.onRoomSelected(position);
            }
        });

        if (roomList.isEmpty()) {
            mSwipeRefresh.post(new Runnable() {
                @Override
                public void run() {
                    mSwipeRefresh.setRefreshing(true);
                    refreshRoomList();
                }
            });
        }

        return view;
    }

    @Override
    public void onStart() {
        super.onStart();
        MixpanelHelper.track(getActivity(), "HomeFragment - Room List");
    }

    public void setRoomSelectedListener(RoomListSelectListener listener) {
        mRoomSelectedCallback = listener;
    }

    private void refreshRoomList() {
        if (mRoomListView == null) {
            return;
        }

        mRoomListView.setVisibility(View.GONE);
        mSwipeRefresh.setRefreshing(true);

        try {
            CSRequestManager.getInstance(getActivity()).getRooms(new Callback<List<CSRoom>>() {
                @Override
                public void onResponse(Call<List<CSRoom>> call, Response<List<CSRoom>> response) {
                    Log.d(TAG, "Room refresh successful - " + response.code() + " - " + response.message());
                    if (response.isSuccessful()) {
                        CSRouter.getInstance().setRoomList(response.body());
                        mRoomListView.setVisibility(View.VISIBLE);
                        mRoomListViewAdapter.updateRoomModel(response.body());
                        mSwipeRefresh.setRefreshing(false);
                    } else {
                        mRoomListView.setVisibility(View.VISIBLE);
                        mSwipeRefresh.setRefreshing(false);
                    }
                }

                @Override
                public void onFailure(Call<List<CSRoom>> call, Throwable t) {
                    Log.d(TAG, "Room refresh failed - " + t.getLocalizedMessage());
                    mRoomListView.setVisibility(View.VISIBLE);
                    mSwipeRefresh.setRefreshing(false);
                }
            });
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    public void onRefresh() {
        refreshRoomList();
    }

    public String getTitle() {
        return getString(R.string.home_title);
    }

    @Override
    public void onBookNowClick(View sender, CSRoom room) {
        int roomState = room.getState();

        if (roomState == CSRoom.ROOM_STATE_UNAVAILABLE) {
            return;
        } else if (roomState == CSRoom.ROOM_STATE_OCCUPIED) {
            if (room.getNextEvent().canEndNow()) {
                endNow(room);
            }
            return;
        }

        bookNow(room);
    }

    private void bookNow(final CSRoom room) {
        final List<Date> timeSlots = room.getAvailableTimeSlots();
        if (timeSlots.size() == 0) {
            Toast.makeText(getActivity(), getString(R.string.book_no_slots), Toast.LENGTH_SHORT).show();
            return;
        }

        final Date currentDate = new Date();
        CharSequence[] timeSlotTexts = DateHelper.dateStringsForList(timeSlots, "h:mm a");

        AlertDialog dialog = AlertDialogHelper.createBookNowDialog(getActivity(), getString(R.string.book_until), timeSlotTexts, new AlertDialogHelper.AlertDialogListener() {
            @Override
            public void OnAlertDialogSelect(int position) {
                Date bookingDate = timeSlots.get(position);
                CSCalendarEvent.createEvent(getActivity(), room, currentDate, bookingDate, new Callback<CSCalendarEvent>() {
                    @Override
                    public void onResponse(Call<CSCalendarEvent> call, Response<CSCalendarEvent> response) {
                        if (response.isSuccessful()) {
                            String message = getString(R.string.welcome_text, room.getName());
                            CSCalendarEvent event = response.body();
                            NotificationHelper.createEndNowNotification(getActivity(), getString(getActivity().getApplicationInfo().labelRes), message, room, event);
                            MixpanelHelper.track(getActivity(), "HomeFragment - Book Now Successful");
                        } else {
                            MixpanelHelper.track(getActivity(), "HomeFragment - Book Now Failed");
                        }

                        final Handler handler = new Handler();
                        handler.postDelayed(new Runnable() {
                            @Override
                            public void run() {
                                refreshRoomList();
                            }
                        }, 1000);                    }

                    @Override
                    public void onFailure(Call<CSCalendarEvent> call, Throwable t) {
                        Toast.makeText(getActivity(), getString(R.string.book_failure), Toast.LENGTH_SHORT).show();
                        refreshRoomList();
                        MixpanelHelper.track(getActivity(), "HomeFragment - Book Now Failed");
                    }
                });
            }

            @Override
            public void OnAlertDialogCancel() {
                mRoomListViewAdapter.notifyDataSetChanged();
            }
        });
        dialog.setCanceledOnTouchOutside(false);
    }

    private void endNow(final CSRoom room) {
        AlertDialog dialog = AlertDialogHelper.createEndNowDialog(getActivity(), room, new AlertDialogHelper.AlertDialogListener() {
            @Override
            public void OnAlertDialogSelect(int position) {

            }

            @Override
            public void OnAlertDialogCancel() {
                mRoomListViewAdapter.notifyDataSetChanged();
            }

        }, new Callback<CSCalendarEvent>() {
            @Override
            public void onResponse(Call<CSCalendarEvent> call, Response<CSCalendarEvent> response) {
                Log.d(TAG, "End now request success" + response.code() + " - " + response.message());
                if (response.isSuccessful()) {
                    Toast.makeText(getActivity(), getString(R.string.end_now_success), Toast.LENGTH_SHORT).show();
                    MixpanelHelper.track(getActivity(), "HomeFragment - End Now Successful");
                } else {
                    Toast.makeText(getActivity(), getString(R.string.end_now_failure), Toast.LENGTH_SHORT).show();
                    MixpanelHelper.track(getActivity(), "HomeFragment - End Now Failed");
                }

                final Handler handler = new Handler();
                handler.postDelayed(new Runnable() {
                    @Override
                    public void run() {
                        refreshRoomList();
                    }
                }, 1000);
            }

            @Override
            public void onFailure(Call<CSCalendarEvent> call, Throwable t) {
                Log.d(TAG, "End now request success" + t.getLocalizedMessage());
                Toast.makeText(getActivity(), getString(R.string.end_now_failure), Toast.LENGTH_SHORT).show();
                MixpanelHelper.track(getActivity(), "HomeFragment - End Now Failed");
            }
        });
        dialog.setCanceledOnTouchOutside(false);
    }

}