package com.chargespot.workscape;

import android.content.Context;
import android.content.DialogInterface;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AlertDialog;
import android.text.InputType;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import android.support.v7.widget.Toolbar;

import com.chargespot.workscape.Helpers.CircleTransform;
import com.chargespot.workscape.Helpers.MixpanelHelper;
import com.chargespot.workscape.Helpers.SharedPreferencesHelper;
import com.chargespot.workscape.Models.CSFloor;
import com.chargespot.workscape.Models.CSUser;
import com.squareup.picasso.Picasso;

/**
 * Created by kelvinlau on 2016-08-22.
 */

public class SettingsFragment extends BaseFragment implements
    View.OnClickListener {


    private ImageView mProfileImageView;
    private TextView mNameTextView;
    private TextView mSubtitleTextView;
    private TextView mFloorTextView;
    private Button mEditButton;
    private TextView mVersionTextView;

    private int mCurrentFloor;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_settings, container, false);

        mProfileImageView = (ImageView)view.findViewById(R.id.profileImageView);
        mNameTextView = (TextView)view.findViewById(R.id.nameTextView);
        mSubtitleTextView = (TextView)view.findViewById(R.id.subtitleTextView);
        mFloorTextView = (TextView)view.findViewById(R.id.floorTextView);
        mEditButton = (Button)view.findViewById(R.id.editButton);
        mVersionTextView = (TextView)view.findViewById(R.id.versionTextView);

        updateUI();

        return view;
    }

    private void updateUI() {
        CSUser currentUser = CSRouter.getInstance().getCurrentUser();

        Picasso.with(getActivity()).load(currentUser.getImageUrl()).transform(new CircleTransform()).fit().into(mProfileImageView);
        mNameTextView.setText(currentUser.getDisplayName());
        mSubtitleTextView.setText(CSRouter.getInstance().getSession().getClient().getName());
        mEditButton.setOnClickListener(this);

        mCurrentFloor = SharedPreferencesHelper.getCurretUserFloor(getActivity());
        if (mCurrentFloor == 0) {
            mFloorTextView.setText(getString(R.string.settings_no_floor_set));
        } else {
            mFloorTextView.setText(CSFloor.getOrdinalFloorName(mCurrentFloor));
        }

        PackageManager manager = getActivity().getPackageManager();
        try {
            PackageInfo info = manager.getPackageInfo(getActivity().getPackageName(), 0);
            String version = info.versionName;
            String build = info.versionCode + "";
            mVersionTextView.setText("Ver. " + version + " Build " + build);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    public void onStart() {
        super.onStart();
        MixpanelHelper.track(getActivity(), "SettingsFragment - Settings");
    }

    @Override
    public void onResume() {
        super.onResume();

        Toolbar toolbar = (Toolbar) getActivity().findViewById(R.id.toolbar);
        toolbar.setTitle(getTitle());
    }

    public String getTitle() {
        return getString(R.string.settings_title);
    }

    @Override
    public void onClick(View v) {
        switch(v.getId()) {
            case R.id.editButton:
                createFloorEditDialog(mCurrentFloor);
                break;
        }
    }

    private void createFloorEditDialog(final int floor) {
        AlertDialog.Builder alert = new AlertDialog.Builder(getActivity());
        alert.setTitle("Set your current floor number");
        alert.setMessage("");
        // Create TextView
        final EditText input = new EditText(getActivity());
        input.setInputType(InputType.TYPE_CLASS_NUMBER);
        input.setSelectAllOnFocus(true);

        if (floor > 0) {
            input.setText(floor + "");
        }

        alert.setView(input);

        final InputMethodManager imm = (InputMethodManager) getActivity().getSystemService(Context.INPUT_METHOD_SERVICE);

        alert.setPositiveButton("Save", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int whichButton) {
                SharedPreferencesHelper.saveCurrentUserFloor(getActivity(), Integer.valueOf(input.getText().toString()));
                getActivity().getWindow().setSoftInputMode(
                        WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_HIDDEN
                );
                updateUI();
            }
        });

        alert.setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int whichButton) {
                getActivity().getWindow().setSoftInputMode(
                        WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_HIDDEN
                );
            }
        });
        alert.show();
        input.requestFocus();
        imm.toggleSoftInput(InputMethodManager.SHOW_FORCED, InputMethodManager.HIDE_IMPLICIT_ONLY);
    }
}
