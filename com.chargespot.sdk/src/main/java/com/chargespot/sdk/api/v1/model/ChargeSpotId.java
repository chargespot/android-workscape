/*
    Copyright (C) 2015 ChargeSpot
 */
package com.chargespot.sdk.api.v1.model;

/**
 * ChargeSpot device model
 *
 * @version 0.5
 */
public final class ChargeSpotId {
    /**
     * ID of the ChargeSpot device
     */
    public String id;
}
