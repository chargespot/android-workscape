package com.chargespot.sdk.handler;

/**
 * Created by kelvinlau on 16-05-31.
 */
public interface ICSChargeSpotErrorHandler {
    void didReceivedError(int errorCode, String errorDescription);
}
