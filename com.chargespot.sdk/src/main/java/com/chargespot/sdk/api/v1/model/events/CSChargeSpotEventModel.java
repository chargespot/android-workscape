/*
    Copyright (C) 2015 ChargeSpot
 */
package com.chargespot.sdk.api.v1.model.events;

import com.chargespot.sdk.events.CSChargeSpotEvent;

/**
 * ChargeSpot Event descriptor for handling
 * internal ChargeSpot events. This is an internal
 * event model.
 *
 * @version 0.5
 */
public final class CSChargeSpotEventModel
        extends CSEventModel {
    //region Internal
    public CSChargeSpotEvent Event;
    //endregion

    //region Constructors

    /**
     * Create default ChargeSpot Event Model
     */
    public CSChargeSpotEventModel() {
    }

    /**
     * Create ChargeSpot Event Model
     *
     * @param uuid  UUID of the device where the event occurred
     * @param major Major version of the ChargeSpot
     * @param minor Minor version of the ChargeSpot
     * @param event Event occurred on the device
     */
    public CSChargeSpotEventModel(final String uuid, final int major, final int minor, final CSChargeSpotEvent event) {
        this.UUID = uuid;
        this.Major = major;
        this.Minor = minor;
        this.Event = event;
    }

    /**
     * Create ChargeSpot Event Model
     *
     * @param address Address of the device on BLE
     * @param uuid    UUID of the device where the event occurred
     * @param major   Major version of the ChargeSpot
     * @param minor   Minor version of the ChargeSpot
     * @param event   Event occurred on the device
     */
    public CSChargeSpotEventModel(final String address, final String uuid, final int major, final int minor, final CSChargeSpotEvent event) {
        this.Address = address;
        this.UUID = uuid;
        this.Major = major;
        this.Minor = minor;
        this.Event = event;
    }
    //endregion

    /**
     * Check if the Event Model is valid
     */
    public boolean isValid() {
        return !(!super.isValid() || null == Event);
    }
}
