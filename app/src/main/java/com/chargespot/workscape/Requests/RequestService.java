package com.chargespot.workscape.Requests;

import com.chargespot.workscape.Models.CSCalendarEvent;
import com.chargespot.workscape.Models.CSRoom;

import java.util.List;

import retrofit2.Call;
import retrofit2.http.Field;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.GET;
import retrofit2.http.Header;
import retrofit2.http.POST;
import retrofit2.http.PUT;
import retrofit2.http.Path;

/**
 * Created by kelvinlau on 2016-07-20.
 */

public interface RequestService {

    @FormUrlEncoded
    @POST("auth")
    Call<CSSession> auth (
            @Header("X-Device-Identifier") String deviceUid,
            @Header("X-User-Agent") String userAgent,
            @Header("X-SDK-Version") String sdkVersion,
            @Header("X-User-Language") String contentLanguage,
            @Header("X-Request-Origin") String requestOrigin,
            @Field("id_token") String idToken
    );

    @FormUrlEncoded
    @POST("rooms/{id}/events")
    Call<CSCalendarEvent> createEvent (
            @Header("X-CSAuth-Token") String token,
            @Header("X-Device-Identifier") String deviceUid,
            @Header("X-User-Agent") String userAgent,
            @Header("X-SDK-Version") String sdkVersion,
            @Header("X-User-Language") String contentLanguage,
            @Header("X-Request-Origin") String requestOrigin,
            @Path("id") String id,
            @Field("summary") String summary,
            @Field("start") String startDate,
            @Field("end") String endDate
    );

    @PUT("rooms/{room_id}/events/{event_id}/endnow")
    Call<CSCalendarEvent> endNow(
            @Header("X-CSAuth-Token") String token,
            @Header("X-Device-Identifier") String deviceUid,
            @Header("X-User-Agent") String userAgent,
            @Header("X-SDK-Version") String sdkVersion,
            @Header("X-User-Language") String contentLanguage,
            @Header("X-Request-Origin") String requestOrigin,
            @Path("room_id") String roomId,
            @Path("event_id") String eventId
    );

    @GET("rooms/{id}")
    Call<CSRoom> getRoom (
            @Header("X-CSAuth-Token") String token,
            @Header("X-Device-Identifier") String deviceUid,
            @Header("X-User-Agent") String userAgent,
            @Header("X-SDK-Version") String sdkVersion,
            @Header("X-User-Language") String contentLanguage,
            @Header("X-Request-Origin") String requestOrigin,
            @Path("id") String id
    );

    @GET("rooms")
    Call<List<CSRoom>> getRooms (
            @Header("X-CSAuth-Token") String token,
            @Header("X-Device-Identifier") String deviceUid,
            @Header("X-User-Agent") String userAgent,
            @Header("X-SDK-Version") String sdkVersion,
            @Header("X-User-Language") String contentLanguage,
            @Header("X-Request-Origin") String requestOrigin
    );
}
