package com.chargespot.workscape.Helpers;

import android.Manifest;
import android.annotation.TargetApi;
import android.app.Activity;
import android.bluetooth.BluetoothAdapter;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.os.Build;
import android.os.Bundle;
import android.provider.Settings;
import android.text.TextUtils;
import android.util.Log;

/**
 * Created by kelvinlau on 2016-08-18.
 */

public class PermissionsHelper {
    public static final String TAG = PermissionsHelper.class.getCanonicalName();
    public static final int BLUETOOTH_REQUEST_CODE = 1;
    public static final int FINE_LOCATION_REQUEST_CODE = 2;

    private boolean mLocationEnabled;
    private BluetoothAdapter mBluetoothAdapter;
    private Intent enableBtIntent;
    private LocationManager mLocationManager;
    private Activity mActivity;

    {
        mLocationEnabled = false;
        mBluetoothAdapter = null;
        enableBtIntent = null;
        mLocationManager = null;
    }

    public PermissionsHelper(Activity activity) {
        mActivity = activity;
        mLocationManager = (LocationManager) activity.getSystemService(Context.LOCATION_SERVICE);
        mBluetoothAdapter = BluetoothAdapter.getDefaultAdapter();
    }

    private final class MyLocationListener implements LocationListener {

        @Override
        public void onLocationChanged(Location locFromGps) {}

        @Override
        public void onProviderDisabled(String provider) {
            PermissionsHelper.this.mLocationEnabled = false;
        }

        @Override
        public void onProviderEnabled(String provider) {
            PermissionsHelper.this.mLocationEnabled = true;
        }

        @Override
        public void onStatusChanged(String provider, int status, Bundle extras) {}
    }

    // Required permissions
    private static final String[] REQUIRED_PERMS = {
            Manifest.permission.ACCESS_FINE_LOCATION
    };

    /**
     * Requests fine location permission and bluetooth
     */
    public void getPermissions() {
        // Marshmallow and up requires additional permissions
        if (Build.VERSION.SDK_INT >= 23) {
            if (!canAccessLocation()) {
                mActivity.requestPermissions(REQUIRED_PERMS, FINE_LOCATION_REQUEST_CODE);
            }
        }

        if (this.hasBluetooth() && !this.bluetoothEnabled()) {
            this.enableBtIntent = new Intent(BluetoothAdapter.ACTION_REQUEST_ENABLE);
            mActivity.startActivityForResult(enableBtIntent, BLUETOOTH_REQUEST_CODE);
        }
    }

    /**
     * Check for fine location permission and bluetooth status
     * @return boolean
     */
    public boolean hasSufficientPermissions() {
        // Marshmallow and up requires additional permissions
        if (Build.VERSION.SDK_INT >= 23 && !canAccessLocation()) {
            return false;
        }

        return !(!this.hasBluetooth() || !this.bluetoothEnabled());

    }

    /**
     * Check for bluetooth on device
     * @return boolean
     */
    private boolean hasBluetooth() {
        return this.mBluetoothAdapter != null;
    }

    /**
     * Check for bluetooth status
     * @return boolean
     */
    private boolean bluetoothEnabled() {
        return this.mBluetoothAdapter.isEnabled();
    }

    @TargetApi(23)
    private boolean hasPermission(Context context, String string) {
        return context.checkSelfPermission(string) == PackageManager.PERMISSION_GRANTED;
    }

    /**
     * Checks if the location services is currently turned on
     * @return boolean
     */
    private boolean isLocationEnabled() {
        int locationMode;
        String locationProviders;

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.KITKAT){
            try {
                locationMode = Settings.Secure.getInt(mActivity.getContentResolver(), Settings.Secure.LOCATION_MODE);
            } catch (Settings.SettingNotFoundException e) {
                return false;
            }
            return locationMode != Settings.Secure.LOCATION_MODE_OFF;
        } else{
            locationProviders = Settings.Secure.getString(mActivity.getContentResolver(), Settings.Secure.LOCATION_PROVIDERS_ALLOWED);
            return !TextUtils.isEmpty(locationProviders);
        }
    }

    /**
     * Checks current permission for fine location
     * @return boolean
     */
    private boolean canAccessLocation() {
        if (this.hasPermission(mActivity, Manifest.permission.ACCESS_FINE_LOCATION)) {
            LocationListener locationListener = new MyLocationListener();
            try {
                mLocationManager.requestLocationUpdates(LocationManager.GPS_PROVIDER, 35000, 10, locationListener);
            } catch (SecurityException se) { Log.d(TAG, "No location permission."); }
            return true;
        }
        return false;
    }


}
