/*
    Copyright (C) 2015 ChargeSpot
 */
package com.chargespot.sdk.api.v1.model.events;

import com.chargespot.sdk.events.CSGroupEvent;

/**
 * ChargeSpot Event descriptor. This is an internal
 * event model.
 *
 * @version 0.3
 */
public final class CSGroupEventModel
        extends CSEventModel {
    //region Internal
    public CSGroupEvent Event;
    //endregion

    //region Constructors

    /**
     * Create default Group Event Model
     */
    public CSGroupEventModel() {
    }

    /**
     * Create Group Event Model
     *
     * @param uuid  UUID of the device where the event occurred
     * @param major Major version of the ChargeSpot
     * @param minor Minor version of the ChargeSpot
     * @param event Event occurred on the device
     */
    public CSGroupEventModel(final String uuid, final int major, final int minor, final CSGroupEvent event) {
        this.UUID = uuid;
        this.Major = major;
        this.Minor = minor;
        this.Event = event;
    }

    /**
     * Create Group Event Model
     *
     * @param address Address of the device on the BLE
     * @param uuid    UUID of the device where the event occurred
     * @param major   Major version of the ChargeSpot
     * @param minor   Minor version of the ChargeSpot
     * @param event   Event occurred on the device
     */
    public CSGroupEventModel(final String address, final String uuid, final int major, final int minor, final CSGroupEvent event) {
        this.Address = address;
        this.UUID = uuid;
        this.Major = major;
        this.Minor = minor;
        this.Event = event;
    }
    //endregion

    /**
     * Check if the Event Model is valid
     */
    public boolean isValid() {
        return !(!super.isValid() || null == Event);
    }
}
