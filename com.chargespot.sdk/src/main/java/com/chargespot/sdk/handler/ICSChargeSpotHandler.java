/*
    Copyright (C) 2015 ChargeSpot
 */
package com.chargespot.sdk.handler;

import android.support.annotation.Nullable;

import com.chargespot.sdk.events.ICSChargeSpotEventHandler;
import com.chargespot.sdk.events.ICSGroupEventHandler;
import com.chargespot.sdk.exceptions.CSNotInitializedException;
import com.chargespot.sdk.manager.chargespot.CSChargeSpotManagerOptions;
import com.chargespot.sdk.state.ICSDeviceStateHandler;

/**
 * ChargeSpot handler interface for handling
 * all of the events being received from the devices, and
 * executing proper actions related to them.
 *
 * @version 0.5
 */
public interface ICSChargeSpotHandler {

    //region ChargeSpotEvents

    /**
     * Register for ChargeSpot events to be received
     *
     * @param callback Callback to execute upon receiving a ChargeSpot event
     * @throws CSNotInitializedException
     */
    void registerForChargeSpotEvents(@Nullable ICSChargeSpotEventHandler callback, @Nullable CSChargeSpotManagerOptions options) throws CSNotInitializedException;

    /**
     * Unregister for ChargeSpot events to be received
     *
     * @throws CSNotInitializedException
     */
    void unregisterForChargeSpotEvents() throws CSNotInitializedException;
    //endregion

    //region GroupEvent

    /**
     * Register for Group events to be received
     *
     * @param callback Callback to execute upon receiving a Group event
     * @throws CSNotInitializedException
     */
    void registerForGroupEvents(@Nullable ICSGroupEventHandler callback, @Nullable CSChargeSpotManagerOptions options) throws CSNotInitializedException;

    /**
     * Unregister for Group events to be received.
     *
     * @throws CSNotInitializedException
     */
    void unregisterForGroupEvents() throws CSNotInitializedException;
    //endregion

    /**
     * Register for Errors to be received
     *
     * @param callback Callback to execute upon receiving a error
     */
    void registerForChargeSpotErrors(@Nullable ICSChargeSpotErrorHandler callback);

    /**
     * Unregister for errors to be received
     */
    void unregisterForChargeSpotErrors();

    //region DeviceState

    /**
     * Retrieve the current device state. This will return the last known
     * state of the device.
     *
     * @param callback Callback to be executed upon device state retrieval
     * @throws CSNotInitializedException
     */
    void retrieveDeviceState(@Nullable ICSDeviceStateHandler callback) throws CSNotInitializedException;
    //endregion
}
