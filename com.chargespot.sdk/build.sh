MY_PATH="$( cd "$( dirname "${BASH_SOURCE[0]}" )" && pwd )"
VERSION=0
PACKAGE_NAME="com.chargespot.sdk"
RELEASE="release"
FILENAME=""
DOC_NAME="ChargeSpot-Android-SDK-IntegrationGuide"
DOC_EXT=".md"
BUILD_OUTPUT="build/outputs/aar"
OUTPUT="ChargeSpot_Android_SDK"
TEMP_OUTPUT_DIR="sdk-output"
NO_ZIP=0

function showHelp {
	echo "\nUsage: sh build.sh (RELEASE) (--options)\n"
	echo "Releases: debug | release"
	echo "Options: --no-zip\n"
}

function getFilename {
	FILENAME=$(ls $MY_PATH/$BUILD_OUTPUT | grep "$PACKAGE_NAME")
	echo "Build filename: $FILENAME"
	if [ -z "$FILENAME" ]
	  then
	  	echo "Could not find build file for release $RELEASE"
	  	exit 1
	fi
}

function getVersion {
	VERSION=$(echo $FILENAME | grep "[0-9]\+.[0-9]\+.[0-9]\+" -o)
	echo "Version: $VERSION"
}

function gradleBuild { 
	echo "Gradle building $RELEASE from assemble$RELEASE"
	(cd $MY_PATH; gradle -q clean assemble$RELEASE)
}

function generateJavadocs {
	echo "Generating Javadocs..."
	(cd $MY_PATH/src/main/java/ ; javadoc -d $MY_PATH/docs/ -encoding utf-8 -quiet -public -subpackages com.chargespot.sdk) 2> /dev/null
}

function moveFiles {
	echo "Moving files to $TEMP_OUTPUT_DIR"
	rm -rf $TEMP_OUTPUT_DIR
	mkdir $TEMP_OUTPUT_DIR
	cp -rf $MY_PATH/build/outputs/aar/$FILENAME $TEMP_OUTPUT_DIR
	cp $MY_PATH/$DOC_NAME$DOC_EXT $TEMP_OUTPUT_DIR
	cp -rf $MY_PATH/docs $TEMP_OUTPUT_DIR
}

function zipFiles {
	echo "Zipping files..."
	OUTPUT_ZIP="${OUTPUT}_${VERSION}"
	zip -r -q $OUTPUT_ZIP.zip $TEMP_OUTPUT_DIR/$FILENAME $TEMP_OUTPUT_DIR/$DOC_NAME$DOC_EXT $TEMP_OUTPUT_DIR/docs

	echo "Cleaning up..."
	rm -rf $MY_PATH/$TEMP_OUTPUT_DIR
}

while [[ $# > 0 ]]
	do
		key="$1"
		case $key in
			debug|release)
			    RELEASE="$1"
			    shift # past argument
			    ;;
		    --no-zip)
			    NO_ZIP=1
			    shift # past argument
			    ;;
		    *)
				shift
		   		;;
		esac
done

gradleBuild
getFilename
getVersion
generateJavadocs
moveFiles
if [ $NO_ZIP -eq 0 ]
  then
	zipFiles
fi

echo "Finished building!"
