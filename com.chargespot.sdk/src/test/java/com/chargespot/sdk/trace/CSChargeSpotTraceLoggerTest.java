/*
    Copyright (C) 2015 ChargeSpot
 */
package com.chargespot.sdk.trace;

import android.test.suitebuilder.annotation.SmallTest;

import junit.framework.TestCase;

/**
 * CSChargeSpotTraceLoggerTest
 *
 * Date: 10/12/2015
 * @version 0.3
 */
public class CSChargeSpotTraceLoggerTest
    extends TestCase
{
    private CSChargeSpotTraceLogger getTraceLoggerInstance() {
        return new CSChargeSpotTraceLogger();
    }

    @SmallTest
    public void testGetInstance()
        throws Exception
    {
        CSChargeSpotTraceLogger traceLogger = getTraceLoggerInstance();

        // Ensure no exceptions are thrown
        traceLogger.Trace(null, null);
        traceLogger.Trace(null, "test");
        traceLogger.Trace(CSChargeSpotTraceLevel.CHARGESPOT_TRACE_ERROR, null);
        traceLogger.Trace(CSChargeSpotTraceLevel.CHARGESPOT_TRACE_WARNING, null);
        traceLogger.Trace(CSChargeSpotTraceLevel.CHARGESPOT_TRACE_DEBUG, null);
    }
}
