/*
    Copyright (C) 2015 ChargeSpot
 */
package com.chargespot.sdk.api.v1.model;

/**
 * ChargeSpotRegionModel
 *
 * @version 0.5
 */
public final class ChargeSpotRegionModel {
    /**
     * ChargeSpot region identifier
     */
    public String identifier;

    /**
     * UUID of a ChargeSpot region
     */
    public String uuid;

    /**
     * Major version of the ChargeSpot
     */
    public int major;

    /**
     * Minor version of the ChargeSpot
     */
    public int minor;
}
