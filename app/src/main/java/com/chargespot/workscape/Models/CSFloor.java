package com.chargespot.workscape.Models;

import com.google.gson.annotations.SerializedName;

/**
 * Created by kelvinlau on 2016-07-20.
 */

public class CSFloor {
    public String name;
    public String identifier;
    public int client_id;

    @SerializedName("floor_number")
    public int floor;

    private static final String mFloorSuffix = "Floor";

    public  String floorName() {
        return getOrdinalFloorName(floor);
    }

    public static String getOrdinalFloorName(int floor) {
        int mod100 = floor % 100;
        int mod10 = floor % 10;
        if(mod10 == 1 && mod100 != 11) {
            return floor + "st " + mFloorSuffix;
        } else if(mod10 == 2 && mod100 != 12) {
            return floor + "nd " + mFloorSuffix;
        } else if(mod10 == 3 && mod100 != 13) {
            return floor + "rd " + mFloorSuffix;
        } else {
            return floor + "th " + mFloorSuffix;
        }
    }
}
