/*
    Copyright (C) 2015 ChargeSpot
 */
package com.chargespot.sdk.api.v1;

import com.chargespot.sdk.api.v1.model.ChargeSpotGroupHookListModel;
import com.chargespot.sdk.api.v1.model.ChargeSpotGroupListModel;
import com.chargespot.sdk.api.v1.model.ChargeSpotHookListModel;
import com.chargespot.sdk.api.v1.model.ChargeSpotRegionListModel;

import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Header;
import retrofit2.http.Query;

/**
 * ChargeSpot REST API V1 interface
 *
 * @version 0.5
 */
public interface IChargeSpotApi {
    @GET("chargespot/region/list")
    Call<ChargeSpotRegionListModel> listRegions(
            @Header("Application-Token") String token,
            @Header("X-Device-Identifier") String deviceUid,
            @Header("X-User-Agent") String userAgent,
            @Header("X-SDK-Version") String sdkVersion,
            @Header("X-User-Language") String contentLanguage,
            @Header("X-Request-Origin") String requestOrigin);

    @GET("group/list")
    Call<ChargeSpotGroupListModel> listGroups(
            @Header("Application-Token") String token,
            @Header("X-Device-Identifier") String deviceUid,
            @Header("X-User-Agent") String userAgent,
            @Header("X-SDK-Version") String sdkVersion,
            @Header("X-User-Language") String contentLanguage,
            @Header("X-Request-Origin") String requestOrigin);

    @GET("group/hook/list")
    Call<ChargeSpotGroupHookListModel> listGroupHooks(
            @Header("Application-Token") String token,
            @Header("X-Device-Identifier") String deviceUid,
            @Header("X-User-Agent") String userAgent,
            @Header("X-SDK-Version") String sdkVersion,
            @Header("X-User-Language") String contentLanguage,
            @Header("X-Request-Origin") String requestOrigin,
            @Query("uuid") String uuid,
            @Query("major") int major,
            @Query("minor") int minor,
            @Query("event") int event);

    @GET("chargespot/hook/list")
    Call<ChargeSpotHookListModel> listChargeSpotHooks(
            @Header("Application-Token") String token,
            @Header("X-Device-Identifier") String deviceUid,
            @Header("X-User-Agent") String userAgent,
            @Header("X-SDK-Version") String sdkVersion,
            @Header("X-User-Language") String contentLanguage,
            @Header("X-Request-Origin") String requestOrigin,
            @Query("uuid") String uuid,
            @Query("major") int major,
            @Query("minor") int minor,
            @Query("event") int event);
}
