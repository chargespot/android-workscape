/*
    Copyright (C) 2015 ChargeSpot
 */
package com.chargespot.sdk.discovery.kitkat;

import com.chargespot.sdk.CSChargeSpotManager;
import com.chargespot.sdk.api.v1.model.ChargeSpotRegionListModel;
import com.chargespot.sdk.api.v1.model.ChargeSpotRegionModel;
import com.chargespot.sdk.exceptions.CSInvalidParameterException;
import com.chargespot.sdk.helper.HexConverterHelper;
import com.chargespot.sdk.helper.BeaconHelper;
import com.chargespot.sdk.model.CSChargeSpot;
import com.chargespot.sdk.model.CSChargeSpotRadio;

import java.util.ArrayList;

/**
 * Helper to handle the BLE record information
 *
 * @version 0.5
 */
final class DeviceDiscoveryHelper {
    private DeviceDiscoveryHelper() {
    }

    public static CSChargeSpotRadio parseBeaconRecord(final String address, final byte[] scanRecord, final int rssi)
            throws CSInvalidParameterException {
        final ChargeSpotRegionListModel regionListModel = CSChargeSpotManager.getInstance().getRegionList();
        ArrayList<String> chargeSpotUUIDs = new ArrayList<>();
        for (ChargeSpotRegionModel region : regionListModel.chargespot_regions) {
            chargeSpotUUIDs.add(BeaconHelper.formatUUIDForFilter(region.uuid));
        }

        for (int idx = 0; idx < scanRecord.length; idx++) {
            if (scanRecord[idx] == (byte) 0x1A && scanRecord[idx + 1] == (byte) 0xFF) {
                // Manufacturer AD record
                final int company = scanRecord[idx + 2] << 8 + scanRecord[idx + 3];
                final short APPLE_MANUFACTURER = 0x4C00;
                if (company != APPLE_MANUFACTURER) {
                    idx += scanRecord[idx];
                    continue;
                }

                final String UUID = HexConverterHelper.convertBytesToHex(scanRecord, idx + 6, 16);

                if (!chargeSpotUUIDs.contains(UUID)) {
                    idx += scanRecord[idx];
                    continue;
                }

                final int major = ((int) (scanRecord[idx + 22] & 0xFF) << 8) + (int) (scanRecord[idx + 23] & 0xFF);
                final int minor = ((int) (scanRecord[idx + 24] & 0xFF) << 8) + (int) (scanRecord[idx + 25] & 0xFF);
                final int txPower = ((byte) (scanRecord[idx + 26] & 0xFF));

                final CSChargeSpot device = new CSChargeSpot();

                device.Address = address;
                device.UUID = UUID;
                device.Major = major;
                device.Minor = minor;

                return new CSChargeSpotRadio(device, rssi, txPower);
            } else {
                idx += scanRecord[idx];
            }
        }
        return null;
    }
}
