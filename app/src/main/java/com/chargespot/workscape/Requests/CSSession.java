package com.chargespot.workscape.Requests;

import android.net.Uri;

import com.chargespot.workscape.Models.CSClient;
import com.google.gson.annotations.SerializedName;

import java.util.Date;

/**
 * Created by kelvinlau on 2016-08-04.
 */

public class CSSession {

    @SerializedName("auth_token")
    public String authToken;

    @SerializedName("data")
    private CSSessionUserData userData;

    public CSClient getClient() {
        return userData.client;
    }
}

class CSSessionUserData {

    public int clientId;

    public Date creationDate;

    public Date lastUpdated;

    public String firstName;

    public String lastName;

    public String email;

    public Uri imageUrl;

    public int role;

    public int identifier;

    public CSClient client;

}

