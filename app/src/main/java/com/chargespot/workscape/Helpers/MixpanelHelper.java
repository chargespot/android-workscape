package com.chargespot.workscape.Helpers;

import android.content.Context;

import com.mixpanel.android.mpmetrics.MixpanelAPI;

import org.json.JSONObject;

/**
 * Created by kelvinlau on 2016-08-31.
 */

public class MixpanelHelper {

    private static final String MIXPANEL_API_TOKEN = "55612f7ec5769a10712cc0e68b517ac8";

    public static MixpanelAPI getInstance(Context context) {
        return MixpanelAPI.getInstance(context, MIXPANEL_API_TOKEN);
    }

    public static void track(Context context, String eventName) {
        MixpanelAPI mixpanel = MixpanelHelper.getInstance(context);
        mixpanel.track(eventName);
    }

    public static void track(Context context, String eventName, JSONObject jsonObject) {
        MixpanelAPI mixpanel = MixpanelHelper.getInstance(context);
        mixpanel.track(eventName, jsonObject);
    }

    public static void flush(Context context) {
        MixpanelAPI mixpanel = MixpanelHelper.getInstance(context);
        mixpanel.flush();
    }

    public static void identify(Context context, String identifyString) {
        MixpanelAPI mixpanel = MixpanelHelper.getInstance(context);
        mixpanel.getPeople().identify(identifyString);
    }

    public static void setPeople(Context context, JSONObject object) {
        MixpanelAPI mixpanel = MixpanelHelper.getInstance(context);
        mixpanel.getPeople().set(object);
    }
}
