/*
    Copyright (C) 2015 ChargeSpot
 */
package com.chargespot.sdk.api.v1.model;

import com.chargespot.sdk.helper.BeaconHelper;
import com.chargespot.sdk.model.CSChargeSpot;

/**
 * Region List of ChargeSpots
 *
 * @version 0.5
 */
public class ChargeSpotRegionListModel {
    /**
     * List of ChargeSpot regions
     */
    public ChargeSpotRegionModel[] chargespot_regions;

    public boolean beaconInRegionList(CSChargeSpot chargeSpot) {
        for (ChargeSpotRegionModel regionModel : chargespot_regions) {
            if (BeaconHelper.formatUUIDForFilter(regionModel.uuid).equals(chargeSpot.UUID) && regionModel.major == chargeSpot.Major) {
                return true;
            }
        }
        return false;
    }
}
