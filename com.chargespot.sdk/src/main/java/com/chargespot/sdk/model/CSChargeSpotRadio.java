/*
    Copyright (C) 2015 ChargeSpot
 */
package com.chargespot.sdk.model;

import android.support.annotation.NonNull;

/**
 * ChargeSpot Radio information
 * Date: 10/12/2015
 *
 * @version 0.3
 */
public final class CSChargeSpotRadio {
    private CSChargeSpot mChargeSpot;
    private int mRssi;
    private int mTxPower;
    private int mPingCount;
    private int mSignalRating;

    private static final int CHARGESPOT_IMMEDIATE_THRESHOLD = 5;
    private static final int CHARGESPOT_NEAR_THRESHOLD = -15;

    {
        this.mRssi = Integer.MAX_VALUE;
        this.mTxPower = Integer.MAX_VALUE;
        this.mPingCount = 0;
    }

    public CSChargeSpotRadio(@NonNull CSChargeSpot device, int rssi, int txPower) {
        this.mChargeSpot = device;
        this.mRssi = rssi;
        this.mTxPower = txPower;
        this.mSignalRating = this.mRssi - this.mTxPower;
    }

    public CSChargeSpot getChargeSpot() {
        return this.mChargeSpot;
    }

    public int getRssi() {
        return this.mRssi;
    }

    public int getTxPower() {
        return this.mTxPower;
    }

    public int getPingCount() {
        return this.mPingCount;
    }

    public int getSignalRating() {
        return this.mSignalRating;
    }

    public void incrementPingCount() {
        this.mPingCount++;
    }

    public int getDistance() {
        if (this.mSignalRating > CHARGESPOT_IMMEDIATE_THRESHOLD) {
            return CSDistance.IMMEDIATE;
        } else if (this.mSignalRating > CHARGESPOT_NEAR_THRESHOLD) {
            return CSDistance.NEAR;
        }
        return CSDistance.FAR;
    }

    @Override
    public boolean equals(Object o) {
        if (o == null || o.getClass() != CSChargeSpotRadio.class) {
            return false;
        }

        CSChargeSpotRadio otherRadio = (CSChargeSpotRadio) o;
        if (otherRadio.getChargeSpot() != this.mChargeSpot ||
                otherRadio.getRssi() != this.mRssi ||
                otherRadio.getTxPower() != this.mTxPower ||
                otherRadio.getPingCount() != this.getPingCount()) {
            return false;
        }

        return true;
    }
}
