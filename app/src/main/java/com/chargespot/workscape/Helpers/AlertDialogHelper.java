package com.chargespot.workscape.Helpers;

import android.content.Context;
import android.content.DialogInterface;
import android.support.v7.app.AlertDialog;

import com.chargespot.workscape.R;
import com.chargespot.workscape.Requests.CSRequestManager;
import com.chargespot.workscape.Models.CSCalendarEvent;
import com.chargespot.workscape.Models.CSRoom;

import retrofit2.Callback;


/**
 * Created by kelvinlau on 2016-08-03.
 */

public class AlertDialogHelper {
    public interface AlertDialogListener {
        void OnAlertDialogSelect(int position);
        void OnAlertDialogCancel();
    }

    public static AlertDialog createBookNowDialog(Context context, String title, CharSequence[] items, final AlertDialogListener listener) {
        return new AlertDialog.Builder(context)
                .setTitle(title)
                .setItems(items, new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                        listener.OnAlertDialogSelect(which);
                    }
                })
                .setNegativeButton(context.getString(R.string.cancel), new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        listener.OnAlertDialogCancel();
                        dialog.dismiss();
                    }
                })
                .show();
    }

    public static AlertDialog createEndNowDialog(final Context context, final CSRoom room, final AlertDialogListener listener, final Callback<CSCalendarEvent> callback) {
        return new AlertDialog.Builder(context)
                .setMessage(context.getString(R.string.end_now_dialog))
                .setPositiveButton(context.getString(R.string.yes), new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        CSCalendarEvent nextEvent = room.getNextEvent();
                        try {
                            CSRequestManager.getInstance(context).endNow(room.getIdentifier(), nextEvent.identifier, callback);
                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                    }
                })
                .setNegativeButton(context.getString(R.string.cancel), new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        listener.OnAlertDialogCancel();
                        dialog.dismiss();
                    }
                }).show();
    }
}
