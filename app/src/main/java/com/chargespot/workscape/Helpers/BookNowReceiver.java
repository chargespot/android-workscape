package com.chargespot.workscape.Helpers;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;

import com.chargespot.workscape.Models.CSCalendarEvent;
import com.chargespot.workscape.Models.CSRoom;
import com.chargespot.workscape.R;

import java.util.Date;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Created by kelvinlau on 2016-08-23.
 */

public class BookNowReceiver extends BroadcastReceiver{

    public static final String TAG = BookNowReceiver.class.getCanonicalName();

    @Override
    public void onReceive(final Context context, Intent intent) {
        NotificationHelper.cancelNotification(context, intent);

        final CSRoom room = CSRoom.fromJson(intent.getStringExtra(NotificationHelper.NOTIFICATION_ROOM_KEY));

        if (room == null) {
            handleBookingError(context);
            return;
        }

        Date startDate = new Date();
        Date endDate = new Date();
        endDate.setTime(intent.getLongExtra(NotificationHelper.NOTIFICATION_END_DATE_KEY, 0));

        if (startDate.getTime() > endDate.getTime()) {
            handleBookingError(context);
            return;
        }

        CSCalendarEvent.createEvent(context, room, startDate, endDate, new Callback<CSCalendarEvent>() {
            @Override
            public void onResponse(Call<CSCalendarEvent> call, Response<CSCalendarEvent> response) {
                CSCalendarEvent event = response.body();
                NotificationHelper.createEndNowNotification(context, context.getString(context.getApplicationInfo().labelRes), context.getString(R.string.welcome_text, room.getName()), room, event);
            }

            @Override
            public void onFailure(Call<CSCalendarEvent> call, Throwable t) {
                handleBookingError(context);
            }
        });
    }

    private void handleBookingError(Context context) {
        NotificationHelper.createNotification(context, context.getString(context.getApplicationInfo().labelRes), context.getString(R.string.booking_error));
    }

}
