package com.chargespot.workscape;
import android.app.Dialog;

import com.chargespot.workscape.Helpers.MixpanelHelper;
import com.chargespot.workscape.Models.CSCalendarEvent;
import com.chargespot.workscape.Models.CSRoom;
import com.wdullaer.materialdatetimepicker.time.RadialPickerLayout;
import com.wdullaer.materialdatetimepicker.time.TimePickerDialog;
import com.wdullaer.materialdatetimepicker.date.DatePickerDialog;

import android.content.Context;
import android.content.DialogInterface;
import android.graphics.Color;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.CompoundButton;
import android.widget.FrameLayout;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.Switch;
import android.widget.TextView;
import android.widget.Toast;

import com.chargespot.workscape.Helpers.DateHelper;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Created by kelvinlau on 2016-08-05.
 */

public class BookLaterFragment extends BaseFragment
    implements View.OnClickListener,
        TimePickerDialog.OnTimeSetListener,
        Dialog.OnCancelListener,
        DatePickerDialog.OnDateSetListener,
        Switch.OnCheckedChangeListener {

    public interface BookLaterFragmentListener {
        void returnHome();
    }

    private CSRoom mRoom;
    private BookLaterFragmentListener mListener;

    private Date mStartDate;
    private Date mEndDate;

    private DateFormat mDateFormatter;
    private DateFormat mTimeFormatter;

    private Switch mAllDaySwitch;

    private Button mCreateMeetingButton;

    private TextView mMeetingNameTextView;
    private TextView mStartDateTextView;
    private TextView mStartTimeTextView;
    private TextView mEndTimeTextView;
    private TextView mEndDateTextView;

    private LinearLayout mStartDateLinearLayout;
    private LinearLayout mEndDateLinearLayout;

    private Calendar mCalendar;

    private FrameLayout mMaskView;
    private ProgressBar mLoadingSpinner;

    private boolean mStartDateEditing;
    private boolean mEndDateEditng;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_book_later, container, false);

        Date currentDate = new Date();

        mStartDate = DateHelper.roundDateToNearestFifteenMinutes(currentDate);
        mEndDate = DateHelper.addMinutesToDate(mStartDate, 30);

        mCalendar = Calendar.getInstance();

        mDateFormatter = new SimpleDateFormat("EEE d MMM yyyy");
        mTimeFormatter = new SimpleDateFormat("h:mm a");

        mMeetingNameTextView = (TextView)view.findViewById(R.id.meetingNameTextView);
        mMeetingNameTextView.setOnClickListener(this);

        mStartDateTextView = (TextView)view.findViewById(R.id.startDateTextView);
        mStartTimeTextView = (TextView)view.findViewById(R.id.startTimeTextView);
        mEndDateTextView = (TextView)view.findViewById(R.id.endDateTextView);
        mEndTimeTextView = (TextView)view.findViewById(R.id.endTimeTextView);
        mStartTimeTextView.setOnClickListener(this);
        mEndTimeTextView.setOnClickListener(this);
        mStartDateTextView.setOnClickListener(this);
        mEndDateTextView.setOnClickListener(this);
        refreshDates();

        mStartDateLinearLayout = (LinearLayout)view.findViewById(R.id.startDateLinearLayout);
        mEndDateLinearLayout = (LinearLayout)view.findViewById(R.id.endDateLinearLayout);

        mAllDaySwitch = (Switch)view.findViewById(R.id.allDaySwitch);
        mAllDaySwitch.setOnCheckedChangeListener(this);

        mCreateMeetingButton = (Button)view.findViewById(R.id.createMeetingButton);
        mCreateMeetingButton.setOnClickListener(this);

        mMaskView = (FrameLayout)view.findViewById(R.id.maskView);
        mLoadingSpinner = (ProgressBar)view.findViewById(R.id.loadingSpinner);

        return view;
    }

    @Override
    public void onStart() {
        super.onStart();
        MixpanelHelper.track(getActivity(), "BookLaterFragment - Book Later");
    }

    private void refreshDates() {
        mStartDateTextView.setText(mDateFormatter.format(mStartDate));
        mStartTimeTextView.setText(mTimeFormatter.format(mStartDate));
        mEndDateTextView.setText(mDateFormatter.format(mEndDate));
        mEndTimeTextView.setText(mTimeFormatter.format(mEndDate));
    }

    private void setDatesEnabled(boolean enabled) {
        mStartDateLinearLayout.setEnabled(enabled);
        mStartDateTextView.setEnabled(enabled);
        mStartTimeTextView.setEnabled(enabled);

        mEndDateLinearLayout.setEnabled(enabled);
        mEndDateTextView.setEnabled(enabled);
        mEndTimeTextView.setEnabled(enabled);
    }

    private void setLoading(boolean loading) {
        if (loading) {
            mMaskView.setVisibility(View.VISIBLE);
            mLoadingSpinner.setVisibility(View.VISIBLE);
        } else {
            mMaskView.setVisibility(View.GONE);
            mLoadingSpinner.setVisibility(View.GONE);
        }

        mMeetingNameTextView.setEnabled(!loading);
        mAllDaySwitch.setEnabled(!loading);
        mStartDateTextView.setEnabled(!loading);
        mStartTimeTextView.setEnabled(!loading);
        mEndDateTextView.setEnabled(!loading);
        mEndTimeTextView.setEnabled(!loading);
    }

    @Override
    public void onClick(View v) {
        switch(v.getId()) {
            case R.id.startTimeTextView:
                mStartDateEditing = true;
                createTimePickerDialog(mStartDate);
                break;
            case R.id.endTimeTextView:
                mEndDateEditng = true;
                createTimePickerDialog(mEndDate);
                break;
            case R.id.startDateTextView:
                mStartDateEditing = true;
                createDatePickerDialog(mStartDate);
                break;
            case R.id.endDateTextView:
                mEndDateEditng = true;
                createDatePickerDialog(mEndDate);
                break;
            case R.id.createMeetingButton:
                validateAndSubmit();
                break;
        }
    }

    private void createTimePickerDialog(Date initialDate) {
        mCalendar.setTime(initialDate);

        TimePickerDialog timePickerDialog = TimePickerDialog.newInstance(this, mCalendar.get(Calendar.HOUR_OF_DAY), mCalendar.get(Calendar.MINUTE), false);
        timePickerDialog.setTimeInterval(1, 15);
        timePickerDialog.setMinTime(mCalendar.get(Calendar.HOUR), mCalendar.get(Calendar.MINUTE), mCalendar.get(Calendar.SECOND));
        timePickerDialog.show(getActivity().getFragmentManager(), "TimePickerDialog");
    }

    private void createDatePickerDialog(Date initialDate) {
        mCalendar.setTime(initialDate);

        DatePickerDialog datePickerDialog = com.wdullaer.materialdatetimepicker.date.DatePickerDialog.newInstance(this, mCalendar.get(Calendar.YEAR), mCalendar.get(Calendar.MONTH), mCalendar.get(Calendar.DAY_OF_MONTH));
        datePickerDialog.show(getActivity().getFragmentManager(), "DatePickerDialog");
    }

    private void validateAndSubmit() {
        if (mMeetingNameTextView.getText().length() == 0) {
            mMeetingNameTextView.requestFocus();
            mMeetingNameTextView.setHintTextColor(Color.parseColor("#FF8080"));
            InputMethodManager imm = (InputMethodManager) getActivity().getSystemService(Context.INPUT_METHOD_SERVICE);
            imm.showSoftInput(mMeetingNameTextView, InputMethodManager.SHOW_IMPLICIT);
            return;
        }

        setLoading(true);
        String meetingSummary = mMeetingNameTextView.getText().toString();
        CSCalendarEvent.createEvent(getActivity(), mRoom, meetingSummary, mStartDate, mEndDate, new Callback<CSCalendarEvent>() {
            @Override
            public void onResponse(Call<CSCalendarEvent> call, Response<CSCalendarEvent> response) {
                Toast.makeText(getActivity(), "Successfully created booking for " + mRoom.getName(), Toast.LENGTH_SHORT);
                if (mListener != null) {
                    mListener.returnHome();
                }
                MixpanelHelper.track(getActivity(), "BookLaterFragment - Book Later Successful");
            }

            @Override
            public void onFailure(Call<CSCalendarEvent> call, Throwable t) {
                Toast.makeText(getActivity(), "Failed to create booking. Please try again.", Toast.LENGTH_SHORT);
                setLoading(false);
                MixpanelHelper.track(getActivity(), "BookLaterFragment - Book Later Failed");
            }
        });
    }

    @Override
    public void onTimeSet(RadialPickerLayout view, int hourOfDay, int minute, int second) {
        Log.d(TAG, "OnTimeSet - " + hourOfDay + "h " + minute + "minute ");
        if (mStartDateEditing) {
            setStartDate(hourOfDay, minute);
        } else if (mEndDateEditng) {
            setEndDate(hourOfDay, minute);
        }

        mStartDateEditing = false;
        mEndDateEditng = false;
    }

    @Override
    public void onDateSet(DatePickerDialog view, int year, int monthOfYear, int dayOfMonth) {
        if (mStartDateEditing) {
            setStartDate(year, monthOfYear, dayOfMonth);
        } else if (mEndDateEditng) {
            setEndDate(year, monthOfYear, dayOfMonth);
        }

        mStartDateEditing = false;
        mEndDateEditng = false;
    }

    @Override
    public void onCancel(DialogInterface dialog) {
        mStartDateEditing = false;
        mEndDateEditng = false;
        dialog.cancel();
    }

    private void setStartDate(int hourOfDay, int minute) {
        mCalendar.setTime(mStartDate);
        mCalendar.set(Calendar.HOUR_OF_DAY, hourOfDay);
        mCalendar.set(Calendar.MINUTE, minute);
        mStartDate = mCalendar.getTime();
        if (mStartDate.getTime() > mEndDate.getTime()) {
            mEndDate = DateHelper.addMinutesToDate(mStartDate, 30);
        }
        refreshDates();
    }

    private void setStartDate(int year, int monthOfYear, int dayOfMonth) {
        mCalendar.setTime(mStartDate);
        mCalendar.set(Calendar.YEAR, year);
        mCalendar.set(Calendar.MONTH, monthOfYear);
        mCalendar.set(Calendar.DAY_OF_MONTH, dayOfMonth);
        mStartDate = mCalendar.getTime();

        if (mStartDate.getTime() > mEndDate.getTime()) {
            mEndDate = DateHelper.addMinutesToDate(mStartDate, 30);

        }
        refreshDates();
    }

    private void setEndDate(int hourOfDay, int minute) {
        mCalendar.setTime(mEndDate);
        mCalendar.set(Calendar.HOUR_OF_DAY, hourOfDay);
        mCalendar.set(Calendar.MINUTE, minute);
        mEndDate = mCalendar.getTime();
        if (mStartDate.getTime() > mEndDate.getTime()) {
            mEndDate = DateHelper.addMinutesToDate(mEndDate, -30);
        }
        refreshDates();
    }

    private void setEndDate(int year, int monthOfYear, int dayOfMonth) {
        mCalendar.setTime(mEndDate);
        mCalendar.set(Calendar.YEAR, year);
        mCalendar.set(Calendar.MONTH, monthOfYear);
        mCalendar.set(Calendar.DAY_OF_MONTH, dayOfMonth);
        mEndDate = mCalendar.getTime();

        if (mStartDate.getTime() > mEndDate.getTime()) {
            mStartDate = DateHelper.addMinutesToDate(mEndDate, -30);
        }
        refreshDates();
    }

    @Override
    public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
        if (isChecked) {
            mCalendar.setTime(mStartDate);
            mCalendar.set(Calendar.HOUR_OF_DAY, 0);
            mCalendar.set(Calendar.MINUTE, 0);
            mStartDate = mCalendar.getTime();

            mCalendar.set(Calendar.HOUR_OF_DAY, 23);
            mCalendar.set(Calendar.MINUTE, 59);
            mEndDate = mCalendar.getTime();

            refreshDates();
            setDatesEnabled(false);
        } else {
            setDatesEnabled(true);
        }
    }

    public void setRoom(CSRoom room) {
        mRoom = room;
    }

    public void setBookLaterFragmentListener(BookLaterFragmentListener listener) {
        mListener = listener;
    }
}
