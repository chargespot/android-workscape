/*
    Copyright (C) 2015 ChargeSpot
 */
package com.chargespot.sdk.trace;

/**
 * Different trace levels
 * <p/>
 * Date: 05/12/2015
 *
 * @version 0.3
 */
public enum CSChargeSpotTraceLevel {
    /**
     * Error level trace message. This should be taken
     * as highest priority message
     */
    CHARGESPOT_TRACE_ERROR,

    /**
     * Warning level trace message.
     */
    CHARGESPOT_TRACE_WARNING,

    /**
     * Debug level trace message.
     */
    CHARGESPOT_TRACE_DEBUG
}