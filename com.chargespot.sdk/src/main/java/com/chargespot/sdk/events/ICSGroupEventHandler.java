/*
    Copyright (C) 2015 ChargeSpot
 */
package com.chargespot.sdk.events;

import com.chargespot.sdk.api.v1.model.ChargeSpotGroupHookListModel;
import com.chargespot.sdk.exceptions.CSException;

/**
 * ChargeSpot group event handler
 *
 * @version 0.5
 */
public interface ICSGroupEventHandler {
    /**
     * ChargeSpot group event has occurred
     *
     * @param event         Kind of the event that occurred
     * @param groupHookList List of hook models
     */
    void onGroupEventSuccess(CSGroupEvent event, ChargeSpotGroupHookListModel groupHookList);

    /**
     * An exception has been found during the tracking of
     * ChargeSpot group.
     *
     * @param e Exception that occurred while trying to
     *          track a ChargeSpot group.
     */
    void onGroupEventFailure(CSException e);
}
