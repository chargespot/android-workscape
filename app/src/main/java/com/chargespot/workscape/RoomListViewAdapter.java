package com.chargespot.workscape;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v4.content.ContextCompat;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.chargespot.workscape.Models.CSCalendarEvent;
import com.chargespot.workscape.Models.CSRoom;
import com.chargespot.workscape.Models.CSFloor;

import java.util.List;

/**
 * Created by kelvinlau on 2016-07-21.
 */

public class RoomListViewAdapter extends ArrayAdapter<CSRoom> {

    public interface BookNowListener {
        void onBookNowClick(View sender, CSRoom roomModel);
    }

    private BookNowListener mListener;

    public final static String TAG = RoomListViewAdapter.class.getCanonicalName();
    private List<CSRoom> mRoomList;
    private Context mContext;
    private LinearLayout mBookNowLinearLayout;

    private ProgressBar mLoadingSpinner;

    public RoomListViewAdapter(Context context, int resource) {
        super(context, resource);
    }

    public RoomListViewAdapter(Context context, int resource, List<CSRoom> roomModelList) {
        super(context, resource, roomModelList);
        mRoomList = roomModelList;
        mContext = context;
    }

    static class ViewHolder {
        CSRoom room;
        LinearLayout bookNowLinearLayout;
        ProgressBar loadingSpinner;
        int position;
    }

    @NonNull
    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        final CSRoom room = mRoomList.get(position);
        View view = convertView;
        final ViewHolder holder;
        if (view == null) {
            LayoutInflater inflater = LayoutInflater.from(getContext());
            view = inflater.inflate(R.layout.listview_room, null);
            holder = new ViewHolder();
            holder.position = position;
            holder.bookNowLinearLayout = (LinearLayout)view.findViewById(R.id.bookNowLinearLayout);
            holder.loadingSpinner = (ProgressBar)view.findViewById(R.id.loadingSpinner);
            view.setTag(holder);
        } else {
            holder = (ViewHolder) view.getTag();
        }
        holder.room = room;

        TextView roomNameTextView = (TextView)view.findViewById(R.id.roomNameTextView);
        TextView floorTextView = (TextView)view.findViewById(R.id.floorTextView);
        TextView capacityTextView = (TextView)view.findViewById(R.id.capacityTextView);
        mBookNowLinearLayout = holder.bookNowLinearLayout;
        mBookNowLinearLayout.setVisibility(View.VISIBLE);

        ImageView bookNowImageView = (ImageView)view.findViewById(R.id.bookNowImageView);
        TextView bookNowTextView = (TextView)view.findViewById(R.id.bookNowTextView);

        mLoadingSpinner = holder.loadingSpinner;
        mLoadingSpinner.setVisibility(View.GONE);


        roomNameTextView.setText(room.getName());

        if (room.getFloor() != null) {
            CSFloor floor = room.getFloor();
            floorTextView.setText(floor.floorName());
        }

        capacityTextView.setText(Integer.toString(room.getCapacity()));

        int roomState = room.getState();
        CSCalendarEvent event = room.getNextEvent();
        bookNowTextView.setText(mContext.getString(R.string.book_now).toUpperCase());
        if (roomState == room.ROOM_STATE_AVAILABLE) {
            bookNowImageView.setColorFilter(ContextCompat.getColor(mContext, R.color.colorCSTeal));
            bookNowTextView.setTextColor(ContextCompat.getColor(mContext, R.color.colorCSTeal));
        } else if (roomState == room.ROOM_STATE_AVAILABLE_LIMITED) {
            bookNowImageView.setColorFilter(ContextCompat.getColor(mContext, R.color.colorCSYellow));
            bookNowTextView.setTextColor(ContextCompat.getColor(mContext, R.color.colorCSYellow));
        } else if (roomState == room.ROOM_STATE_OCCUPIED) {
            if (event != null && event.canEndNow()) {
                bookNowTextView.setText(mContext.getString(R.string.end_now).toUpperCase());
            } else {
                bookNowTextView.setText("OCCUPIED");
            }

            bookNowImageView.setColorFilter(ContextCompat.getColor(mContext, R.color.colorCSRed));
            bookNowTextView.setTextColor(ContextCompat.getColor(mContext, R.color.colorCSRed));
        } else {
            mBookNowLinearLayout.setVisibility(View.GONE);
            bookNowImageView.setVisibility(View.GONE);
            bookNowTextView.setVisibility(View.GONE);
        }

        holder.bookNowLinearLayout.setOnClickListener(new View.OnClickListener() {
            @Override
                public void onClick(View v) {
                int roomState = holder.room.getState();
                CSCalendarEvent event = holder.room.getNextEvent();
                if (roomState == CSRoom.ROOM_STATE_OCCUPIED && !event.canEndNow()) {
                    return;
                }
                ProgressBar loadingSpinner = holder.loadingSpinner;
                LinearLayout bookNowLinearLayout = holder.bookNowLinearLayout;

                bookNowLinearLayout.setVisibility(View.GONE);
                loadingSpinner.setVisibility(View.VISIBLE);
                if (mListener != null) {
                    mListener.onBookNowClick(v, room);
                }
            }
        });

        return view;
    }

    public void updateRoomModel(List<CSRoom> roomModel) {
        mRoomList.clear();
        mRoomList.addAll(roomModel);
        notifyDataSetChanged();
    }

    public void setBookNowListener(BookNowListener listener) {
        mListener = listener;
    }

    @Override
    public int getItemViewType(int position) {
        return position;
    }
}

