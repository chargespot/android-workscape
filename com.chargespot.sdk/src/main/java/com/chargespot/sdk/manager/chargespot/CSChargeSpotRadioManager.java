package com.chargespot.sdk.manager.chargespot;

import com.chargespot.sdk.CSChargeSpotManager;
import com.chargespot.sdk.api.v1.model.ChargeSpotRegionListModel;
import com.chargespot.sdk.api.v1.model.ChargeSpotRegionModel;
import com.chargespot.sdk.model.CSChargeSpotRadio;
import com.chargespot.sdk.trace.CSChargeSpotTraceLogger;
import com.chargespot.sdk.trace.ICSChargeSpotTraceLogger;

import java.util.HashMap;
import java.util.List;

/**
 * Created by kelvinlau on 16-04-14.
 */
public final class CSChargeSpotRadioManager {

    //region Trace
    private ICSChargeSpotTraceLogger mTraceLogger;
    //endregion

    private HashMap<String, CSChargeSpotRadio> mRadioMap;

    private int mMinimumPing = 3;

    public CSChargeSpotRadioManager() {
        this.mTraceLogger = new CSChargeSpotTraceLogger();
        this.mRadioMap = new HashMap<String, CSChargeSpotRadio>();
    }

    public CSChargeSpotRadio getClosestValidRadio(int maximumDistance) {
        CSChargeSpotRadio radio = this.getRadioWithHighestSignalFromMap(maximumDistance);
        return radio;
    }

    public void addRadio(CSChargeSpotRadio radio) {
        ChargeSpotRegionListModel regionList = CSChargeSpotManager.getInstance().getRegionList();
        if (regionList != null && regionList.beaconInRegionList(radio.getChargeSpot())) {
            this.insertRadioIntoMap(radio);
        }
    }

    public HashMap<String, CSChargeSpotRadio> getRadios() {
        return this.mRadioMap;
    }

    public void setMinimumPing(int minimumPing) {
        this.mMinimumPing = minimumPing;
    }

    public int getMinimumPing() {
        return this.mMinimumPing;
    }

    protected void insertRadioIntoMap(CSChargeSpotRadio radio) {
        String radioKey = getKeyFromRadio(radio);
        if (this.mRadioMap.containsKey(radioKey)) {
            CSChargeSpotRadio mapRadio = this.mRadioMap.get(radioKey);
            mapRadio.incrementPingCount();
        } else {
            radio.incrementPingCount();
            this.mRadioMap.put(radioKey, radio);
        }
    }

    protected String getKeyFromRadio(CSChargeSpotRadio radio) {
        return "" + radio.getChargeSpot().Address + radio.getChargeSpot().Major + radio.getChargeSpot().Minor + radio.getDistance();
    }

    protected CSChargeSpotRadio getRadioWithHighestSignalFromMap(int maximumDistance) {
        CSChargeSpotRadio radio = null;
        for (CSChargeSpotRadio mapRadio : this.mRadioMap.values()) {
            if (mapRadio.getPingCount() >= mMinimumPing && mapRadio.getDistance() <= maximumDistance) {
                if (radio == null) {
                    radio = mapRadio;
                } else {
                    radio = radio.getSignalRating() < mapRadio.getSignalRating() ? radio : mapRadio;
                }
            }
        }

        return radio;
    }
}
