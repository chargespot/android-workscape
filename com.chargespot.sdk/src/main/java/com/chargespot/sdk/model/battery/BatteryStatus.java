/*
    Copyright (C) 2015 ChargeSpot
 */
package com.chargespot.sdk.model.battery;

/**
 * Battery Status for the device
 * <p/>
 * Date: 10/12/2015
 *
 * @version 0.5
 */
public final class BatteryStatus {
    public boolean isCharging;
    public boolean isCharged;
}
