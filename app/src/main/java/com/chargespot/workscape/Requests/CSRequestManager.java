package com.chargespot.workscape.Requests;

import android.content.Context;
import android.content.pm.PackageInfo;
import android.graphics.Bitmap;
import android.support.annotation.Nullable;
import android.util.Log;

import com.chargespot.workscape.CSRouter;
import com.chargespot.workscape.Helpers.BitmapHelper;
import com.chargespot.workscape.Helpers.DeviceInformationHelper;
import com.chargespot.workscape.Helpers.GsonStringConverterFactory;
import com.chargespot.workscape.Models.CSCalendarEvent;
import com.chargespot.workscape.Models.CSRoom;
import com.google.android.gms.auth.api.signin.GoogleSignInAccount;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;
import java.util.concurrent.TimeUnit;

import okhttp3.Interceptor;
import okhttp3.MediaType;
import okhttp3.MultipartBody;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.RequestBody;
import okhttp3.Response;
import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

/**
 * Created by kelvinlau on 2016-07-20.
 */

public class CSRequestManager {
    public static final String TAG = CSRequestManager.class.getCanonicalName();
    private static final String ApiEndpointUri = "https://dash.workscape.io/api/v1/";

    private static CSRequestManager mInstance;
    private Retrofit mRetroFit;
    private Context mContext;
    private RequestService mRequestService;
    private FileUploadService mFileUploadService;
    private String mToken;
    private String mRequestOrigin;
    private DeviceInformationHelper mDeviceInformationHelper;

    private static final int mMaxImageEdge = 512;

    public static CSRequestManager getInstance(Context context) {
        if (mInstance == null) {
            mInstance = new CSRequestManager();
        } else {
            mInstance.mContext = context;
        }
        mInstance.refreshForNewRequest(context);
        return mInstance;
    }

    public CSRequestManager() {
        OkHttpClient okHttpClient = this.clientWithRetryInterceptor();
        this.mRetroFit = new Retrofit.Builder()
                .client(okHttpClient)
                .baseUrl(ApiEndpointUri)
                .addConverterFactory(new GsonStringConverterFactory())
                .addConverterFactory(GsonConverterFactory.create())
                .build();

        mRequestService = this.mRetroFit.create(RequestService.class);
        mFileUploadService = this.mRetroFit.create(FileUploadService.class);
    }

    private void refreshForNewRequest(Context context) {
        mContext = context;
        mToken = CSRouter.getInstance().getAuthToken();
        mRequestOrigin = CSRouter.getInstance().getRequestOrigin();
        mDeviceInformationHelper = new DeviceInformationHelper(context);
    }

    /**
     * An interceptor that runs before every request. Retries failed requests 3 times before it passes a response
     *
     * @return OkHttpClient with an initialized interceptor that will retry requests
     */
    private OkHttpClient clientWithRetryInterceptor() {
        return new OkHttpClient.Builder()
                .connectTimeout(15, TimeUnit.SECONDS)
                .readTimeout(15, TimeUnit.SECONDS)
                .writeTimeout(15, TimeUnit.SECONDS)
                .addInterceptor(new Interceptor() {
                    @Override
                    public Response intercept(Interceptor.Chain chain) throws IOException {
                        Request request = chain.request();
                        // try the request
                        Response response = chain.proceed(request);

                        int tryCount = 0;
                        while (!response.isSuccessful() && shouldRetryRequest(response.code()) && tryCount < 3) {
                            tryCount++;

                            // retry the request
                            response = chain.proceed(request);
                        }

                        // otherwise just pass the original response on
                        return response;
                    }
                }).build();
    }

    private boolean shouldRetryRequest(int errorCode) {
        boolean shouldRetry = true;
        switch (errorCode) {
            case 401:
            case 404:

                shouldRetry = false;
                break;
        }

        return shouldRetry;
    }

    public void createEvent(String roomId, String summary, Date startDate, Date endDate, final Callback<CSCalendarEvent> callback)
        throws Exception {
        Log.d(TAG, "Create Event Request started");
        Log.d(TAG, "Start Date - " + startDate);
        Log.d(TAG, "End Date - " + endDate);

        DateFormat formatter = new SimpleDateFormat("yyyy-MM-dd HH:mm:ssZ");
        String startDateString = formatter.format(startDate);
        String endDateString = formatter.format(endDate);

        mRequestService.createEvent(
                mToken,
                mDeviceInformationHelper.getDeviceUID(),
                mDeviceInformationHelper.getFormattedDeviceInformation(),
                "1.0",
                mDeviceInformationHelper.getDeviceLanguagePreference(),
                mRequestOrigin,
                roomId,
                summary,
                startDateString,
                endDateString).enqueue(callback);
    }

    public void endNow(String roomId, String eventId, final Callback<CSCalendarEvent> callback)
        throws Exception {
        Log.d(TAG, "End Now Request started");

        mRequestService.endNow(mToken,
                mDeviceInformationHelper.getDeviceUID(),
                mDeviceInformationHelper.getFormattedDeviceInformation(),
                "1.0",
                mDeviceInformationHelper.getDeviceLanguagePreference(),
                mRequestOrigin,
                roomId,
                eventId).enqueue(callback);
    }

    public void getRoom(String roomId, final Callback<CSRoom> callback)
        throws Exception {
        Callback getRoomCallback = new Callback<CSRoom>() {
            @Override
            public void onResponse(Call<CSRoom> call, retrofit2.Response<CSRoom> response) {
                callback.onResponse(call, response);
                CSRouter.getInstance().updateRoom(response.body());
            }

            @Override
            public void onFailure(Call<CSRoom> call, Throwable t) {
                callback.onFailure(call, t);
            }
        };

        Log.d(TAG, "getRoom request - " + roomId);
        mRequestService.getRoom(
                mToken,
                mDeviceInformationHelper.getDeviceUID(),
                mDeviceInformationHelper.getFormattedDeviceInformation(),
                "1.0",
                mDeviceInformationHelper.getDeviceLanguagePreference(),
                mRequestOrigin,
                roomId
        ).enqueue(getRoomCallback);
    }

    public void getRooms(@Nullable Callback<List<CSRoom>> callback)
        throws Exception{
        mRequestService.getRooms(
                    mToken,
                    mDeviceInformationHelper.getDeviceUID(),
                    mDeviceInformationHelper.getFormattedDeviceInformation(),
                    "1.0",
                    mDeviceInformationHelper.getDeviceLanguagePreference(),
                    mRequestOrigin
                ).enqueue(callback);
    }

    public void auth(String idToken, Callback<CSSession> callback)
        throws Exception {

        mRequestService.auth(
                mDeviceInformationHelper.getDeviceUID(),
                mDeviceInformationHelper.getFormattedDeviceInformation(),
                "1.0",
                mDeviceInformationHelper.getDeviceLanguagePreference(),
                mRequestOrigin,
                idToken
        ).enqueue(callback);
    }

    public void submitSupportTicket(Bitmap image, String location, String message, int category, Callback<ResponseBody> callback)
        throws Exception {
        GoogleSignInAccount googleAccount = CSRouter.getInstance().getGoogleAccount();
        String email = googleAccount.getEmail();

        MultipartBody.Part imageBody = null;
        if (image != null) {
            ByteArrayOutputStream bao = new ByteArrayOutputStream();
            Bitmap resizedImage = BitmapHelper.getResizedBitmap(image, mMaxImageEdge);
            resizedImage.compress(Bitmap.CompressFormat.PNG, 100, bao);
            byte[] imageByteArray = bao.toByteArray();

            RequestBody requestFile = RequestBody.create(MediaType.parse("image/*"), imageByteArray);
            imageBody = MultipartBody.Part.createFormData("image", "image.png", requestFile);
        }

        mFileUploadService.uploadSupportTicket(
                mToken,
                mDeviceInformationHelper.getDeviceUID(),
                mDeviceInformationHelper.getFormattedDeviceInformation(),
                "1.0",
                mDeviceInformationHelper.getDeviceLanguagePreference(),
                mRequestOrigin,
                getAppInfo(),
                email,
                location,
                category,
                message,
                imageBody).enqueue(callback);

    }

    public String getAppInfo() {
        int stringId = mContext.getApplicationInfo().labelRes;
        String appName =  mContext.getString(stringId);

        String version = "";
        try {
            PackageInfo pInfo = mContext.getPackageManager().getPackageInfo(mContext.getPackageName(), 0);
            version = "ver. " + pInfo.versionName;
        } catch(Exception e) {
            e.printStackTrace();
        }

        return appName + version;
    }
}
