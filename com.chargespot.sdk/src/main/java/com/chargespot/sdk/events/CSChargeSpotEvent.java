/*
    Copyright (C) 2015 ChargeSpot
 */
package com.chargespot.sdk.events;

/**
 * ChargeSpot events
 *
 * @version 0.5
 */
public enum CSChargeSpotEvent {
    /**
     * Device is charging on ChargeSpot
     */
    CSEventStartedChargingOnChargeSpot(1),

    /**
     * Device stopped charging on a ChargeSpot
     */
    CSEventStoppedChargingOnChargeSpot(2);

    private final int mValue;

    CSChargeSpotEvent(int value) {
        this.mValue = value;
    }

    public int getValue() {
        return this.mValue;
    }
}
