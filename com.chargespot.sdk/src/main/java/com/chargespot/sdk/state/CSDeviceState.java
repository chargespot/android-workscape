/*
    Copyright (C) 2015 ChargeSpot
 */
package com.chargespot.sdk.state;

/**
 * Group events definitions
 * <p/>
 * Date: 10/12/2015
 *
 * @version 0.3
 */
public enum CSDeviceState {
    /**
     * Device state is unknown. Occurs when an error has happened.
     */
    CSDeviceStateUnknown(0),

    /**
     * Device bluetooth is disabled. Bluetooth is required to discover ChargeSpots.
     */
    CSDeviceBluetoothDisabled(1),

    /**
     * Device is not on a ChargeSpot.
     */
    CSDeviceNotOnChargeSpot(2),

    /**
     * Device is on a ChargeSpot but is not charging.
     */
    CSDeviceOnChargeSpot(3),

    /**
     * Device is charging and on a ChargeSpot.
     */
    CSDeviceChargingOnChargeSpot(4),

    /**
     * Device is charging on a ChargeSpot and has full battery.
     */
    CSDeviceFullChargingOnChargeSpot(5);

    private final int mValue;

    CSDeviceState(int value) {
        this.mValue = value;
    }

    public int getValue() {
        return this.mValue;
    }
}
