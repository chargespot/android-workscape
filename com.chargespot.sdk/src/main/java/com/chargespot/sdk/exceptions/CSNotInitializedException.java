/*
    Copyright (C) 2015 ChargeSpot
 */
package com.chargespot.sdk.exceptions;

/**
 * ChargeSpot SDK not initialized
 *
 * @version 0.3
 */
public class CSNotInitializedException
        extends CSException {
    /**
     * ChargeSpot not initialized exception
     *
     * @param message Information exception message
     */
    public CSNotInitializedException(final String message) {
        super(message);
    }
}
