package com.chargespot.sdk.manager;

import android.test.suitebuilder.annotation.SmallTest;

import com.chargespot.sdk.manager.chargespot.CSChargeSpotRadioManager;
import com.chargespot.sdk.model.CSChargeSpot;
import com.chargespot.sdk.model.CSChargeSpotRadio;
import com.chargespot.sdk.model.CSDistance;

import junit.framework.Assert;
import junit.framework.TestCase;

import java.util.HashMap;

/**
 * Created by kelvinlau on 16-04-15.
 */
public class CSChargeSpotRadioManagerTest extends TestCase {
    CSChargeSpotRadio immediateRadio;
    CSChargeSpotRadio nearRadio;
    CSChargeSpotRadio farRadio;

    @Override
    protected void setUp() throws Exception {
        super.setUp();

        CSChargeSpot chargespot = new CSChargeSpot();
        immediateRadio = new CSChargeSpotRadio(chargespot, -40, -59);
        nearRadio = new CSChargeSpotRadio(chargespot, -60, -59);
        farRadio = new CSChargeSpotRadio(chargespot, -100, -59);
    }

    @SmallTest
    public void testInitialization() {
        CSChargeSpotRadioManager radioManager = new CSChargeSpotRadioManager();
        Assert.assertNotNull("Radio Manager is null", radioManager);
    }

    @SmallTest
    public void testAddRadio() {
        CSChargeSpotRadioManager radioManager = new CSChargeSpotRadioManager();
        radioManager.addRadio(immediateRadio);
        HashMap<String, CSChargeSpotRadio> managerHashMap = radioManager.getRadios();

        String immediateKey = "" + immediateRadio.getChargeSpot().Address + immediateRadio.getChargeSpot().Major + immediateRadio.getChargeSpot().Minor + immediateRadio.getDistance();
        Assert.assertEquals("Radio Manager hash map does not contain immediate radio", managerHashMap.get(immediateKey), immediateRadio);
    }

    @SmallTest
    public void testGetRadios() {
        CSChargeSpotRadioManager radioManager = new CSChargeSpotRadioManager();
        radioManager.addRadio(immediateRadio);
        radioManager.addRadio(nearRadio);
        radioManager.addRadio(farRadio);

        HashMap<String, CSChargeSpotRadio> managerHashMap = radioManager.getRadios();

        Assert.assertEquals("Radio Manager hash map does not contain 3 radios", managerHashMap.size(), 3);

        String immediateKey = "" + immediateRadio.getChargeSpot().Address + immediateRadio.getChargeSpot().Major + immediateRadio.getChargeSpot().Minor + immediateRadio.getDistance();
        Assert.assertEquals("Radio Manager hash map does not contain immediate radio", managerHashMap.get(immediateKey), immediateRadio);

        String nearKey = "" + nearRadio.getChargeSpot().Address + nearRadio.getChargeSpot().Major + nearRadio.getChargeSpot().Minor + nearRadio.getDistance();
        Assert.assertEquals("Radio Manager hash map does not contain near radio", managerHashMap.get(nearKey), nearRadio);

        String farKey = "" + farRadio.getChargeSpot().Address + farRadio.getChargeSpot().Major + farRadio.getChargeSpot().Minor + farRadio.getDistance();
        Assert.assertEquals("Radio Manager hash map does not contain far radio", managerHashMap.get(farKey), farRadio);
    }

    @SmallTest
    public void testGetDefaultMinimumPing() {
        CSChargeSpotRadioManager radioManager = new CSChargeSpotRadioManager();
        int minimumPing = radioManager.getMinimumPing();

        Assert.assertEquals(minimumPing, 3);
    }

    @SmallTest
    public void testSetMinimumPing() {
        CSChargeSpotRadioManager radioManager = new CSChargeSpotRadioManager();
        radioManager.setMinimumPing(100);
        int minimumPing = radioManager.getMinimumPing();

        Assert.assertEquals(minimumPing, 100);
    }

    @SmallTest
    public void testGetClosestValidRadioForOneFarRadio() {
        CSChargeSpotRadio farResult;
        CSChargeSpotRadio nearResult;
        CSChargeSpotRadio immediateResult;

        CSChargeSpotRadioManager radioManager = new CSChargeSpotRadioManager();
        radioManager.setMinimumPing(0);

        radioManager.addRadio(farRadio);

        farResult = radioManager.getClosestValidRadio(CSDistance.FAR);
        Assert.assertNotNull(farResult);
        Assert.assertEquals(farResult, farRadio);

        nearResult = radioManager.getClosestValidRadio(CSDistance.NEAR);
        Assert.assertNull(nearResult);

        immediateResult = radioManager.getClosestValidRadio(CSDistance.IMMEDIATE);
        Assert.assertNull(immediateResult);
    }

    @SmallTest
    public void testGetClosestValidRadioForNearRadio() {
        CSChargeSpotRadio farResult;
        CSChargeSpotRadio nearResult;
        CSChargeSpotRadio immediateResult;

        CSChargeSpotRadioManager radioManager = new CSChargeSpotRadioManager();
        radioManager.setMinimumPing(0);

        radioManager.addRadio(farRadio);
        radioManager.addRadio(nearRadio);
        farResult = radioManager.getClosestValidRadio(CSDistance.FAR);
        Assert.assertNotNull(farResult);
        Assert.assertEquals(farResult, nearRadio);

        nearResult = radioManager.getClosestValidRadio(CSDistance.NEAR);
        Assert.assertNotNull(nearResult);
        Assert.assertEquals(nearResult, nearRadio);

        immediateResult = radioManager.getClosestValidRadio(CSDistance.IMMEDIATE);
        Assert.assertNull(immediateResult);
    }

    @SmallTest
    public void testGetClosestValidRadioForImmediateRadio() {
        CSChargeSpotRadio farResult;
        CSChargeSpotRadio nearResult;
        CSChargeSpotRadio immediateResult;

        CSChargeSpotRadioManager radioManager = new CSChargeSpotRadioManager();
        radioManager.setMinimumPing(0);

        radioManager.addRadio(farRadio);
        radioManager.addRadio(nearRadio);
        radioManager.addRadio(immediateRadio);

        farResult = radioManager.getClosestValidRadio(CSDistance.FAR);
        Assert.assertNotNull(farResult);
        Assert.assertEquals(farResult, immediateRadio);

        nearResult = radioManager.getClosestValidRadio(CSDistance.NEAR);
        Assert.assertNotNull(nearResult);
        Assert.assertEquals(nearResult, immediateRadio);

        immediateResult = radioManager.getClosestValidRadio(CSDistance.IMMEDIATE);
        Assert.assertNotNull(immediateResult);
        Assert.assertEquals(immediateResult, immediateRadio);
    }
}
