/*
    Copyright (C) 2015 ChargeSpot
 */
package com.chargespot.sdk.api.v1.model;

import com.chargespot.sdk.model.CSHook;

/**
 * ChargeSpot hook list model
 *
 * @version 0.5
 */
public final class ChargeSpotHookListModel {
    /**
     * The ChargeSpot device being tracked
     */
    public ChargeSpotId chargespot;

    /**
     * List of the hooks associated with the ChargeSpot device
     */
    public CSHook[] hook;
}
