/*
    Copyright (C) 2015 ChargeSpot
 */
package com.chargespot.sdk.exceptions;

/**
 * Bluetooth errors
 *
 * @version 0.3
 */
public enum CSBluetoothErrors {
    /**
     * Bluetooth not enabled
     */
    BluetoothNotEnabled,

    /**
     * No Bluetooth permissions available
     */
    BluetoothNoPermission,
}
