/*
    Copyright (C) 2015 ChargeSpot
 */
package com.chargespot.sdk.exceptions;

/**
 * ChargeSpot Bluetooth exception
 *
 * @version 0.3
 */
public class CSBluetoothException
        extends CSException {
    /**
     * Bluetooth errors
     */
    public final CSBluetoothErrors BluetoothError;

    /**
     * Create a ChargeSpot Bluetooth exceptions
     *
     * @param message Exception message
     * @param error   Bluetooth error
     */
    public CSBluetoothException(final String message, final CSBluetoothErrors error) {
        super(message);

        this.BluetoothError = error;
    }
}
