/*
    Copyright (C) 2015 ChargeSpot
 */
package com.chargespot.sdk.exceptions;

/**
 * ChargeSpot Invalid parameter exception
 *
 * @version 0.3
 */
public class CSInvalidParameterException
        extends CSException {
    /**
     * ChargeSpot exception
     *
     * @param message Information exception message
     */
    public CSInvalidParameterException(final String message) {
        super(message);
    }
}
