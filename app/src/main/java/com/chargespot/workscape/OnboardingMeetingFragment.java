package com.chargespot.workscape;

import android.support.v4.app.Fragment;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;

/**
 * Created by kelvinlau on 2016-08-23.
 */

public class OnboardingMeetingFragment extends Fragment implements View.OnClickListener {

    public interface OnboardingMeetingListener {
        void onNextClick();
    }

    private Button mNextButton;
    private OnboardingMeetingListener mListener;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_onboarding_meeting, container, false);

        mNextButton = (Button)view.findViewById(R.id.nextButton);
        mNextButton.setOnClickListener(this);

        return view;
    }

    public void setOnboardingMeetingListener(OnboardingMeetingListener listener) {
        mListener = listener;
    }

    @Override
    public void onClick(View v) {
        switch(v.getId()) {
            case R.id.nextButton:
                if (mListener != null) {
                    mListener.onNextClick();
                }
                break;
        }
    }
}
