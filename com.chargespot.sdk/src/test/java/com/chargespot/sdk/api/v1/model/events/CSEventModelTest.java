/*
    Copyright (C) 2015 ChargeSpot
 */
package com.chargespot.sdk.api.v1.model.events;

import android.test.suitebuilder.annotation.SmallTest;

import com.chargespot.sdk.events.CSChargeSpotEvent;

import junit.framework.Assert;
import junit.framework.TestCase;

/**
 * CSEventModelTest
 *
 * Date: 10/12/2015
 * @version 0.3
 */
public class CSEventModelTest
    extends TestCase
{
    @SmallTest
    public void testIsValid()
        throws Exception
    {
        final CSChargeSpotEvent event = CSChargeSpotEvent.CSEventStartedChargingOnChargeSpot;
        CSEventModel model = new CSChargeSpotEventModel(null, 0, 0, event);
        Assert.assertFalse(model.isValid());

        model.UUID = "";
        Assert.assertFalse(model.isValid());

        model.UUID = "UUID";
        Assert.assertTrue(model.isValid());
    }
}