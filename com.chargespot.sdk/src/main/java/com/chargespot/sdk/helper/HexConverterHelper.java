/*
    Copyright (C) 2015 ChargeSpot
 */
package com.chargespot.sdk.helper;

import com.chargespot.sdk.exceptions.CSInvalidParameterException;

/**
 * Helper to handle Hex conversions
 *
 * @version 0.5
 */
public class HexConverterHelper {
    /**
     * This is a purely static class
     */
    private HexConverterHelper() {
    }

    final private static char[] mHexArray = "0123456789ABCDEF".toCharArray();

    /**
     * Convert the bytes to a hex string
     *
     * @param bytes  Input array of bytes
     * @param start  Starting index of the array
     * @param length Length of the array bytes to convert
     * @version 0.3
     */
    public static String convertBytesToHex(final byte[] bytes, final int start, final int length) throws CSInvalidParameterException {
        if (null == bytes || bytes.length < start + length) {
            throw new CSInvalidParameterException("Invalid byte array");
        }

        char[] hexChars = new char[length * 2];
        for (int j = start; j < start + length; j++) {
            int v = bytes[j] & 0xFF;
            hexChars[(j - start) * 2] = mHexArray[v >>> 4];
            hexChars[(j - start) * 2 + 1] = mHexArray[v & 0x0F];
        }

        return new String(hexChars);
    }

    public static byte[] convertHexToBytes(String s) {
        int len = s.length();
        byte[] data = new byte[len / 2];
        for (int i = 0; i < len; i += 2) {
            data[i / 2] = (byte) ((Character.digit(s.charAt(i), 16) << 4)
                    + Character.digit(s.charAt(i + 1), 16));
        }
        return data;
    }
}
