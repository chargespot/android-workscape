/*
    Copyright (C) 2015 ChargeSpot
 */
package com.chargespot.sdk.api.v1.model.events;

import com.chargespot.sdk.model.CSChargeSpot;

/**
 * ChargeSpot Event common descriptor
 *
 * @version 0.5
 */
abstract class CSEventModel
        extends CSChargeSpot {
    /**
     * Check if the Event Model is valid
     *
     * @return Checks if the ChargeSpot Device record is valid
     */
    public boolean isValid() {
        return !(null == UUID || UUID.isEmpty());
    }
}
