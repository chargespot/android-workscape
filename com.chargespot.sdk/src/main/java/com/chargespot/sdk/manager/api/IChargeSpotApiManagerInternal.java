/*
    Copyright (C) 2015 ChargeSpot
 */
package com.chargespot.sdk.manager.api;

import android.support.annotation.NonNull;
import android.support.annotation.Nullable;

import com.chargespot.sdk.api.v1.model.ChargeSpotGroupHookListModel;
import com.chargespot.sdk.api.v1.model.ChargeSpotHookListModel;
import com.chargespot.sdk.api.v1.model.events.CSChargeSpotEventModel;
import com.chargespot.sdk.api.v1.model.events.CSGroupEventModel;
import com.chargespot.sdk.exceptions.CSInvalidParameterException;

import retrofit2.Callback;

/**
 * Internal extension to the API interface. These
 * functions will not be exposed to the SDK consumer
 * <p/>
 * Date: 10/12/2015
 *
 * @version 0.3
 */
public interface IChargeSpotApiManagerInternal
        extends IChargeSpotApiManager {
    /**
     * listGroupHooks returns the group hooks based on a specific
     * ChargeSpot instance
     *
     * @param groupEvent Group event that occurred
     * @param callback   Callback to return the list of chargespot regions
     */
    void listGroupHooks(@Nullable CSGroupEventModel groupEvent, @Nullable Callback<ChargeSpotGroupHookListModel> callback) throws CSInvalidParameterException;

    /**
     * listChargeSpotHooks returns the chargespot hooks based on a specific
     * ChargeSpot instance
     *
     * @param chargeSpotEvent ChargeSpot event that occurred
     * @param callback        Callback to return the list of chargespot regions
     */
    void listChargeSpotHooks(@Nullable CSChargeSpotEventModel chargeSpotEvent, Callback<ChargeSpotHookListModel> callback) throws CSInvalidParameterException;
}
