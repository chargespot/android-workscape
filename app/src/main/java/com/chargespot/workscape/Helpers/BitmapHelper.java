package com.chargespot.workscape.Helpers;

import android.graphics.Bitmap;

/**
 * Created by kelvinlau on 2016-08-02.
 */

public class BitmapHelper {
    public static Bitmap getResizedBitmap(Bitmap image, int maxSize) {
        int width = image.getWidth();
        int height = image.getHeight();

        int scaledWidth;
        int scaledHeight;
        float bitmapRatio = (float) width / (float) height;
        if (bitmapRatio > 1) {
            scaledWidth = maxSize;
            scaledHeight = (int) (scaledWidth / bitmapRatio);
        } else {
            scaledHeight = maxSize;
            scaledWidth = (int) (scaledHeight * bitmapRatio);
        }

        return Bitmap.createScaledBitmap(image, scaledWidth, scaledHeight, true);
    }

}
