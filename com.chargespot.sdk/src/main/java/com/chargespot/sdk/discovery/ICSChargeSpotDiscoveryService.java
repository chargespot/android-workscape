/*
    Copyright (C) 2015 ChargeSpot
 */
package com.chargespot.sdk.discovery;

import android.support.annotation.Nullable;

import com.chargespot.sdk.exceptions.CSBluetoothException;

/**
 * Discovery service interface fore responses.
 *
 * @version 0.5
 */
public interface ICSChargeSpotDiscoveryService {
    /**
     * Register a ChargeSpot discovery
     * callback.
     *
     * @param callback Callback for handling discovery events
     */
    void registerChargeSpotDiscoveryCallback(@Nullable ICSChargeSpotDiscoveryCallback callback);

    /**
     * Unregister a ChargeSpot discovery callback
     */
    void unregisterChargeSpotDiscoveryCallback();

    /**
     * Start the discovery
     */
    void startDiscovery() throws CSBluetoothException;

    /**
     * Stop the discovery
     */
    void stopDiscovery();
}
