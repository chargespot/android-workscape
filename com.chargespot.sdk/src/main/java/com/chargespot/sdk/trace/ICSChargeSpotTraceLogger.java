/*
    Copyright (C) 2015 ChargeSpot
 */
package com.chargespot.sdk.trace;

import android.support.annotation.NonNull;
import android.support.annotation.Nullable;

/**
 * Base trace logger
 * <p/>
 * Date: 05/12/2015
 *
 * @version 0.3
 */
public interface ICSChargeSpotTraceLogger {
    /**
     * ChargeSpot logging interface
     *
     * @param level   Type of the trace message
     * @param message Message to be logged
     */
    void Trace(@Nullable CSChargeSpotTraceLevel level, @Nullable String message);
}
