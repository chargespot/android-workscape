/*
    Copyright (C) 2015 ChargeSpot
 */
package com.chargespot.sdk.events;

/**
 * Group events definitions
 *
 * @version 0.5
 */
public enum CSGroupEvent {
    /**
     * Device entered a ChargeSpot group.
     */
    CSEventEnteredGroup(1),

    /**
     * Device exited a ChargeSpot group.
     */
    CSEventExitedGroup(2);

    private final int mValue;

    CSGroupEvent(int value) {
        this.mValue = value;
    }

    public int getValue() {
        return this.mValue;
    }
}
