package com.chargespot.workscape.Helpers;

import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.support.v4.app.NotificationCompat;
import android.support.v4.content.ContextCompat;

import com.chargespot.workscape.BaseActivity;
import com.chargespot.workscape.R;
import com.chargespot.workscape.Models.CSCalendarEvent;
import com.chargespot.workscape.Models.CSRoom;

import java.util.Date;

/**
 * Created by kelvinlau on 2016-08-23.
 */

public class NotificationHelper {

    public static final String NOTIFICATION_KEY = "CS_NOTIFICATION";
    public static final String NOTIFICATION_ROOM_KEY= "CS_NOTIFICATION_ROOM";
    public static final String NOTIFICATION_EVENT_KEY= "CS_NOTIFICATION_EVENT";
    public static final String NOTIFICATION_END_DATE_KEY = "CS_NOTIFICATION_END_DATE_KEY";
    public static final int NOTIFICATION_ID = 110;

    public static void createNotification(Context context, String title, String message) {
        NotificationCompat.Builder mBuilder =
                new NotificationCompat.Builder(context)
                        .setSmallIcon(R.drawable.ic_workscape_notification)
                        .setContentTitle(title)
                        .setContentText(message)
                        .setStyle(new NotificationCompat.BigTextStyle()
                                .bigText(message))
                        .setColor(ContextCompat.getColor(context, R.color.colorCSTeal));

        Intent notificationIntent = new Intent(context, BaseActivity.class);
        notificationIntent.setFlags(Intent.FLAG_ACTIVITY_SINGLE_TOP);
        PendingIntent pendingIntent = PendingIntent.getActivity(context, 0, notificationIntent, PendingIntent.FLAG_UPDATE_CURRENT);
        mBuilder.setContentIntent(pendingIntent);

        NotificationManager mNotificationManager = (NotificationManager) context.getSystemService(Context.NOTIFICATION_SERVICE);
        mNotificationManager.notify(NOTIFICATION_ID, mBuilder.build());
    }

    public static void createBookNowNotification(Context context, String title, String message, CSRoom room) {
        Date endDate = DateHelper.addMinutesToDate(new Date(), 15);
        endDate = DateHelper.roundDateToNearestFifteenMinutes(endDate);

        Intent bookNowIntent = new Intent(context, BookNowReceiver.class);
        bookNowIntent.putExtra(NOTIFICATION_KEY, NOTIFICATION_ID);

        bookNowIntent.putExtra(NOTIFICATION_ROOM_KEY, room.toJson());
        bookNowIntent.putExtra(NOTIFICATION_END_DATE_KEY, endDate.getTime());

        PendingIntent pendingBookNow = PendingIntent.getBroadcast(context, 0, bookNowIntent, PendingIntent.FLAG_UPDATE_CURRENT);

        NotificationCompat.Builder mBuilder =
                new NotificationCompat.Builder(context)
                        .setSmallIcon(R.drawable.ic_workscape_notification)
                        .setContentTitle(title)
                        .setContentText(message)
                        .setStyle(new NotificationCompat.BigTextStyle()
                                .bigText(message))
                        .setColor(ContextCompat.getColor(context, R.color.colorCSTeal))
                        .addAction(R.drawable.ic_book_now_action, context.getString(R.string.book_until_date, DateHelper.stringForDateFormat(endDate, "h:mm a")), pendingBookNow);

        Intent notificationIntent = new Intent(context, BaseActivity.class);
        notificationIntent.setFlags(Intent.FLAG_ACTIVITY_SINGLE_TOP);
        PendingIntent pendingIntent = PendingIntent.getActivity(context, 0, notificationIntent, PendingIntent.FLAG_UPDATE_CURRENT);
        mBuilder.setContentIntent(pendingIntent);

        NotificationManager mNotificationManager = (NotificationManager) context.getSystemService(Context.NOTIFICATION_SERVICE);
        mNotificationManager.notify(NOTIFICATION_ID, mBuilder.build());
    }

    public static void createEndNowNotification(Context context, String title, String message, CSRoom room, CSCalendarEvent event) {
        Intent bookNowIntent = new Intent(context, EndNowReceiver.class);
        bookNowIntent.putExtra(NOTIFICATION_KEY, NOTIFICATION_ID);

        bookNowIntent.putExtra(NOTIFICATION_ROOM_KEY, room.toJson());
        bookNowIntent.putExtra(NOTIFICATION_EVENT_KEY, event.toJson());

        PendingIntent pendingBookNow = PendingIntent.getBroadcast(context, 0, bookNowIntent, PendingIntent.FLAG_UPDATE_CURRENT);

        NotificationCompat.Builder mBuilder =
                new NotificationCompat.Builder(context)
                        .setSmallIcon(R.drawable.ic_workscape_notification)
                        .setContentTitle(title)
                        .setContentText(message)
                        .setStyle(new NotificationCompat.BigTextStyle()
                                .bigText(message))
                        .setColor(ContextCompat.getColor(context, R.color.colorCSRed))
                        .addAction(R.drawable.ic_end_now, context.getString(R.string.end_now), pendingBookNow);

        Intent notificationIntent = new Intent(context, BaseActivity.class);
        notificationIntent.setFlags(Intent.FLAG_ACTIVITY_SINGLE_TOP);
        PendingIntent pendingIntent = PendingIntent.getActivity(context, 0, notificationIntent, PendingIntent.FLAG_UPDATE_CURRENT);
        mBuilder.setContentIntent(pendingIntent);

        NotificationManager mNotificationManager = (NotificationManager) context.getSystemService(Context.NOTIFICATION_SERVICE);
        mNotificationManager.notify(NOTIFICATION_ID, mBuilder.build());
    }

    public static void cancelNotification(Context context, Intent intent) {
        int notificationId = intent.getIntExtra(NOTIFICATION_KEY, 0);
        if (notificationId != 0) {
            NotificationManager notificationManager = (NotificationManager)context.getSystemService(Context.NOTIFICATION_SERVICE);
            notificationManager.cancel(notificationId);
        }
    }
}
